import matplotlib.pyplot as plt
import numpy as np
from torch.autograd import Variable


def grid(activation):
    n_act, dim_x, dim_y = activation.shape
    size = int(np.ceil(np.sqrt(n_act)))

    grid = np.zeros([size * dim_x, size * dim_y])
    for i, act in enumerate(activation):
        x = int(np.floor(i / size))
        y = i % size

        grid[x*dim_x:(x+1)*dim_x, y*dim_y:(y+1)*dim_y] = act

    return grid


def plot_activations(activations, vmin=None, vmax=None):
    n_plots = len(activations)
    size = int(np.ceil(np.sqrt(n_plots)))
    f, AX = plt.subplots(size, size, figsize=[10, 10])
    AX = np.reshape(AX, [-1])

    activations = list(activations.items())
    for i, ax in enumerate(AX):
        if i < n_plots:
            name, act = activations[i]
            has_roi = False
            if isinstance(act, list):
                has_roi = True
                act, roi = act

            # Test
            if isinstance(act, Variable):
                act = act.data
            act = act.cpu().squeeze().numpy()
            grid_repr = grid(act)
            ax.imshow(grid_repr, vmin=vmin, vmax=vmax)

            size_x, size_y = grid_repr.shape
            n_act, step_x, step_y = act.shape
            x_lines = np.arange(0, size_x, step=step_x) - 0.5
            y_lines = np.arange(0, size_y, step=step_y) - 0.5

            ax.vlines(y_lines, -1, size_x+1, colors='r', lw=0.5)
            ax.hlines(x_lines, -1, size_y+1, colors='r', lw=0.5)
            ax.set_xlim(-0.5, size_y-0.5)
            ax.set_ylim(-0.5, size_x-0.5)
            if has_roi:
                hstart, hstop, wstart, wstop = roi.cpu().numpy()
                X = np.array([hstart, hstop, hstart, hstop])
                Y = np.array([wstart, wstart, wstop, wstop])
                h_lines = np.linspace(hstart, hstop, num=11, endpoint=True)
                w_lines = np.linspace(wstart, wstop, num=11, endpoint=True)
                for i in np.arange(size_x, step=step_x):
                    XX = X + i
                    HH = h_lines + i
                    for j in np.arange(size_y, step=step_y):
                        YY = Y + j
                        WW = w_lines + j
                        ax.plot(YY, XX,
                                c='r', ls='',
                                marker='o', ms=2)
                        ax.vlines(WW, X[0], X[1], colors='r', lw=0.5)
                        ax.hlines(HH, Y[0], Y[2], colors='r', lw=0.5)

            ax.set_title('{} {}x{}x{}'.format(name, n_act, step_x, step_y))
        else:
            ax.axis('off')
    plt.show()
