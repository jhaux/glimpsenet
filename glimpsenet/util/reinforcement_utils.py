import tensorflow as tf


def time_decay(N_ts):
    timesteps = tf.range(1., tf.to_float(N_ts+1), dtype=tf.float32)
    time_decay = 1. / timesteps

    return time_decay


def time_neg_reward(N_ts):
    return -tf.ones([N_ts])


def time_zero_reward(N_ts):
    return -tf.zeros([N_ts])


def H(glimpse):
    '''Calculates normalized entropy of image'''

    H = -tf.reduce_mean(glimpse * tf.log(glimpse+1e-10)) / 0.36
    return H
