import torch
import torch.nn as nn
import torch.nn.init as init
from torch.autograd import Variable
import numpy as np
import os as osys
# import time

import gym
import gym_multiMnist

from model.drama import DeepRecurrentAttentionModel as DRAM
from model.buildingBlocks import Baseline
from training.hooks.plots import PlotGenerator
from util.tensor_ops import to_variable, to_array, to_tensor


def log_pdf(sample, mean, sigma):
    '''Compute log prob. density fn. of a normal distribution with given mean
    and stddev.

    Normal PDF: pd(x | \mu, \sigma) = \frac{1}{\sqrt{2\pi\sigma^2}}
                                      \cdot \exp{-\frac{(x - \mu)^2}{2\sigma^2}
    Log PDf: lg_pd(x | \mu, \sigma) = -\frac{(x - \mu)^2}{2\sigma^2}
                                      - \log{\sqrt{2\pi\sigma^2}}
    '''
    log_exp = -(sample - mean)**2 / (2*sigma**2)
    offset = np.log(np.sqrt(2*np.pi)*sigma)

    return log_exp - offset


class A3C_Worker(nn.Module):
    '''Wraps around a model and trains it.'''

    def __init__(self, Config, ckpt_path,
                 parameter_server=False,
                 test_worker=False,
                 fixed_stepsize=True):
        super(A3C_Worker, self).__init__()

        self.Config = Config

        self.policy = DRAM(Config).cuda()
        self.baseline = Baseline(Config.getParameter('Baseline')).cuda()
        self.fss = Config.getParameter('HyperParameters/fixedStepSize')
        step_size = Config.getParameter('HyperParameters/predSteps')

        n_o = Config.getParameter('HyperParameters/maxObjs')
        dif = Config.getParameter('Dataset/difficulty')
        env_params = {
            'N_obj': n_o,
            'difficulty': dif,
            'sorted_targets': True,
            'fss': None if not self.fss else step_size,
            'mode': 'train' if not test_worker else 'test'
            }

        env_id = 'MultiMnist-{}.{}-v1'.format(n_o, dif)
        gym.envs.register(id=env_id,
                          entry_point='gym_multiMnist.envs:MultiMnistEnv',
                          kwargs=env_params)
        self.env = gym.make(env_id)

        self.max_iter = Config.getParameter('HyperParameters/maxIter')
        self.M = Config.getParameter('HyperParameters/M')
        self.sig_em = Config.getParameter('HyperParameters/sigma_em')
        self.sig_sig = Config.getParameter('HyperParameters/sigma_sig')
        self.l = Config.getParameter('HyperParameters/l')
        self.discount = Config.getParameter('HyperParameters/discountFactor')
        self.pred_thresh = Config.getParameter('predictionThreshold')
        self.accumulate = not self.fss
        self.test_losses = False

        self.last_checkpoint = None

        self.store_for_log = False
        if parameter_server:
            self.save_path = ckpt_path
            print('Save Path of this model is {}'.format(self.save_path))
        if parameter_server or test_worker:
            self.plotter = PlotGenerator(Config, ckpt_path)
            self.store_for_log = True

    def find_checkpoint(self):
        files = osys.listdir(self.save_path)
        files = [f for f in files if '_model.torch.tar' in f]
        if len(files) > 0:
            nums = [int(n) for n, _ in [f.split('_') for f in files]]
            highNum = max(nums)
            file_name = '{}_model.torch.tar'.format(highNum)
            self.last_checkpoint = osys.path.join(self.save_path, file_name)
            self.last_ckpt_step = highNum
            return True
        else:
            return False

    def restore_from_checkpoint(self):
        if self.last_checkpoint is not None:
            self.load_state_dict(torch.load(self.last_checkpoint))
            print('Restoring from {}'.format(self.last_checkpoint))
            self.step = self.last_ckpt_step

    def initialize_variables(self):
        if self.find_checkpoint():
            self.restore_from_checkpoint()
        else:
            self.step = 0

            def initializer(m):
                if isinstance(m, nn.Conv2d) or isinstance(m, nn.Conv2d):
                    init.xavier_uniform(m.weight)
                    init.normal(m.bias)
                if isinstance(m, nn.LSTMCell):
                    init.xavier_uniform(m.weight_ih)
                    init.normal(m.bias_ih)
                    init.xavier_uniform(m.weight_hh)
                    init.normal(m.bias_hh)

            self.apply(initializer)

    def eval_loop(self, low_res, samples=None):
        '''One forward pass of the recurrent newtork.'''
        self.env.restart()

        l_list = []
        ps_list = []
        o_list = []
        V_list = []
        R_list = []
        pred_list = []
        pred_sig_list = []
        targ_list = []
        glimpse_list = []

        state1, state2 = self.policy.initial_lstm_states(low_res)
        c2 = state2[1]
        if samples is None:
            l, ps, o = self.policy.initial_action(c2)
        else:
            l, ps, o = samples[0]
        pred = None
        targ = None
        pred_signal = 0.
        reward = 0
        terminal = False
        action = {'location': np.squeeze(to_array(l)),
                  'patch_size': np.squeeze(to_array(ps)),
                  'prediction': pred,
                  'pred_signal': pred_signal}
        glimpse_np, reward, terminal, info = self.env.step(action)
        glimpse = np.transpose(glimpse_np, [2, 0, 1])
        glimpse = np.expand_dims(glimpse, 0)
        glimpse = to_variable(glimpse).cuda()
        targ = info['target']

        for t in range(self.max_iter + 1):
            V = self.baseline(state2[1].detach())
            l_list.append(l)
            ps_list.append(ps)
            o_list.append(o)
            V_list.append(V)
            R_list.append(reward)
            pred_list.append(pred)
            pred_sig_list.append(pred_signal)
            targ_list.append(targ)
            glimpse_list.append(glimpse_np)

            if pred_signal > self.pred_thresh:
                pred_signal = 0

            if terminal:
                break

            l, ps, o, pred, state1, state2 \
                = self.policy(l, ps, glimpse, state1, state2)
            pred_signal += np.squeeze(to_array(o))

            if samples is not None:
                l, ps, o = samples[t + 1]

            action = {'location': np.squeeze(to_array(l)),
                      'patch_size': np.squeeze(to_array(ps)),
                      'prediction': np.squeeze(to_array(pred)),
                      'pred_signal': pred_signal}
            glimpse_np, reward, terminal, info = self.env.step(action)
            glimpse = np.transpose(glimpse_np, [2, 0, 1])
            glimpse = np.expand_dims(glimpse, 0)
            glimpse = to_variable(glimpse).cuda()
            targ = info['target']

        return [l_list,
                ps_list,
                o_list,
                V_list,
                R_list,
                pred_list,
                pred_sig_list,
                targ_list,
                glimpse_list]

    def calc_returns(self, record):
        rewards = record[4]
        targets = record[7]
        returns = []
        R = 0
        for r, t in zip(rewards[::-1], targets[::-1]):
            if self.accumulate:
                # Return is discounted sum of futur rewards
                R += self.discount * r
                returns.append(R)
            else:
                # Return is one until correct prediction else 0
                if t is not None:
                    if r > 0:
                        R = 1
                    else:
                        R = 0
                returns.append(R)

        returns = returns[::-1]
        return returns

    def train_step(self, log):
        M = self.M
        s_e = self.sig_em
        s_s = self.sig_sig

        low_res = self.env.reset()
        low_res = np.transpose(low_res, [2, 0, 1])
        low_res = np.expand_dims(low_res, 0)
        low_res = to_variable(low_res, grad=False).cuda()

        recording_init = self.eval_loop(low_res)
        recording_init.append(self.calc_returns(recording_init))

        ls = recording_init[0]
        pss = recording_init[1]
        os = recording_init[2]
        recordings = []
        total_reinforce_loss = 0.
        total_classification_loss = 0.
        total_baseline_loss = 0.
        for m in range(M):
            ls_s = []
            pss_s = []
            os_s = []
            for ll, pp, oo in zip(ls, pss, os):
                ll_s = ll + Variable(torch.randn(2)).cuda() * s_e
                pp_s = pp + Variable(torch.randn(1)).cuda() * s_e
                pp_s = pp_s.clamp(min=0)
                oo_s = oo + Variable(torch.randn(1)).cuda() * s_s
                oo_s = oo_s.clamp(min=0)
                ls_s.append(ll_s)
                pss_s.append(pp_s)
                os_s.append(oo_s)

            samples = list(zip(ls_s, pss_s, os_s))
            recording = list(self.eval_loop(low_res, samples))
            returns = self.calc_returns(recording)
            recording.append(returns)

            recordings.append(recording)

            reinforce_loss = 0.
            classification_loss = 0.
            N_pred = 0
            baseline_loss = 0.
            for ts, (l, ps, o, V, r, pr, prs, t, g, R) \
                    in enumerate(zip(*recording)):
                l_hat = ls[ts]
                ps_hat = pss[ts]
                o_hat = os[ts]

                # Loss: grad[log(1/sqrt(2pi sig^2) * exp((s-m)^2)/2sig^2)]
                #     = grad[(s-m)^2/2sig^2 - log(sqrt(2pi sig^2)]
                log_pl = log_pdf(l.detach(), l_hat, self.sig_em)
                log_pps = log_pdf(ps.detach(), ps_hat, self.sig_em)
                log_po = log_pdf(o.detach(), o_hat, self.sig_em)

                eligibility = (R - V).detach()

                re_tmp = -torch.mean(log_pl * eligibility)
                re_tmp += -torch.mean(log_pps * eligibility)
                re_tmp += -torch.mean(log_po * eligibility)

                bl_tmp = (R - V)**2

                cl_tmp = 0.
                if t is not None:
                    pred = pr + 1e-20  # .clamp(min=1e-20)
                    crossE = torch.log(pred) * to_variable(t).cuda()
                    crossE = torch.sum(crossE)

                    cl_tmp = -crossE
                    N_pred += 1

                reinforce_loss += re_tmp
                classification_loss += cl_tmp
                baseline_loss += bl_tmp

            N_ts = (ts + 1)
            total_baseline_loss += baseline_loss / N_ts
            total_classification_loss += classification_loss / N_pred
            total_reinforce_loss += reinforce_loss / N_ts

        total_baseline_loss /= M
        total_classification_loss /= M
        total_reinforce_loss /= M
        total_loss = total_baseline_loss + total_classification_loss
        total_loss += self.l * total_reinforce_loss

        if log:
            re_loss = np.squeeze(to_array(total_reinforce_loss))
            cl_loss = np.squeeze(to_array(total_classification_loss))
            bl_loss = np.squeeze(to_array(total_baseline_loss))
            t_loss = np.squeeze(to_array(total_loss))
            glimpses = np.stack(recording_init[-2], axis=0)
            logs = dict(
                    images=dict(
                        image=self.env.image,
                        low_res=self.env.low_res,
                        glimpses=glimpses),
                    scalars=dict(
                        total_loss=t_loss,
                        total_reinforce_loss=re_loss,
                        total_baseline_loss=bl_loss,
                        total_classification_loss=cl_loss),
                )
        else:
            logs = None

        if self.store_for_log:
            self.recordings = recordings
            self.recording_init = recording_init

        return total_loss, logs

    def test_step(self):
        low_res = self.env.reset()
        low_res = np.transpose(low_res, [2, 0, 1])
        low_res = np.expand_dims(low_res, 0)
        low_res = to_variable(low_res, grad=False).cuda()

        recording_init = self.eval_loop(low_res)
        recording_init.append(self.calc_returns(recording_init))

        recordings = []
        total_classification_loss = 0.
        total_baseline_loss = 0.
        N_pred = 0
        for ts, (l, ps, o, V, r, pr, prs, t, g, R) \
                in enumerate(zip(*recording_init)):
            bl_tmp = (R - V)**2

            cl_tmp = 0.
            if t is not None:
                pred = pr + 1e-20  # .clamp(min=1e-20)
                crossE = torch.log(pred) * to_variable(t).cuda()
                crossE = torch.sum(crossE)

                cl_tmp = -crossE
                N_pred += 1

            total_classification_loss += cl_tmp
            total_baseline_loss += bl_tmp

        N_ts = (ts + 1)
        total_baseline_loss /= N_ts
        total_classification_loss /= N_pred

        total_loss = total_baseline_loss + total_classification_loss

        cl_loss = np.squeeze(to_array(total_classification_loss))
        bl_loss = np.squeeze(to_array(total_baseline_loss))
        t_loss = np.squeeze(to_array(total_loss))
        glimpses = np.stack(recording_init[-2], axis=0)
        logs = dict(
                images=dict(
                    image=self.env.image,
                    low_res=self.env.low_res,
                    glimpses=glimpses),
                scalars=dict(
                    total_loss=t_loss,
                    total_baseline_loss=bl_loss,
                    total_classification_loss=cl_loss),
            )

        self.recordings = recordings
        self.recording_init = recording_init

        return total_loss, logs

    def log(self, step, log_samples=False, test=False, index=None):
        accuracy = 0.
        if not test:
            for m in range(self.M):
                recording = self.recordings[m]
                acc_ts = 0.
                N_pred = 0.
                for ts, (l, ps, o, V, r, pr, prs, t, g, R) \
                        in enumerate(zip(*recording)):
                    if t is not None:
                        _, pred = torch.max(pr, -1)
                        _, targ = torch.max(to_tensor(t), -1)
                        acc_ts += torch.equal(pred.data, targ.cuda())
                        N_pred += 1
                acc_ts /= N_pred
                accuracy += acc_ts
            accuracy /= self.M
        else:
            accuracy = 0.
            N_pred = 0.
            for ts, (l, ps, o, V, r, pr, prs, t, g, R) \
                    in enumerate(zip(*self.recording_init)):
                if t is not None:
                    _, pred = torch.max(pr, -1)
                    _, targ = torch.max(to_tensor(t), -1)
                    accuracy += torch.equal(pred.data, targ.cuda())
                    N_pred += 1
            accuracy /= N_pred

        accuracy *= 100.
        im = self.env.image
        targs = self.env.targets
        plot_rec = to_array(self.recording_init)
        mode = 'train' if not test else 'eval'
        self.plotter(plot_rec,
                     im,
                     targs,
                     step=step,
                     accuracy=accuracy,
                     mode=mode,
                     index=index)

        if log_samples and not test:
            for s, rec in enumerate(self.recordings):
                plot_rec = to_array(rec)
                self.plotter(plot_rec, im, targs, step=step,
                             accuracy=accuracy,
                             sample=s)

        return accuracy
