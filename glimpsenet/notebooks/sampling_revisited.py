import tensorflow as tf
import numpy as np


def sample(mean, sigma, M):
    shape = tf.concat([[M], mean.shape], axis=0)
    mean = tf.expand_dims(mean, 0)
    noise = tf.random_normal(shape=shape, mean=0., stddev=1., seed=42)
    scaled_noise = sigma * noise
    samples = mean + scaled_noise
    return samples, scaled_noise, mean


mean = tf.ones(shape=[20, 2], dtype=tf.float32)
sigma = 3.
M = 100

samples, noise, mean_2 = sample(mean, sigma, M)

with tf.Session() as s:
    S = s.run(samples)

print np.mean(S)
