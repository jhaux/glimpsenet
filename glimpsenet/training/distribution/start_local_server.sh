#!/bin/bash
# Given the JOB, TASK and RESET arguments this script either starts a worker
# server, which executes training or a parameter_server.
# Currently only a single parameter server is supported.


JOB=$1
TASK=$2
RESET=$3


# Only set TF_CONFIG if running jobs, that use the cluster (eval does not)
if [[ ! $JOB = 'eval' ]]; then
    export TF_CONFIG=$(python get_environment_vars.py $JOB $TASK --tf_config)
fi

CVD=$(python get_environment_vars.py $JOB $TASK)
GF=$(python get_environment_vars.py $JOB $TASK --gpu_frac)
WS=$(python get_environment_vars.py $JOB $TASK --world_size)

if [[ ! $JOB = 'ps' ]]; then
        echo start local server
        if [[ ! -z $RESET ]]; then
                CUDA_VISIBLE_DEVICES="$CVD" ../../train.sh --r=$TASK --w=$WS --job=$JOB --reset=$RESET
        else    
                CUDA_VISIBLE_DEVICES="$CVD" ../../train.sh --r=$TASK --w=$WS --job=$JOB
        fi
fi
