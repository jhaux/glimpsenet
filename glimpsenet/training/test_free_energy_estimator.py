import unittest
from unittest import TestCase
import tensorflow as tf


class TestFree_energy_estimator(TestCase):

    def setUp(self):
        M = 10
        bs = 6
        n_classes = 10
        n_timesteps = 7

        self.py = tf.get_variable('py', [M, bs, n_classes])
        self.groundtruth = tf.get_variable('gt', [bs, n_classes])
        self.pl = tf.get_variable('pl', [M, bs, n_timesteps])
        self.R = tf.get_variable('R', [M, bs])
        self.b = tf.get_variable('b', [M, bs])
        self.l = 1

        super(TestFree_energy_estimator, self).setUp()

    def test_dimensions(self):
        self.assertEqual()

    def test_free_energy_estimator(self):
        self.fail()


if __name__ == '__main__':
    unittest.main()
