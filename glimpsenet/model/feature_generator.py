import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.utils.model_zoo as zoo
import numpy as np
from collections import OrderedDict
from scipy.misc import face
from glimpsenet.util.plotting import plot_activations


cfg = {
    'VGG16': [64, 64, 'M',
              128, 128, 'M',
              256, 256, 256, 'M',
              512, 512, 512, 'M',
              512, 512, 512, 'M'],
    'VGG19': [64, 64, 'M',
              128, 128, 'M',
              256, 256, 256, 256, 'M',
              512, 512, 512, 512, 'M',
              512, 512, 512, 512, 'M']}


model_urls = {
    'vgg16': 'https://download.pytorch.org/models/vgg16-397923af.pth',
    'vgg19': 'https://download.pytorch.org/models/vgg19-dcbb9e9d.pth',
    'vgg16_bn': 'https://download.pytorch.org/models/vgg16_bn-6c64b313.pth',
    'vgg19_bn': 'https://download.pytorch.org/models/vgg19_bn-c79401a0.pth',
}


class VGG16_conv_only(nn.Module):

    def __init__(self, cfg, in_channels):
        super(VGG16_conv_only, self).__init__()

        self.cfg = cfg
        in_ch = in_channels
        counter = 0
        for v in cfg:
            name = 'features.{}'.format(counter)
            if v == "M":
                setattr(self, name+'.m', nn.MaxPool2d(kernel_size=2, stride=2))
                counter += 1
            else:
                conv2d = nn.Conv2d(in_ch, v, kernel_size=3, padding=1)
                setattr(self, name, conv2d)
                counter += 1
                setattr(self, name+'.relu', nn.ReLU(inplace=True))
                counter += 1
                in_ch = v

    def forward(self, image, return_names=['max_pool_5']):
        activations = OrderedDict()
        activation = image
        counter = 0
        conv_counter = 1
        mp_counter = 1
        for v in self.cfg:
            name = 'features.{}'.format(counter)
            if v == 'M':
                activation = getattr(self, name+'.m')(activation)
                mp_name = 'max_pool_{}'.format(mp_counter)
                if mp_name in return_names:
                    activations[mp_name] = activation
                counter += 1
                mp_counter += 1
            else:
                preact = getattr(self, name)(activation)
                counter += 1
                activation = getattr(self, name+'.relu')(preact)
                counter += 1
                conv_name = 'conv_{}'.format(conv_counter)
                if conv_name in return_names:
                    activations[conv_name] = activation
                conv_counter += 1
        return activation, activations


class VGG16(VGG16_conv_only):

    def __init__(self):
        super(VGG16, self).__init__(cfg['VGG16'], 3)

        pretrained_weights = zoo.load_url(model_urls['vgg16'])
        pretrained_weights = {k: pretrained_weights[k] for k in
                              pretrained_weights.keys() if 'features' in k}
        self.load_state_dict(pretrained_weights)


def roi_pool(features, roi, out_size=[11, 11]):
    bs, n_ch, fh, fw = features.size()
    H, W = out_size
    hstart_init, hstopp_init, wstart_init, wstopp_init = roi.cpu().numpy()
    h = hstopp_init - hstart_init
    w = wstopp_init - wstart_init

    bin_size_h = float(h) / float(H)
    bin_size_w = float(w) / float(W)

    out = Variable(torch.zeros([bs, n_ch, H, W]))
    # print('o', out.size())
    for ph in range(H):
        hstart = int(np.floor(hstart_init + ph*bin_size_h))
        hstopp = int(np.ceil(hstart_init + (ph+1)*bin_size_h))
        for pw in range(W):
            wstart = int(np.floor(wstart_init + pw*bin_size_w))
            wstopp = int(np.ceil(wstart_init + (pw+1)*bin_size_w))

            # print('start stop', hstart, hstopp, wstart, wstopp)
            # print('f', fh, fw)

            window = features[:, :, hstart:hstopp, wstart:wstopp]
            max_pool = torch.max(torch.max(window, 3)[0], 2)[0]
            # print('m', max_pool.size())
            max_pool = max_pool.view(-1)

            out[0, :, ph, pw] = max_pool

    return out


if __name__ == '__main__':

    from data_preparation import ScatterGeneratorWithVGG as ScatterGenerator
    import argparse

    perser = argparse.ArgumentParser()
    perser.add_argument('--i', type=str, default='mm',
                        help='Image. Can be mm, test, face')
    perser.add_argument('--tv', action='store_true', default=False,
                        dest='test_vgg',
                        help='Image. Can be mm, test, face')

    args = perser.parse_args()

    vgg = VGG16_conv_only(cfg['VGG16'], 3).cuda()

    pretrained_weights = zoo.load_url(model_urls['vgg16'])
    pretrained_weights = {k: pretrained_weights[k] for k in
                          pretrained_weights.keys() if 'features' in k}
    vgg.load_state_dict(pretrained_weights)

    print(vgg)

    if args.i == 'mm':
        mm = ScatterGenerator('mnist', 2, 5)
        sample = next(mm)
        image = Variable(sample['features']).cuda()
    elif args.i == 'face':
        image = torch.from_numpy(face()).float().unsqueeze(0)
        image = Variable(image.permute(0, 3, 1, 2)[:, :, ::10, ::10]).cuda()
        print(image.size())
    elif args.i == 'test':
        imsize = 1, 1, 100, 100
        image = np.linspace(0, 1, num=np.prod(imsize))
        image = np.reshape(image, imsize)

        for i in range(4):
            image[..., i*25:i*25 + 12] \
                = image[..., i*25:i*25 + 12][::-1] ** (i+1)
            image[..., i*25+12:i*25 + 25] \
                = image[..., i*25+12:i*25 + 25][::1] - 0.1*i
        image = Variable(torch.from_numpy(image).float())
        image = image.expand(1, 3, 100, 100).cuda()
    else:
        raise ValueError('Incorrect argument i: {}. Must be eiher mm, face, i'
                         'test')

    if args.test_vgg:
        acts = ['conv_1']
        acts += ['conv_{}'.format(i) for i in range(0, 13)]
        out, activations = vgg(image, acts)

        x, y, ps = 0, 0, 12
        im_x, im_y = image.size()[2:4]
        roi = torch.FloatTensor([
            (x + im_x) / 2. - ps / 2,
            (x + im_x) / 2. + ps / 2,
            (y + im_y) / 2. - ps / 2,
            (y + im_y) / 2. + ps / 2
            ])
        # ROI strategy
        rois = OrderedDict()
        rois['orig_o'] = [image, roi]
        rois['orig'] = roi_pool(image, roi)
        for name, act in activations.items():
            scale_x = float(act.size()[2]) / float(image.size()[2])
            scale_y = float(act.size()[3]) / float(image.size()[3])

            print(scale_x, scale_y)

            roi_s = roi * torch.FloatTensor([scale_x, scale_x,
                                             scale_y, scale_y])
            rois[name + '_o'] = act
            rois[name] = roi_pool(act, roi_s)
        vmin = None  # float(torch.min(image).data.cpu().numpy())
        vmax = None  # float(torch.max(image).data.cpu().numpy())
        plot_activations(rois, vmin, vmax)

        # Upsampling strategy
        # for name, act in activations.items():
        #     act_new = nn.functional.upsample(act, [100, 100],
        #                                      mode='bilinear')
        #     image = torch.cat((image, act_new), dim=1)
        # # plot_activations(activations)
        # plot_activations({'combined': image})
    else:
        res = next(mm)
        plot_activations({'features': res['features']})
