import json
import logging
import numpy as np
import os
import tensorflow as tf
import time

from model.drama import DeepRecurrentAttentionModel as DRAM
from model.value_function import ValueEstimator
from model.sampler import ActionsSampler
from util.device_assignment import assign_to_device
from util.fileManagement import DirectoryManager
from util.summaries import store_summaries
from training.config import configure
from training.hooks.setup_session_run_hooks import get_eval_hooks, get_monitors
# from training.hooks.plots import PlotGenerator

import gym
import gym_multiMnist  # import needed to let gym know that the env exists


print '+++++++++++++++++++++++++++++++'
print 'Using tensorflow version', tf.__version__
print '+++++++++++++++++++++++++++++++'


logger = logging.getLogger(__name__)

# TF_CONFIG is not set when starting the evaluation experiment.
try:
    tf_config = os.environ['TF_CONFIG']
    tf_config = json.loads(tf_config) if tf_config else None
except KeyError:
    tf_config = None

Config = configure()

if tf_config:
    Config.setParameters({'type': tf_config['task']['type'],
                          'task': tf_config['task']['index'],
                          'cluster': tf_config['cluster']})


# Ensure that if resetting training only one new model directory is created
job = Config.getParameter('type')
task = Config.getParameter('task')

chief = int(task) == 0 and job == 'worker'
print 'CHIEF:', chief, job, task, int(task) == 0, job == 'worker'

if int(task) > 0 or job != 'worker':
    Config.setParameters({'reuse': True})

# Create or get directories
Dirs = DirectoryManager(Config)
print 'Checkpoint Directory is {}'.format(Dirs.ckpt_path)

# Create Session config -> Will be set by server!!
mem_fraction = Config.getParameter('gpuFraction')
gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=mem_fraction,
                            allow_growth=True)
sess_config = tf.ConfigProto(allow_soft_placement=True,
                             gpu_options=gpu_options)

# Set up server
server = tf.train.Server(tf_config['cluster'],
                         job_name=job,
                         task_index=int(task),
                         config=sess_config)

print 'target:', server.target


# Get hooks
monitors = []
if job == 'worker' and int(task) == 0:
    monitors = get_monitors(Config, Dirs)
print '+++++++++++++++++++++++++'
print monitors
print '+++++++++++++++++++++++++'
eval_hooks = get_eval_hooks(Config, Dirs)

# Ensure correct devide placement
dev = '/job:{}/replica:0/task:{}/gpu:0'.format(job, task)
var_dev = Config.getParameter('HyperParameters/variableDevice')

# Create the participants of the play
fss = Config.getParameter('HyperParameters/fixedStepSize')
step_size = Config.getParameter('HyperParameters/predSteps')
env_params = {
    'N_obj': 2,
    'difficulty': 5,
    'sorted_targets': True,
    'fss': None if not fss else step_size
    }

gym.envs.register(id='MultiMnist-v100',
                  entry_point='gym_multiMnist.envs:MultiMnistEnv',
                  kwargs=env_params)
env = gym.make('MultiMnist-v100')
Sampler = ActionsSampler(Config)
pred_thresh = Config.getParameter('predictionThreshold')

print 'Creating actor and critic'
with tf.device(assign_to_device(dev, var_dev)):
    actor = DRAM(Config)
    critic = ValueEstimator(Config)
    step = tf.train.get_or_create_global_step()
    increase_step_op = tf.assign(step, step + 1)

    saver = tf.train.Saver(tf.global_variables(), max_to_keep=10)

with tf.train.MonitoredTrainingSession(
        master=server.target,
        is_chief=chief,
        checkpoint_dir=Dirs.ckpt_path,
        chief_only_hooks=monitors,
        save_summaries_steps=None,
        save_summaries_secs=None) as mon_sess:

    print 'Starting Training'

    checkpoint = tf.train.latest_checkpoint(Dirs.ckpt_path)
    if checkpoint is not None and chief:
        saver.restore(mon_sess._sess._sess._sess._sess, checkpoint)
        print 'Restoring from {}'.format(checkpoint)

    gs_old = gs_init = mon_sess.run(step)
    steps_since_save = 0
    step_last_save = gs_old
    n_plots = 0

    t_start = time.time()
    # Training loop, written as explicit as possible, while staying as concise
    # as possible
    while not mon_sess.should_stop():
        t_start_loop = time.time()
        # Initialize environment and policy net
        low_res = env.reset()
        t_max = Config.getParameter('HyperParameters/maxIter')

        # Run exemplary episode
        actions_policy = []
        rs_policy = []
        vs_policy = []
        states_policy = []
        targets_policy = []
        preds_policy = []
        preds_ts_policy = []

        # Make initiale step by looking at downscaled version of the image
        action = actor.initialize_episode(low_res, mon_sess)
        state, r, terminal, _ = env.initial_step(action)
        V = critic(action['actor_state'], mon_sess)

        actions_policy.append(action)
        rs_policy.append(r)
        vs_policy.append(V)
        states_policy.append(state)

        # Walk the image. t = 0 was made when generating action_init
        t = 1
        while t < t_max:
            o_cum = action['pred_signal']
            action = actor.policy(o_cum, state, mon_sess)
            state, r, terminal, _ = env.step(action)
            V = critic(action['actor_state'], mon_sess)
            target = info['target']
            if target is not None:
                targets_policy.append(target)
                preds_policy.append(action['prediction'])
                pred_ts_policy.append(t)

            actions_policy.append(action)
            rs_policy.append(r)
            vs_policy.append(V)
            states_policy.append(state)

            t += 1
            if terminal:
                break

        # Sample and evaluate new episodes
        sampledAActions = Sampler(actions)  # actions for M episodes

        aactions = []
        rrs = []
        vvs = []
        sstates = []
        targets = []
        accuracies = []
        ppredictions = []
        pred_tss = []
        for m, sampled_actions in enumerate(sampledAActions):
            N_actions = len(sampled_actions)

            # Reset environment without switching the world (image)
            env.restart()

            actions = []  # Record actions again, as terminal can be met early
            rs = []
            states = []
            proposed_targets = []
            predictions = []
            pred_ts = []
            Vs = []

            # Make initial step by looking at downscaled version of the image
            action = actor.initialize_episode(low_res, mon_sess)
            state, r, terminal, _ = env.initial_step(sampled_actions[0])
            V = critic(action['actor_state'], mon_sess)

            actions.append(action)
            rs.append(r)
            states.append(state)
            Vs.append(V)
            t = 1
            # Walk it off boy
            while t < t_max:
                o_cum = action['pred_signal']
                action = actor.policy(o_cum, state,
                                      mon_sess,
                                      sampled_actions[t])
                state, r, terminal, info = env.step(action)
                V = critic(action['actor_state'], mon_sess)

                actions.append(action)
                rs.append(r)
                states.append(state)
                Vs.append(V)
                target = info['target']
                if target is not None:
                    proposed_targets.append(target)
                    predictions.append(action['prediction'])
                    pred_ts.append(t)

                t += 1
                if terminal or t >= N_actions:
                    break

            if len(proposed_targets) == 0:
                proposed_targets = [np.zeros([10])]
                print 'No predictions'

            aactions.append(actions)
            rrs.append(rs)
            vvs.append(Vs)
            sstates.append(states)
            targets.append(proposed_targets)
            ppredictions.append(predictions)
            pred_tss.append(pred_ts)
            accuracy_s = []
            for t, p in zip(proposed_targets, predictions):
                accuracy_s.append(float(np.argmax(t, -1) == np.argmax(p, -1)))
            accuracy_s = np.mean(accuracy_s)
            accuracies.append(accuracy_s if np.isfinite(accuracy_s) else 0)

            # Calculate discounted cumulative Returns for each episode
            R = 0 if terminal else critic(action['actor_state'], mon_sess)

            Rs = []
            discount = Config.getParameter('HyperParameters/discountFactor')
            for r in rs[::-1]:
                R += r + discount * R
                Rs.append(R)
            Rs = Rs[::-1]

            # Calculate gradients
            grad_pi = actor.calculate_gradients(Rs, Vs,
                                                actions,
                                                proposed_targets,
                                                states_policy,
                                                states,
                                                mon_sess)
            lstm_states = [a['actor_state'] for a in actions]
            grad_V = critic.calculate_gradients(lstm_states, Rs, mon_sess)

            # Accumulate Gradients
            if m == 0:
                grads_pi = grad_pi
                grads_V = grad_V
            else:
                grads_pi = {n: g + grad_pi[n] for n, g in grads_pi.iteritems()}
                grads_V = {n: g + grad_V[n] for n, g in grads_V.iteritems()}

        # average gradients
        grads_pi = {n: g / len(sampledAActions) for n, g
                    in grads_pi.iteritems()}
        grads_V = {n: g / len(sampledAActions) for n, g
                   in grads_V.iteritems()}

        # Apply gradients
        grad_summary = actor.apply_gradients(grads_pi, mon_sess)
        critic.apply_gradients(grads_V, mon_sess)

        accuracy = np.mean(accuracies)
        t_dur = time.time() - t_start

        _ = mon_sess.run(increase_step_op)
        gs_new = mon_sess.run(step)
        steps_since_save += gs_new - gs_old
        steps_since_plot += gs_new - gs_old
        sps = (gs_new - gs_init) / t_dur
        gs_old = gs_new
        print '{} - ep: {} - acc: {:3.2f} - {:3.2f} step/s'.format(
                time.strftime('%d-%H:%M:%S', time.gmtime(t_dur)),
                gs_new,
                accuracy,
                sps)

        if chief and steps_since_save >= 500:
            ckpt_path = saver.save(mon_sess._sess._sess._sess._sess,
                                   Dirs.ckpt_path+'/model.ckpt',
                                   global_step=gs_new)
            step_last_save = gs_new
            steps_since_save = 0
            print 'Saving checkpoint to {}'.format(ckpt_path)

        if chief and steps_since_plot >= 100:
            steps_since_plot = 0
            store_summaries(gs_new, time.time(), Dirs,
                            grads_pi,
                            actions_policy

                            )

        #     plotter = PlotGenerator(
        #          emissions,
        #          signals,
        #          predictions,
        #          predictions_ts,
        #          prediction_seqs=predictions,
        #          glimpses=sstates,
        #          values=rrs,
        #          baselines=vvs,
        #          images,
        #          low_res=low_res,
        #          target=targets,
        #          accuracy=accuracy,
        #          globalStep=gs_new,
        #          mode='train',
        #          config=Config,
        #          savepath=Dirs.ckpt_path,
        #          N_show=1)
