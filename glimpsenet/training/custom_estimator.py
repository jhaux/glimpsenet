import tensorflow as tf
from tensorflow.contrib.learn.python.learn.estimators \
        import model_fn as model_fn_lib
from tensorflow.contrib.learn.python.learn.estimators \
        import run_config as run_config_lib
import logging

from model.drama import DeepRecurrentAttentionModel
from model.evaluator import Evaluator
from model.baseline import Baseline
from training.optimizers import MyAdam
from util.device_assignment import assign_to_device
from util.summary_helper import getSummaries
from util.initializer import init_from_checkpoint

logger = logging.getLogger(__name__)


def make_estimator(Config, ckpt_path):
    runConfig = tf.contrib.learn.RunConfig(
                save_summary_steps=100,
                gpu_memory_fraction=Config.getParameter('gpuFraction',
                                                        dictName='Training'),
                save_checkpoints_steps=500,
                save_checkpoints_secs=None,
                keep_checkpoint_every_n_hours=2,
                keep_checkpoint_max=None,
                )

    # Hacking jit options into the config
    # runConfig._tf_config.graph_options.optimizer_options.global_jit_level = \
    #         tf.OptimizerOptions.ON_1

    runConfig._tf_config.allow_soft_placement = True
    # runConfig._tf_config.gpu_options.allow_growth = True
    runConfig._environment = run_config_lib.Environment.CLOUD

    logger.debug('RunConfig.tf_config: {}'.format(runConfig.tf_config))

    estimator = tf.contrib.learn.Estimator(
            model_fn=make_model_fn(Config),
            model_dir=ckpt_path,
            config=runConfig)

    return estimator


def make_model_fn(Config):
    def model_fn(features,
                 targets,
                 mode=None,
                 params=None,
                 config=None,
                 model_dir=None):

        logger.info("Constructing {}-mode".format(mode))

        task = Config.getParameter('task')
        job = Config.getParameter('type')
        job = 'localhost' if job == 'eval' else job
        dev = '/job:{}/replica:0/task:{}/gpu:0'.format(job, task)
        var_dev = Config.getParameter('HyperParameters/variableDevice')

        with tf.device(assign_to_device(dev, var_dev)):
            step = tf.train.get_global_step()

            DRAM = DeepRecurrentAttentionModel(Config, mode)

            B = Baseline(Config)
            E = Evaluator(DRAM, B, Config, mode)

            if mode == tf.contrib.learn.ModeKeys.TRAIN:
                L_model, L_baseline, accuracy, _ \
                    = E.evaluate_batch(features, targets)

                L_model = tf.Print(L_model, [L_model], 'L_model: ')
                L_baseline = tf.Print(L_baseline, [L_baseline], 'L_baseline: ')

                tf.summary.scalar('loss', L_model)

                def decay(learning_rate, step):
                    return tf.train.exponential_decay(
                        learning_rate=learning_rate,
                        global_step=step,
                        decay_steps=1000,
                        decay_rate=Config.getParameter('learningDecay'),
                        name='rate')

                model_train_op = tf.contrib.layers.optimize_loss(
                        loss=L_model,
                        learning_rate=Config.getParameter('learningRate'),
                        learning_rate_decay_fn=decay,
                        global_step=step,
                        optimizer=MyAdam,
                        summaries=getSummaries(Config),
                        colocate_gradients_with_ops=True)

                baseline_train_op = tf.contrib.layers.optimize_loss(
                        loss=L_baseline,
                        learning_rate=Config.getParameter('learningRateBl'),
                        global_step=step,
                        optimizer=MyAdam,
                        summaries=getSummaries(Config),
                        variables=tf.get_collection('BASELINE'),
                        colocate_gradients_with_ops=True)

                train_op = tf.group(model_train_op, baseline_train_op)

            else:
                L_model, L_baseline, accuracy, plotting_tensors_tas \
                    = E.evaluate_batch(features, targets)

                L_model = tf.Print(L_model, [L_model], 'L_model: ')
                L_baseline = tf.Print(L_baseline, [L_baseline], 'L_baseline: ')

                tf.summary.scalar('loss', L_model)

                train_op = None

            # Init from Checkpoint => Somehow Does noting yet
            restore_path = Config.getParameter('restorePath')
            if restore_path is not None:
                init_from_checkpoint(restore_path, Config)

            ModelFnOps = model_fn_lib.ModelFnOps(
                    mode,
                    predictions={'accuracy': accuracy},
                    loss=L_model,
                    train_op=train_op)

        return ModelFnOps
    return model_fn
