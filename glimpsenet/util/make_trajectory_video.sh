#!/bin/bash
cd /media/johannes/Data\ 1/Masterarbeit/Trainings/
cd $(find . -maxdepth 2 -type d -exec stat -c "%y %n" {} + | sort -r | head -n1 | cut -d " " -f 4-)
cd plots/eval/trajectories

for i in $(seq 0 9); do 
        ffmpeg -i "eval - step %* traj-00$i.png" -y -framerate 1/5 -r 25 00_learning_process_traj-$i.mp4
done
