import tensorflow as tf


def init_from_checkpoint(checkpoint_path, Config):
    '''Uses the checkpoint at checkpoint_path to initialize all weights, and
    all other variables randomly.

    Arguments:
        checkpoint_path: Well, you name it
        Config: Standart DramaConfig instance
    '''
    scope_dict = {
        'DRAM/':
        'evaluate_batch/record_epsiode/',
        'getLoss/sample_and_evaluate/evaluateTrajectories/':
        'evaluate_batch/evaluate_episodes/',
        'OptimizeLoss/DRAM':
        'OptimizeLoss/evaluate_batch/record_epsiode/'
    }

    var_dict = {
        'DRAM/Generative_Model/ClassificationNet/fc_1/biases':
        'evaluate_batch/record_epsiode/Generative_Model/ClassificationNet/fc_1/biases',
        'DRAM/Generative_Model/ClassificationNet/fc_1/weights':
        'evaluate_batch/record_epsiode/Generative_Model/ClassificationNet/fc_1/weights',
        'DRAM/Generative_Model/ClassificationNet/fc_2/biases':
        'evaluate_batch/record_epsiode/Generative_Model/ClassificationNet/fc_2/biases',
        'DRAM/Generative_Model/ClassificationNet/fc_2/weights':
        'evaluate_batch/record_epsiode/Generative_Model/ClassificationNet/fc_2/weights',
        'DRAM/Generative_Model/EmissionNet/fc1/biases':
        'evaluate_batch/record_epsiode/Generative_Model/EmissionNet/fc1/biases',
        'DRAM/Generative_Model/EmissionNet/fc1/weights':
        'evaluate_batch/record_epsiode/Generative_Model/EmissionNet/fc1/weights',
        'DRAM/Generative_Model/GlimpseNet/Emission_Net/fc/biases':
        'evaluate_batch/record_epsiode/Generative_Model/GlimpseNet/Emission_Net/fc/biases',
        'DRAM/Generative_Model/GlimpseNet/Emission_Net/fc/weights':
        'evaluate_batch/record_epsiode/Generative_Model/GlimpseNet/Emission_Net/fc/weights',
        'DRAM/Generative_Model/GlimpseNet/Image_Net/conv_1/biases':
        'evaluate_batch/record_epsiode/Generative_Model/GlimpseNet/Image_Net/conv_1/biases',
        'DRAM/Generative_Model/GlimpseNet/Image_Net/conv_1/weights':
        'evaluate_batch/record_epsiode/Generative_Model/GlimpseNet/Image_Net/conv_1/weights',
        'DRAM/Generative_Model/GlimpseNet/Image_Net/conv_2/biases':
        'evaluate_batch/record_epsiode/Generative_Model/GlimpseNet/Image_Net/conv_2/biases',
        'DRAM/Generative_Model/GlimpseNet/Image_Net/conv_2/weights':
        'evaluate_batch/record_epsiode/Generative_Model/GlimpseNet/Image_Net/conv_2/weights',
        'DRAM/Generative_Model/GlimpseNet/Image_Net/conv_3/biases':
        'evaluate_batch/record_epsiode/Generative_Model/GlimpseNet/Image_Net/conv_3/biases',
        'DRAM/Generative_Model/GlimpseNet/Image_Net/conv_3/weights':
        'evaluate_batch/record_epsiode/Generative_Model/GlimpseNet/Image_Net/conv_3/weights',
        'DRAM/Generative_Model/GlimpseNet/Image_Net/fc/biases':
        'evaluate_batch/record_epsiode/Generative_Model/GlimpseNet/Image_Net/fc/biases',
        'DRAM/Generative_Model/GlimpseNet/Image_Net/fc/weights':
        'evaluate_batch/record_epsiode/Generative_Model/GlimpseNet/Image_Net/fc/weights',
        'DRAM/Generative_Model/LSTM1/lstm_cell/bias':
        'evaluate_batch/record_epsiode/Generative_Model/LSTM1/lstm_cell/bias',
        'DRAM/Generative_Model/LSTM1/lstm_cell/kernel':
        'evaluate_batch/record_epsiode/Generative_Model/LSTM1/lstm_cell/kernel',
        'DRAM/Generative_Model/LSTM2/lstm_cell/bias':
        'evaluate_batch/record_epsiode/Generative_Model/LSTM2/lstm_cell/bias',
        'DRAM/Generative_Model/LSTM2/lstm_cell/kernel':
        'evaluate_batch/record_epsiode/Generative_Model/LSTM2/lstm_cell/kernel',
        'getLoss/sample_and_evaluate/evaluateTrajectories/Baseline/fc1/biases':
        'evaluate_batch/evaluate_episodes/Baseline/fc1/biases',
        'getLoss/sample_and_evaluate/evaluateTrajectories/Baseline/fc1/weights':
        'evaluate_batch/evaluate_episodes/Baseline/fc1/weights',
        'getLoss/sample_and_evaluate/evaluateTrajectories/Baseline/fc2/biases':
        'evaluate_batch/evaluate_episodes/Baseline/fc2/biases',
        'getLoss/sample_and_evaluate/evaluateTrajectories/Baseline/fc2/weights':
        'evaluate_batch/evaluate_episodes/Baseline/fc2/weights'
    }

    if Config.getParameter('type') != 'eval':
        scope_dict.update({})

    tf.contrib.framework.init_from_checkpoint(checkpoint_path, var_dict)
    print 'Should now initialize from {}'.format(checkpoint_path)
