import argparse                 # run this script from the command line


def getParser():
    # initialize Parser
    # User explicit formatter_class to get right indentation when encountering
    # newlines
    parser = argparse.ArgumentParser(
            description='GlimpseNet model v4',
            formatter_class=argparse.RawTextHelpFormatter)

    # Add the arguments!

    # model name
    parser.add_argument('--baseName', type=str,
                        dest='baseName',
                        help='basename used for creating folders and saving '
                             'files')
    # number of epochs
    parser.add_argument('--numEpochs', type=int,
                        dest='numEpochs',
                        help='number of epochs for training')
    # Number of prediction steps
    parser.add_argument('--predSteps', type=int,
                        dest='predSteps',
                        help='Number of steps before prediction is made')
    # Max number of Objects to discover
    parser.add_argument('--maxObjs', type=int,
                        dest='maxObjs',
                        help='Max expected number of objects to detect (incl. '
                        '<EOS>)')
    # Batch size
    parser.add_argument('--batchSize', type=int,
                        dest='batchSize',
                        help='Number of images per batch')
    # Patch size
    parser.add_argument('--patchSizes', nargs='+', type=int,
                        dest='patchSizes',
                        help='edge-lengths of the patches generated every '
                             'timestep')
    # Number of locations to generate
    parser.add_argument('--numLocations', type=int,
                        dest='numLocations',
                        help='Number of location tuples predicted per '
                             'timestep')
    # Learning rate
    parser.add_argument('--learningRate', type=float,
                        dest='learningRate',
                        help='Learning Rate for the adam optimizer')
    # Learning decay
    parser.add_argument('--learningDecay', type=float,
                        dest='learningDecay',
                        help='Learning rate decay for the adam optimizer')
    # Dataset
    parser.add_argument('--dset', type=str,
                        dest='dset',
                        help='Name of dataset.\nCan be:\n'
                        'MM or Multi_MNIST\n'
                        'M or MNIST\n'
                        'tM or transatedMNIST')

    parser.add_argument('--subset', type=str,
                        dest='subset',
                        help='Name of subset in dataset.\nCan be:\n'
                        'small[-sort]\n'
                        'complete[-sort]\n'
                        'mini[-sort]')

    parser.add_argument('--difficulty', type=int,
                        dest='difficulty',
                        help='Difficulty of curriculum dataset.')

    # Latest Checkpoint
    parser.add_argument('--reuse', type=str,
                        dest='reuse',
                        help='Index or bool to reuse checkpoint.\n'
                        'Can be:\n'
                        'True: latest checkpoint will be continued\n'
                        'False: new checkpoint with own directory will be '
                        'created\n'
                        '[0-9]*: integer of previously created checkpoint '
                        'directories.')

    # Verbose Level
    parser.add_argument('--verbose', type=int,
                        dest='verbose',
                        help='Define, what should be tracked (Higher levels '
                        'add information. 2 also has 1 and 0):\n'
                        '1: Cost, Accuracy, Learning rate\n'
                        '2: Gradients')

    # specify config file
    parser.add_argument('--archi', type=str,
                        dest='archi',
                        help='Specify a architecture config file. Can be:\n'
                        'None: default config is loaded\n'
                        '<path>: config file at <path> is loaded')

    # specify train config file
    parser.add_argument('--train', type=str,
                        dest='train',
                        help='Specify a train config file. Can be:\n'
                        'None: default config is loaded\n'
                        '<path>: train config file at <path> is loaded')

    # specify hyper parameter config file
    parser.add_argument('--hyper', type=str,
                        dest='hyper',
                        help='Specify a hyperparameter config file. Can be:\n'
                        'None: default config is loaded\n'
                        '<path>: train config file at <path> is loaded')

    # Sanity check
    parser.add_argument('--sanity',
                        dest='sanity',
                        action='store_true',
                        help='Only use a subset of the data to test if '
                        'overfitting is possible')

    # Configure Output to console
    parser.add_argument('--debug', type=int,
                        dest='debug',
                        default=1,
                        help='configure the output to the console.\n'
                        'Can be:\n'
                        '0: Only Errors\n'
                        '1: Also Info statements\n'
                        '2: Also Debug statements\n')

    # Schedule parameter for tensorflows learn runner
    parser.add_argument('--schedule', type=str,
                        dest='schedule',
                        default='train_and_evaluate',
                        help='Schedule for tf.contrib.learn\'s train_runner.\n'
                        'Can be:\n'
                        'train: calls Experiment.train()\n'
                        'evaulate: calls Experiment.evaluate()\n'
                        'train_and_evaluate: Calls train and everytime a '
                        'checkpoint is generated also evaluate.')

    parser.add_argument('--M', type=int,
                        dest='M',
                        default=10,
                        help='Number of samples to be drawn at each timestep')

    parser.add_argument('--sigma_em', type=int,
                        dest='sigma_em',
                        help='Stdev for the sampling of emissions.')

    parser.add_argument('--sigma_sig', type=float,
                        dest='sigma_sig',
                        help='Stdev for the sampling of signals.')

    parser.add_argument('--l', type=float,
                        dest='l',
                        help='Scaling factor for the reinforce gradient.')

    parser.add_argument('-vD', '--variableDevice', type=str,
                        dest='variableDevice',
                        help='Device where to put the variables')

    parser.add_argument('--gpuFraction', type=float,
                        dest='gpuFraction',
                        help='Fraction of gpu memory this run is allowed to '
                        'use')

    parser.add_argument('--cumulativeReward',
                        dest='cumulativeReward',
                        default=False,
                        action='store_true',
                        help='Toggle cumulative reward on or off')

    parser.add_argument('--stepWisePredictionOff',
                        dest='stepWisePrediction',
                        default=True,
                        action='store_false',
                        help='Toggle stepwise predictions on or off')

    parser.add_argument('--type', type=str,
                        dest='type',
                        help='Job name. Can be "ps" or "worker"')

    parser.add_argument('--task', type=int,
                        dest='task',
                        help='Task index.')

    parser.add_argument('--restore_path', type=str,
                        dest='restorePath',
                        help='Optional path to restore weights from')

    parser.add_argument('--lstmInit', type=str,
                        dest='lstmInit',
                        help='Determines if top lstm is initialized with '
                        'zeros of by the context network. Can be\n'
                        '- zeros\n'
                        '- anything else\n')

    parser.add_argument('--no-async',
                        dest='async',
                        default=True,
                        action='store_false',
                        help='Turn off parallelized training')

    parser.add_argument('--no-fss',
                        dest='fixedStepSize',
                        default=True,
                        action='store_false',
                        help='Turn fixed number of prediction steps of')

    parser.add_argument('--ps-fixed',
                        dest='psFixed',
                        default=False,
                        action='store_true',
                        help='Turn of learnable patch size')

    parser.add_argument('--r', type=int, help='rank')
    parser.add_argument('--ws', type=int, help='world size')
    parser.add_argument('--job', type=str, help='job')

    return parser
