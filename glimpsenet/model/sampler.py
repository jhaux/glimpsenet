import numpy as np


def gt0(arr):
    '''Clips array 'arr' to values greater than zero.'''
    return np.clip(arr, 0, None)


class ActionsSampler(object):

    def __init__(self, Config):
        self.Config = Config
        getP = Config.getParameter
        self.sigma_em = getP('HyperParameters/sigma_em')
        self.sigma_sig = getP('HyperParameters/sigma_sig')
        self.M = getP('HyperParameters/M')
        self.pred_thresh = getP('predictionThreshold')

    def __call__(self, actions):
        mean_x, mean_y = [], []
        mean_ps = []
        mean_o = []
        for action in actions:
            x, y = action['location']
            ps = action['patch_size']
            o = action['pred_signal_increment']
            mean_x.append(x), mean_y.append(y)
            mean_ps.append(ps)
            mean_o.append(o)

        sigma_x = sigma_y = sigma_ps = self.sigma_em
        sigma_o = self.sigma_sig

        sampled_x = np.random.normal(mean_x, sigma_x,
                                     size=[self.M] + list(np.shape(mean_x)))
        sampled_y = np.random.normal(mean_y, sigma_y,
                                     size=[self.M] + list(np.shape(mean_y)))
        sampled_ps = np.random.normal(mean_ps, sigma_ps,
                                      size=[self.M] + list(np.shape(mean_ps)))
        sampled_o = np.random.normal(mean_o, sigma_o,
                                     size=[self.M] + list(np.shape(mean_o)))

        sampled_actions = []
        # Iterate over samples
        for action_seq \
                in zip(sampled_x, sampled_y, sampled_ps, sampled_o):
            seq = []
            # Iterate over time (need to transpose action_seq)
            for x, y, ps, o in np.array(action_seq).T:
                action = {
                    'location': [x, y],
                    'patch_size': max(1, ps),
                    'pred_signal_increment': o,
                    }
                seq.append(action)
            sampled_actions.append(seq)

        return sampled_actions
