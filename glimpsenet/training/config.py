import json
import os
import logging
import numpy as np
from util.trainParser import getParser

logger = logging.getLogger('config')

# Explicitly write down the architecture of the network to later fine tune the
# parameters and make it reusable
numLocs = 1
predSteps = 6
patchSizes = [5]
featureDim = 64
maxObjs = 1
M = 10
bs = 10
n_timesteps = predSteps * maxObjs
defaultArchitectureDict = {
        'predictionThreshold': 10.,
        'eosThreshold': 10.,
        'GlimpseNet':
        {
            'train': True,
            'ImageNet':
            {
                'train': True,
                'conv1':
                {
                    'n_in': 1,
                    'n_out': 64,
                    'fs': 3,
                    'act': 'elu',
                    'train': True,
                    'W_init': 'xavier_conv',
                    'b_init': 'zeros',
                    'reg': ['L2', 0.1]
                    },
                'conv2':
                {
                    'n_in': 64,
                    'n_out': 32,
                    'fs': 2,
                    'act': 'elu',
                    'train': True,
                    'W_init': 'xavier_conv',
                    'b_init': 'zeros',
                    'reg': ['L2', 0.1]
                    },
                'conv3':
                {
                    'n_in': 32,
                    'n_out': 16,
                    'fs': 2,
                    'act': 'elu',
                    'train': True,
                    'W_init': 'xavier_conv',
                    'b_init': 'zeros',
                    'reg': ['L2', 0.1]
                    },
                'fc1':
                {
                    'n_in': 16 * 7 * 7,
                    'n_out': featureDim,
                    'act': 'elu',
                    'train': True,
                    'W_init': 'xavier',
                    'b_init': 'zeros',
                    'reg': ['L2', 0.1]
                    }
                },
            'LocationNet':
            {
                'train': True,
                'fc1':
                {
                    'n_in': 3,
                    'n_out': featureDim,
                    'act': 'elu',
                    'train': True,
                    'W_init': 'xavier',
                    'b_init': 'zeros',
                    'reg': ['L2', 0.1]
                    }
                }
            },
        'EmissionNet':
        {
            'train': True,
            'pixel_ratio': 0.9,
            'unit_pixels': 0.9*28.,
            'signalScale': 10,
            'fc1':
            {
                'n_in': featureDim,
                'n_out': 100,
                'act': 'elu',
                'train': True,
                'W_init': 'xavier',
                'b_init': 'zeros',
                'reg': ['L2', 0.1]
                },
            'fc2':
            {
                'n_in': 100,
                'n_out': 3*numLocs,  # learnable patch size
                'act': None,
                'train': True,
                'W_init': 'xavier',
                'b_init': 'zeros',
                'reg': ['L2', 0.1]
                }
            },
        'ContextNet':
        {
            'image_net':
            {
                'train': True,
                'conv1':
                {
                    'n_in': 1,
                    'n_out': 32,
                    'fs': 5,
                    'act': 'elu',
                    'train': True,
                    'W_init': 'xavier_conv',
                    'b_init': 'zeros',
                    'reg': ['L2', 0.1]
                    },
                'conv2':
                {
                    'n_in': 32,
                    'n_out': 32,
                    'fs': 3,
                    'act': 'elu',
                    'train': True,
                    'W_init': 'xavier_conv',
                    'b_init': 'zeros',
                    'reg': ['L2', 0.1]
                    },
                'conv3':
                {
                    'n_in': 32,
                    'n_out': 16,
                    'fs': 3,
                    'act': 'elu',
                    'train': True,
                    'W_init': 'xavier_conv',
                    'b_init': 'zeros',
                    'reg': ['L2', 0.1]
                    },
                'fc1':
                {
                    'n_in': 16 * 17 * 17,
                    'n_out': featureDim,
                    'act': 'elu',
                    'train': True,
                    'W_init': 'xavier',
                    'b_init': 'zeros',
                    'reg': ['L2', 0.1]
                    }
                },
            'size_net':
            {
                'fc1':
                {
                    'n_in': 2,
                    'n_out': int(np.sqrt(featureDim)),
                    'act': 'elu',
                    'train': True,
                    'W_init': 'xavier',
                    'b_init': 'zeros',
                    'reg': ['L2', 0.1]
                    },
                'fc2':
                {
                    'n_in': int(np.sqrt(featureDim)),
                    'n_out': featureDim,
                    'act': 'elu',
                    'train': True,
                    'W_init': 'xavier',
                    'b_init': 'zeros',
                    'reg': ['L2', 0.2]
                    }
                }
            },
        'ClassificationNet':
        {
            'train': True,
            'fc1':
            {
                'n_in': featureDim,
                'n_out': 100,
                'act': 'elu',
                'train': True,
                'W_init': 'xavier',
                'b_init': 'zeros',
                'reg': ['L2', 0.1]
                },
            'fc2':
            {
                'n_in': 100,
                'n_out': 10,
                'act': None,
                'train': True,
                'W_init': 'xavier',
                'b_init': 'zeros',
                'reg': ['L2', 0.1]
                },
            },
        'Baseline':
        {
            'fc1':
            {
                'n_in': featureDim,
                'n_out': int(np.sqrt(featureDim)),
                'act': 'elu',
                'train': True,
                'W_init': 'xavier',
                'W_reg': ['L2', 0.1],
                'b_init': 'zeros',
                'b_reg': ['L2', 0.1]
                },
            'fc2':
            {
                'n_in': int(np.sqrt(featureDim)),
                'n_out': 1,
                'act': 'sigmoid',
                'train': True,
                'W_init': 'xavier',
                'W_reg': ['L2', 0.1],
                'b_init': 'zeros',
                'b_reg': ['L2', 0.1],
                },
            'train': True,
            'scale': 1
            },
        }

defaultHyperparameterDict = {
        'HyperParameters':
        {
            'numLocations': numLocs,
            'patchSizes': patchSizes,
            'psFixed': False,
            'maxObjs': maxObjs,
            'predSteps': predSteps,
            'n_timesteps': n_timesteps,
            'inputDim': [100, 100, 1],
            'imsize': [100, 100],
            'lowResDim': [25, 25, 1],
            'glimpseDim': [11, 11, 1],
            'featureDim': featureDim,
            'nLstmUnits': featureDim,
            'M': M,
            'sigma_em': 20.,
            'sigma_sig': 1.,
            'l': 0.01,
            'cumulativeReward': True,
            'gpuWeights': {0: 0.6, 1: 0.4},
            'variableDevice': '/job:ps/replica:0/task:0/cpu:0',
            'lstmInit': 'zeros',
            'maxIter': 20,
            'discountFactor': 0.9,
            'timePenalty': 0.4,
            'fixedStepSize': True
            },
        'Dataset':
        {
            'dset': 'M',
            'subset': 'complete-sort',
            'difficulty': 0
            },
        'GlimpseGeneration':
        {
            'centered': True,
            'normalized': False
            },
        'Names':
        {
            'prediction': 'prediction',
            'prediction_eval': 'DRAM/prediction',
            'prediction_sequence': 'DRAM/Generative_Model/prediction_sequence',
            'emission_sequence': 'DRAM/emissions',
            'location_sequence': 'DRAM/trajectory',
            'patch_sequence': 'DRAM/patchSizes',
            'labels': 'input_fn/targets',
            'labels_key': '__target_key__',  # As defined in numpy_op.oy
            'input_original': 'DRAM/input_original'
            }
}

defaultTrainDict = {
        'numEpochs': None,
        'learningRate': 1e-4,
        'learningRateBl': 1e-4,
        'learningDecay': 0.97,
        'batchSize': bs,
        'baseName': '/media/johannes/Data 1/Masterarbeit/Trainings/gn_v4',
        'verbose': 2,
        'reuse': True,
        'sanity': False,
        'shuffle': True,
        'schedule': 'train_and_evaluate',
        'gpuFraction': 0.3,
        'stepWisePrediction': True,
        'type': 'worker',
        'task': 0,
        'cluster': None,
        'restorePath': None,
        'async': True,
        'r': None,
        'ws': 3,
        'job': 'worker'
        }


class DramaConfig(object):
    '''Config class, to store learning parameters, architecture and paths.
    Saves a log file to make things reusable'''

    def __init__(self,
                 architectureFile=None,
                 trainFile=None,
                 hyperParameterFile=None):
        '''Initialize a config object based on a config file if provided.

        Arguments:
            configFile: path/to/json-file with a dict, that contains all
                        parameters and so on...
        '''

        if architectureFile is not None:
            with open(architectureFile, 'r') as file:
                self.setArchitectureDict(json.load(file))
        else:
            self.setArchitectureDict(defaultArchitectureDict)

        if trainFile is not None:
            with open(trainFile, 'r') as file:
                self.setTrainDict(json.load(file))
        else:
            self.setTrainDict(defaultTrainDict)

        if hyperParameterFile is not None:
            with open(hyperParameterFile, 'r') as file:
                self.setHyperParameterDict(json.load(file))
        else:
            self.setHyperParameterDict(defaultHyperparameterDict)

    def setArchitectureDict(self, architecture):
        self.architecture = architecture

    def setTrainDict(self, train):
        self.trainDict = train

    def setHyperParameterDict(self, hyperParameters):
        self.hyperParameterDict = hyperParameters

    def setParameters(self, parameterDict, update=None):
        for param, value in parameterDict.items():
            # To accsess nested dicts specify "path/to/dict" and recusively
            # call the dicts until at end, then set parameter
            paramPath = param.split('/')
            if update == 'Architecture':
                key = self.architecture
            elif update == 'Training':
                key = self.trainDict
            elif update == 'HyperParameters':
                key = self.hyperParameterDict
            elif update is None:
                primaryKey = paramPath[0]
                found = False
                if primaryKey in self.architecture.keys():
                    key = self.architecture
                    unique = not found
                    found = True
                if primaryKey in self.trainDict.keys():
                    key = self.trainDict
                    unique = not found
                    found = True
                if primaryKey in self.hyperParameterDict.keys():
                    key = self.hyperParameterDict
                    unique = not found
                    found = True

                if not found:
                    raise ValueError('No update supplied, but key \'{}\' not '
                                     'found'.format(primaryKey))
                elif not unique:
                    raise ValueError('No update supplied, but key \'{}\' was '
                                     'not unique.'.format(primaryKey))
            else:
                raise ValueError('update \'{}\' unknown. Can be either '
                                 'Architecture, Training, HyperParameters or '
                                 'None.'.format(parameterDict))

            for p in paramPath[:-1]:
                key = key[p]

            # update value
            logger.debug('key: {}'.format(key))
            logger.debug('paramPath[-1]: {}'.format(paramPath[-1]))
            key[paramPath[-1]] = value

    def getParameter(self, pathToPar, dictName=None):
        paramPath = pathToPar.split('/')
        if dictName == 'Architecture':
            val = self.architecture
        elif dictName == 'Training':
            val = self.trainDict
        elif dictName == 'HyperParameters':
            val = self.hyperParameterDict
        elif dictName is None:
            key = paramPath[0]
            found = False
            if key in self.architecture.keys():
                val = self.architecture
                unique = not found
                found = True
            if key in self.trainDict.keys():
                val = self.trainDict
                unique = not found
                found = True
            if key in self.hyperParameterDict.keys():
                val = self.hyperParameterDict
                unique = not found
                found = True

            if not found:
                raise ValueError('No dictName supplied, but key \'{}\' not '
                                 'found. Can be either Architecture, Training,'
                                 ' HyperParameters.'.format(key))
            elif not unique:
                raise ValueError('No dictName supplied, but key \'{}\' was not'
                                 ' unique.'.format(key))
        else:
            raise ValueError('dictName \'{}\' unknown. Can be either '
                             'Architecture, Training, HyperParameters or None.'
                             .format(dictName))

        for p in paramPath:
            val = val[p]
        return val

    def updateDependentParameters(self):
        ''' After setting parameters via e.g. commandline there might be
        inconsistencies. These are dealt with here'''

        # Get base values
        predSteps = self.getParameter('HyperParameters/predSteps')
        patchS = self.getParameter('HyperParameters/patchSizes')
        if type(patchS) is not list:
            patchS = [patchS]
        featureDim = self.getParameter('HyperParameters/featureDim')
        maxObjs = self.getParameter('HyperParameters/maxObjs')
        pixel_ratio = self.getParameter('EmissionNet/pixel_ratio')
        baseName = self.getParameter('baseName')
        dset = self.getParameter('Dataset/dset')
        job = self.getParameter('type')
        vd = self.getParameter('HyperParameters/variableDevice')

        ############################
        # calculate Update Values: #
        ############################

        nLstmUnits = featureDim

        bHiddenSize = int(np.sqrt(featureDim))

        # Input dimensions are dependend on dataset :D

        glimpse_dim = [11, 11]
        N_classes = 10
        if dset == 'MM':
            inputDim = [100, 100, 1]
            unit_pixels = pixel_ratio * 100.
            maxObjs = 2 if maxObjs <= 1 else maxObjs
        elif dset == 'M':
            inputDim = [28, 28, 1]
            unit_pixels = pixel_ratio * 28.
            maxObjs = 1
        elif dset == 'tM':
            inputDim = [100, 100, 1]
            unit_pixels = pixel_ratio * 100.
            maxObjs = 1
        elif dset == 'cM':
            inputDim = [100, 100, 1]
            unit_pixels = pixel_ratio * 100.
        elif dset == 'gM':
            inputDim = [100, 100, 1]
            unit_pixels = pixel_ratio * 100.
        elif dset == 'CelebsA':
            inputDim = [300, 300, 3]
            unit_pixels = pixel_ratio * 300.
        else:
            raise ValueError('Unknown Dataset {}'.format(dset))

        ctx_in = inputDim[-1]
        n_timesteps = maxObjs * predSteps

        # See if basename path exists up to last folder
        absBaseName = os.path.abspath(baseName)
        parDir = os.path.abspath(os.path.join(absBaseName, os.pardir))

        if not os.path.exists(parDir):
            baseName = os.path.basename(absBaseName)

        if job == 'eval':
            vd = vd.replace('ps', 'localhost')

        # Update Architecture parameters
        updateDict = {
                'GlimpseNet/ImageNet/fc1/n_out': featureDim,
                'GlimpseNet/LocationNet/fc1/n_out': featureDim,
                'ContextNet/image_net/fc1/n_out': nLstmUnits,
                'ContextNet/size_net/fc1/n_out': int(np.sqrt(nLstmUnits)),
                'ContextNet/size_net/fc2/n_out': nLstmUnits,
                'ClassificationNet/fc1/n_in': nLstmUnits,
                'EmissionNet/unit_pixels': unit_pixels,
                'Baseline/fc1/n_out': bHiddenSize,
                'HyperParameters/nLstmUnits': nLstmUnits,
                'HyperParameters/patchSizes': patchS,
                'HyperParameters/inputDim': inputDim,
                'HyperParameters/glimpseDim': glimpse_dim,
                'HyperParameters/n_timesteps': n_timesteps,
                'baseName': baseName,
                'HyperParameters/variableDevice': vd,
                'ContextNet/image_net/conv1/n_in': ctx_in
                # 'GlimpseNet/ImageNet/conv1/n_in': gls_in
                }

        logger.info('Updating dependent parameters:')
        logger.info('parameter: old ==> new')
        for key, val in updateDict.items():
            val_orig = self.getParameter(key)
            logger.info('{}: {} ==> {}'.format(key, val_orig, val))

        self.setParameters(updateDict)


# Helper function to load and manipulate Config with commandline inputs
def configure():
    ''' Load a config and update certain parameters based on commandline input

    Returns:
        config: config Class instance
    '''
    args = getParser().parse_args()

    architectureName = args.archi
    trainName = args.train
    hyperName = args.hyper

    if architectureName in ['M', 'MM']:
        architectureName = os.path.abspath("defaultConfigs/{}.json"
                                           .format(architectureName))

    config = DramaConfig(
            architectureFile=architectureName,
            trainFile=trainName,
            hyperParameterFile=hyperName)

    # Parse the arguments from the commandline and input those, which are not
    # None into the config, allowing high level parameters to be set from
    # the commandline.
    updateDict = getUpdateDict(vars(args))

    logger.debug('Update dict: {}'.format(updateDict))
    print('Update dict: {}'.format(updateDict))

    config.setParameters(updateDict)
    # To be sure no dependent parameters have been set wrongly
    config.updateDependentParameters()

    return config


def getUpdateDict(commandlineArgs):
    '''Prepare keys of commandlineArgs s.t. the update function can understand
    them'''

    trainKeys = [
            'numEpochs',
            'batchSize',
            'learningRate',
            'learningDecay',
            'verbose',
            'sanity',
            'schedule',
            'gpuFraction',
            'stepWisePrediction',
            'type',
            'task',
            'restorePath',
            'async',
            'baseName',
            'r',
            'ws',
            'job'
            ]
    hyperKeys = {
            'HyperParameters': [
                 'numLocations',
                 'patchSizes',
                 'psFixed',
                 'maxObjs',
                 'predSteps',
                 'M',
                 'sigma_em',
                 'sigma_sig',
                 'l',
                 'cumulativeReward',
                 'variableDevice',
                 'lstmInit',
                 'fixedStepSize'
             ],
            'Dataset': [
                'dset',
                'subset',
                'difficulty'
            ]
            }
    updateDict = {}

    # Special case reuse: can be bool or int but is supplied as string
    if 'reuse' in commandlineArgs.keys():
        reuse = commandlineArgs['reuse']
        if reuse is not None:
            if reuse == 'True':
                reuse = True
            elif reuse == 'False':
                reuse = False
            else:
                reuse = int(reuse)
        else:
            reuse = True
        updateDict['reuse'] = reuse

    for key, val in commandlineArgs.items():
        if key in trainKeys and val is not None:
            # key has already correct format assuming all primary keys are
            # unique
            updateDict[key] = val
            continue
        else:
            for prefix, matchList in hyperKeys.items():
                if key in matchList and val is not None:
                    print('===> {}={} <==='.format(key, val))
                    updateDict['{}/{}'.format(prefix, key)] = val
                    continue

    return updateDict


def safeDefaults():
    '''Store the default configurations!'''
    # Simple MNIST default config
    configM = DramaConfig()
    updateM = {'HyperParameters/numLocations': 1,
               'HyperParameters/patchSizes': [5],
               'HyperParameters/predSteps': 3,
               'HyperParameters/maxObjs': 1,
               'HyperParameters/inputDim': [28, 28, 1],
               'Dataset/dset': 'M'}
    configM.setParameters(updateM)
    configM.updateDependentParameters()

    with open('defaultConfigs/M.json', 'wb') as file:
        json.dump(configM.configDict, file, sort_keys=False, indent=4)

    # Multi MNIST default config
    configMM = DramaConfig()
    updateMM = {'HyperParameters/numLocations': 1,
                'HyperParameters/patchSizes': [9],
                'HyperParameters/predSteps': 3,
                'HyperParameters/maxObjs': 2,
                'HyperParameters/inputDim': [100, 60, 1],
                'Dataset/dset': 'MM'}
    configMM.setParameters(updateMM)
    configMM.updateDependentParameters()

    with open('defaultConfigs/MM.json', 'wb') as file:
        json.dump(configMM.configDict, file, sort_keys=False, indent=4)

    with open('defaultConfigs/Train.json', 'wb') as file:
        json.dump(configMM.trainDict, file, sort_keys=False, indent=4)


if __name__ == '__main__':
    try:
        os.mkdir('defaultConfigs')
    except OSError:
        print('Directory already created')
    safeDefaults()
