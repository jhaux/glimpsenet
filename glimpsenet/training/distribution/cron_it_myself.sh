#!/bin/bash

# Start and stop time for greedy (=using all available gpus) training
# in seconds
STARTTIME=$(date -d"$(date +%Y-%m-%d) 1:0:0" '+%s')
STOPTIME=$(date -d"$(date +%Y-%m-%d) 5:0:0" '+%s')

if [[ $STARTTIME -le $(date +%s) ]]; then
        STARTTIME=$(date -d "$(date -d "@$STARTTIME" '+%Y-%m-%d %H')+1 day" '+%s')  # in seconds
        STOPTIME=$(date -d "$(date -d "@$STOPTIME" '+%Y-%m-%d %H')+1 day" '+%s')  # in seconds
fi

echo Next Start: $(date -d "@$STARTTIME")
echo Next Stop: $(date -d "@$STOPTIME")

PASSIV=$1

if [[ $PASSIV = 'p' ]]; then
        echo No training started, assuming training is already running.
else
        ./resume_normal_training.sh $1 & PID_STOP=$!
fi

# Assumtion: You always start before wanting to do the greedy training
WAIT_FOR_GREEDY=true
WAIT_FOR_NORMAL=false

# outer loop: infinite
while [[ "true" = "true" ]]; do
        NOW=$(date +%s)
        if [[ $NOW -ge $STARTTIME && "$WAIT_FOR_GREEDY" = true ]]; then
                ./start_greedy_training.sh & PID_START=$!
                WAIT_FOR_GREEDY=false
                WAIT_FOR_NORMAL=true
                STARTTIME=$(date -d "$(date -d "@$STARTTIME" '+%Y-%m-%d %H')+1 day" +%s)
                kill $PID_STOP
                echo Next Start: $(date -d "@$STARTTIME")
                echo Next Stop: $(date -d "@$STOPTIME")

        elif [[ $NOW -ge $STOPTIME && "$WAIT_FOR_NORMAL" = true ]]; then
                ./resume_normal_training.sh & PID_STOP=$!
                WAIT_FOR_GREEDY=true
                WAIT_FOR_NORMAL=false
                STOPTIME=$(date -d "$(date -d "@$STOPTIME" '+%Y-%m-%d %H')+1 day" +%s)
                kill $PID_START
                echo Next Start: $(date -d "@$STARTTIME")
                echo Next Stop: $(date -d "@$STOPTIME")

        fi
    
        # Do this only once per minute
        sleep 60
done
