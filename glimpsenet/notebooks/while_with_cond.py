import tensorflow as tf


def cond(ts, *args):
    return ts < 10

def body(ts, ta, num_w):
    condition = ts > 4

    def true_fn(ta=ta, num_w=num_w):
        ta = ta.write(num_w, ts)
        num_w += 1
        return ta, num_w

    def false_fn(ta=ta, num_w=num_w):
        return ta, num_w

    ta, num_w = tf.cond(condition, true_fn, false_fn)

    ts += 1

    return ts, ta, num_w

ta = tf.TensorArray(tf.int32, size=1, dynamic_size=True)
n_ts, ta, n_w = tf.while_loop(cond, body, [0, ta, 0])
ta = ta.stack()

with tf.Session() as sess:
    N_ts, Ta, N_w = sess.run([n_ts, ta, n_w])

print N_ts, N_w
print Ta.shape
print Ta
