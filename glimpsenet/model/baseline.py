import tensorflow as tf
from buildingBlocks import baselineMapping
import logging

logger = logging.getLogger(__name__)


class Baseline(object):
    '''Expected reward calculated by mapping the hidden state of c^(2)
    to a scalar value and then regressing it every timestep towards the
    collected rewards.'''

    def __init__(self, Config):
        '''Set up the mapping NN.

        Arguments:
            c2_seqs: Tensor containing all hidden states of the top
            Config: DRAM-config containing all parameters of the basline
                network
            LSTM cell for all timesteps and samples. shape: [M, ts, locs, bs]
        '''

        self.Config = Config
        self.architecture = Config.architecture['Baseline']

        self.baselineMapping_tl = tf.make_template(
                'Baseline',
                baselineMapping,  # see above (imports)
                architecture=self.architecture)

    def mapTrajectory(self, c2_seq):
        c2_seq = tf.stop_gradient(c2_seq)
        N_ts = tf.shape(c2_seq)[0]

        def cond(ts, *args):
            return ts < N_ts

        def body(ts, baseline_seq):
            c2 = c2_seq[ts]
            raw_baseline = self.baselineMapping_tl(c2)

            baseline_seq = baseline_seq.write(ts, raw_baseline)
            ts += 1
            return ts, baseline_seq

        baseline_seq = tf.TensorArray(
                dtype=tf.float32,
                size=1,
                dynamic_size=True,
                clear_after_read=True,
                name='baseline_seq')
        ts = 0

        loop_results = tf.while_loop(cond, body, [ts, baseline_seq])
        _, baseline_seq = loop_results
        baseline_seq = baseline_seq.stack()
        # Last dimension is of size 1 => get rid of it
        baseline_seq = tf.squeeze(baseline_seq, axis=-1)
        baseline_seq.set_shape([None, None])

        return baseline_seq

    def addLoss(self, R, b):
        '''Add losses from several GPUs to be used by train_op

        Arguments:
            R: True rewards
            b: expected rewards
        '''

        targets = tf.stop_gradient(R)
        predictions = b
        self.loss = tf.reduce_mean(tf.square(targets - predictions))

    def getLoss(self, R, b):
        '''Get loss to be used by train_op

        Arguments:
            R: True rewards
            b: expected rewards
        '''

        with tf.variable_scope('baseline_loss'):
            targets = tf.stop_gradient(R)
            predictions = b
            return tf.reduce_mean(tf.square(targets - predictions))
