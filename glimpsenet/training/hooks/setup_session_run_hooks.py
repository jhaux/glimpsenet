from my_session_run_hooks import PlotHook
from tensorflow.python.training.basic_session_run_hooks \
        import CheckpointSaverHook, StepCounterHook


def get_plot_tensors(config):
    bs = config.getParameter('batchSize')
    tensors = {}
    for i in range(bs):
        tensors.update({
            'prediction_{}'.format(i):
                'evaluate_batch/prediction_{}'.format(i),
            'prediction_ts_{}'.format(i):
                'evaluate_batch/prediction_ts_{}'.format(i),
            'prediction_seq_{}'.format(i):
                'evaluate_batch/prediction_seq_{}'.format(i),
            'emission_{}'.format(i): 'evaluate_batch/emission_{}'.format(i),
            'signal_{}'.format(i): 'evaluate_batch/signal_{}'.format(i),
            'c2_seq_{}'.format(i): 'evaluate_batch/c2_seq_{}'.format(i),
            'sig_single_{}'.format(i):
                'evaluate_batch/sig_single_{}'.format(i),
            'glimpses_{}'.format(i): 'evaluate_batch/glimpses_{}'.format(i),
            'value_{}'.format(i): 'evaluate_batch/value_{}'.format(i),
            'baseline_{}'.format(i): 'evaluate_batch/baseline_{}'.format(i),
            'image_{}'.format(i): 'evaluate_batch/image_{}'.format(i),
            'low_res_{}'.format(i): 'evaluate_batch/low_res_{}'.format(i),
            'target_{}'.format(i): 'evaluate_batch/target_{}'.format(i)
            })

    tensors.update({'accuracy': 'evaluate_batch/accuracy_1'})
    tensors.update({'step': 'global_step'})

    return tensors


def get_monitors(config, Dirs):
    '''Setup all training monitors and return as list'''
    monitors = []

    # tensors = get_plot_tensors(config)
    # trainPH = PlotHook(tensors,
    #                    config.getParameter('Names/input_original'),
    #                    config.getParameter('Names/labels'),
    #                    config,
    #                    Dirs.ckpt_path,
    #                    every_n=500,
    #                    mode='train',
    #                    N_show=10)
    # monitors.append(trainPH)

    # monitors.append(CheckpointSaverHook(Dirs.ckpt_path, save_steps=5))
    monitors.append(StepCounterHook(every_n_steps=10))

    return monitors


def get_eval_hooks(config, Dirs):
    '''Setup all eval hooks and return as list.'''

    eval_hooks = []

    tensors = get_plot_tensors(config)
    testPH = PlotHook(tensors,
                      config,
                      Dirs.ckpt_path,
                      every_n=None,
                      mode='eval',
                      N_show=10)
    eval_hooks.append(testPH)

    return eval_hooks
