import numpy as np
import pandas as pd
import socket
import os
import json
import argparse


def _get_ip():
    '''Get the ip address of this machine and return it as string'''
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    ip_adress = str(s.getsockname()[0])
    s.close()

    return ip_adress


def load_config_from_file(config_file='cluster_spec.txt'):
    '''Loads a config file and prepares it to be printed to stdout'''
    names = ['account', 'address', 'port', 'job', 'task_index',
             'CVD', 'gpu_frac', 'home', 'preamble']
    config = pd.read_table(config_file,
                           dtype=str,
                           comment='#',
                           delimiter='|',
                           skipinitialspace=True,
                           names=names)
    for name in names:
        config[name] = config[name].str.strip()

    return config


def replace_with_localhost(config, job, task):
    for i, job in enumerate(config['job']):
        if job == job:
            if config['task_index'][i] == task:
                location = config['address'][i]
                break

    for i, address in enumerate(config['address']):
        if address == location:
            config['address'][i] = 'localhost'

    return config


def make_tf_config(config, local_job, local_task):

    # remove eval from config, as it is running independently from the rest of
    # the cluster
    config = config[config['job'] != 'eval']

    cluster = {job_type: [] for job_type in np.unique(config['job'])}
    for i, address in enumerate(config['address']):
        job = config['job'][i]
        port = config['port'][i]
        cluster[job].append('{}:{}'.format(address, port))

    task = {'type': local_job, 'index': local_task}

    tf_config = {'cluster': cluster, 'task': task}

    return tf_config


def get_cvd(config, local_job, local_task):
    for i, job in enumerate(config['job']):
        if job == local_job:
            if config['task_index'][i] == local_task:
                cvd = config['CVD'][i]
                break

    if cvd == 'None':
        return ''

    return '{}'.format(cvd)


def get_gpu_frac(config, local_job, local_task):
    for i, job in enumerate(config['job']):
        if job == local_job:
            if config['task_index'][i] == local_task:
                gf = config['gpu_frac'][i]
                break

    return '{}'.format(gf)


def export_system_variables(config, job, task, which):
    tf_config = make_tf_config(config, job, task)
    world_size = len(tf_config['cluster']['worker'])
    cvd_string = get_cvd(config, job, task)
    gpu_fraction = get_gpu_frac(config, job, task)

    tf_config = json.dumps(tf_config)
    os.environ['TF_CONFIG'] = tf_config
    if which == 'tf':
        print('{}'.format(tf_config))
    elif which == 'gf':
        print('{}'.format(gpu_fraction))
    elif which == 'ws':
        print('{}'.format(world_size))
    else:
        print('{}'.format(cvd_string))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('job', type=str, help='Name of local job')
    parser.add_argument('task', type=str, help='Index of local task')
    parser.add_argument('--tf_config', action='store_true',
                        help='flag to return tf_config. '
                             'If not set returns either gpu_frac or cvd.')
    parser.add_argument('--gpu_frac', action='store_true',
                        help='flag to return gpu_frac. '
                             'If not set returns either tf_config or cvd.')
    parser.add_argument('--world_size', action='store_true',
                        help='flag to make the script return the world size.')

    args = parser.parse_args()
    job = args.job
    task = args.task
    which = 'tf' if args.tf_config else 'cvd'
    which = 'gf' if args.gpu_frac else which
    which = 'ws' if args.world_size else which
    config = load_config_from_file()
    config = replace_with_localhost(config, job, task)
    export_system_variables(config, job, task, which)
