import tensorflow as tf
import os
import json


config = json.loads(os.environ.get('TF_CONFIG'))


server = tf.train.Server(
        config['cluster'],
        job_name=config['task']['type'],
        task_index=int(config['task']['index']))

server.join()
