import torch
from torch.autograd import Variable
from torch import Tensor
import numpy as np


def to_variable(*args, **kwargs):
    var_list = []
    grad = False
    dtype = 'float'
    if 'grad' in kwargs:
        grad = kwargs['grad']
    if 'dtype' in kwargs:
        dtype = kwargs['dtype']

    for arg in args:
        if arg is None or isinstance(arg, Variable):
            var = arg
        elif not isinstance(arg, (np.ndarray, float, int)):
            var = to_variable(*arg, **{'grad': grad})
        else:
            tensor = torch.from_numpy(np.asarray(arg))
            tensor = getattr(tensor, dtype)()
            var = Variable(tensor, requires_grad=grad)
        var_list.append(var)
    if len(var_list) == 1:
        var_list = var_list[0]
    return var_list


def to_tensor(*args, **kwargs):
    ten_list = []
    for arg in args:
        if arg is None or isinstance(arg, Tensor):
            ten = arg
        elif isinstance(arg, Variable):
            ten = arg.cpu().data
        elif not isinstance(arg, (np.ndarray, float, int)):
            ten = to_tensor(*arg)
        else:
            ten = torch.from_numpy(arg).float()
        ten_list.append(ten)
    if len(ten_list) == 1:
        ten_list = ten_list[0]
    return ten_list


def to_array(*args, **kwargs):
    arr_list = []
    for arg in args:
        if arg is None or isinstance(arg, (np.ndarray, int, float)):
            arr = np.array(arg) if arg is not None else arg
        elif not isinstance(arg, (Variable, Tensor)):
            arr = to_array(*arg)
        else:
            arg = to_tensor(arg)
            arr = arg.numpy()
        arr_list.append(arr)
    if len(arr_list) == 1:
        arr_list = arr_list[0]
    return arr_list
