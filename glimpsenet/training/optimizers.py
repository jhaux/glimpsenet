import tensorflow as tf
import logging

logger = logging.getLogger(__name__)


class MySGD(tf.train.GradientDescentOptimizer):
    def __init__(self, learning_rate):
        super(MySGD, self).__init__(learning_rate)


class MyMoment(tf.train.MomentumOptimizer):
    def __init__(self, learning_rate):
        super(MyMoment, self).__init__(
                learning_rate,
                momentum=0.9,
                use_nesterov=True)


class MyRMS(tf.train.RMSPropOptimizer):
    def __init__(self, learning_rate):
        super(MyRMS, self).__init__(
                learning_rate,
                momentum=0.9,
                centered=True)


class MyAdam(tf.train.AdamOptimizer):
    def __init__(self, learning_rate):
        super(MyAdam, self).__init__(learning_rate)
