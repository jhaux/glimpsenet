import tensorflow as tf
import numpy as np


def ta_write_ta():
    N_inner = 3
    N_outer = 2

    ta_i_list = []
    for i in range(N_outer):
        ta_i = tf.TensorArray(tf.float32, size=1, dynamic_size=True,
                              infer_shape=False,
                              name='ta_i_{}'.format(i))
        for j in range(N_inner):
            arr = np.ones([2, np.random.randint(1, 5)])
            arr = tf.to_float(arr)
            ta_i = ta_i.write(j, arr)
        ta_i_list.append(ta_i)

    ta_o = tf.TensorArray(tf.float32, size=1, dynamic_size=True,
                          infer_shape=False, name='ta_o')

    for i in range(N_outer):
        ta_i = ta_i_list[i]
        for j in range(N_inner):
            idx = N_inner * i + j
            print idx
            ta_o = ta_o.write(idx, ta_i.read(j))

    with tf.Session() as s:
        shapes = []
        for k in range(N_inner * N_outer):
            shapes.append(tf.shape(ta_o.read(k)))

        S = s.run(shapes)
        for sh in S:
            print sh


if __name__ == '__main__':
    ta_write_ta()
