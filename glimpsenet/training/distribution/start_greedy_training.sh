#!/bin/bash -l
# Used by crontab to start training, that uses all available GPUs

# Here are the scripts
cd ~/Documents/Uni\ HD/Masterarbeit/code/glimpsenet/training/distribution

# Kill running session
./auto_distribute.sh K

# Scan and write cluster spec
./scanner.sh
python make_all_server_cluster_spec.py

# relink cluster spec for auto_distribute.sh
ln -sfn all_servers_spec.txt cluster_spec.txt

# Start new session
./auto_distribute.sh
