from multiprocessing import Value, Lock


class Counter(object):
    def __init__(self, initval=0):
        self.val = Value('i', initval)
        self.lock = Lock()

    def increment(self):
        with self.lock:
            self.val.value += 1
            return self.val.value

    def value(self):
        with self.lock:
            return self.val.value


class LoggingCounter(Counter):
    def __init__(self, initval=0, log_every=100):
        super(LoggingCounter, self).__init__(initval)
        self.log_every = log_every

    def increment(self):
        with self.lock:
            self.val.value += 1
            if self.val.value >= self.log_every:
                self.val.value = 0
                return True
            return False
