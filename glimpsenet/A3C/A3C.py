import torch
import torch.nn as nn
import torch.nn.init as init
from torch.autograd import Variable
import numpy as np
import os as osys
import time

from model.drama import DeepRecurrentAttentionModel as DRAM
from model.buildingBlocks import Baseline
from training.hooks.plots import PlotGenerator
from util.tensor_ops import to_array


def log_pdf(sample, mean, sigma):
    '''Compute log prob. density fn. of a normal distribution with given mean
    and stddev.

    Normal PDF: pd(x | \mu, \sigma) = \frac{1}{\sqrt{2\pi\sigma^2}}
                                      \cdot \exp{-\frac{(x - \mu)^2}{2\sigma^2}
    Log PDf: lg_pd(x | \mu, \sigma) = -\frac{(x - \mu)^2}{2\sigma^2}
                                      - \log{\sqrt{2\pi\sigma^2}}
    '''
    log_exp = -(sample - mean)**2 / (2*sigma**2)
    offset = np.log(np.sqrt(2*np.pi)*sigma)

    return log_exp - offset


class A3C_Worker(nn.Module):
    '''Wraps around a model and trains it.'''

    def __init__(self, env, Config, ckpt_path=None,
                 parameter_server=False,
                 test_worker=False,
                 fixed_stepsize=True):
        super(A3C_Worker, self).__init__()

        self.Config = Config

        self.policy = DRAM(Config).cuda()
        self.baseline = Baseline(Config.getParameter('Baseline')).cuda()
        self.trainables = dict(self.named_parameters())

        self.env = env
        self.crossE = torch.nn.CrossEntropyLoss()
        self.crossE.cuda()
        # self.nll = torch.nn.NLLLoss()  # Used to compute cross entropy
        # self.nll.cuda()

        self.max_iter = Config.getParameter('HyperParameters/maxIter')
        self.M = Config.getParameter('HyperParameters/M')
        self.sig_em = Config.getParameter('HyperParameters/sigma_em')
        self.sig_sig = Config.getParameter('HyperParameters/sigma_sig')
        self.l = Config.getParameter('HyperParameters/l')
        self.discount = Config.getParameter('HyperParameters/discountFactor')
        pred_thresh = Config.getParameter('predictionThreshold')
        self.pred_thresh = torch.cuda.FloatTensor([pred_thresh])
        fss = Config.getParameter('HyperParameters/fixedStepSize')
        self.accumulate = not fss
        self.test_losses = False
        self.ps_fixed = Config.getParameter('HyperParameters/psFixed')
        if self.ps_fixed:
            ps = Config.getParameter('HyperParameters/patchSizes')[0]
            self.ps = Variable(torch.cuda.FloatTensor([[ps]]))

        self.last_checkpoint = None

        self.store_for_log = False
        if parameter_server:
            self.save_path = ckpt_path
            print('Save Path of this model is {}'.format(self.save_path))
        if parameter_server or test_worker:
            self.plotter = PlotGenerator(ckpt_path)
            self.store_for_log = True

        rank = Config.getParameter('r')
        self.time_file = 'time_{}.log'.format(rank)
        self.times = []

    def find_checkpoint(self, alternate_restore_path=None):
        if alternate_restore_path is not None:
            root = alternate_restore_path
        else:
            root = self.save_path
        files = osys.listdir(root)
        files = [f for f in files if '_model.torch.tar' in f]
        if len(files) > 0:
            nums = [int(n) for n, _ in [f.split('_') for f in files]]
            highNum = max(nums)
            file_name = '{}_model.torch.tar'.format(highNum)
            self.last_checkpoint = osys.path.join(root, file_name)
            self.last_ckpt_step = highNum
            return True
        else:
            return False

    def initialize_variables(self, alternate_restore_path=None):
        self.step = 0

        def initializer(m):
            if isinstance(m, nn.Conv2d) or isinstance(m, nn.Linear):
                init.xavier_uniform(m.weight)
                m.bias.data.zero_()
            if isinstance(m, nn.LSTMCell):
                init.xavier_uniform(m.weight_ih)
                m.bias_ih.data.zero_()
                init.xavier_uniform(m.weight_hh)
                m.bias_hh.data.zero_()

        self.apply(initializer)
        if self.find_checkpoint(alternate_restore_path):
            if self.last_checkpoint is not None:
                restore = torch.load(self.last_checkpoint)
                this_state_dict = tsd = self.state_dict()

                pretrained = {k: v for k, v in restore.items() if k in tsd}
                this_state_dict.update(pretrained)
                self.load_state_dict(this_state_dict)
                print('Restoring from {}'.format(self.last_checkpoint))
                print('The following parameters are being restored:')
                for k in pretrained.keys():
                    print(k)
                print('The following parameters are randomly initialized:')
                for k in this_state_dict.keys():
                    if k not in pretrained:
                        print(k)
                self.step = self.last_ckpt_step

    def set_as_untrainable(self, *names):
        '''Makes parameters of the networks untrainable given their names or
        parts of their names.'''
        names = list(names)
        for k, v in self.named_parameters():
            tr = 'is'
            for name in names:
                if name in k:
                    v.requires_grad = False
                    tr = 'is not'
                    del self.trainables[k]
                    break
            print('{} {} trainable'.format(k, tr))

    def set_as_trainable(self, *names):
        '''Makes parameters of the networks trainable given their names or
        parts of their names and all others untrainable!'''
        names = list(names)
        print(names)
        for k, v in self.named_parameters():
            tr = 'is not'
            v.requires_grad = False
            for name in names:
                if name in k:
                    v.requires_grad = True
                    tr = 'is'
                    break
            if tr != 'is':
                del self.trainables[k]
            print('{} {} trainable'.format(k, tr))

    def eval_loop(self, low_res, imsize, samples=None):
        '''One forward pass of the recurrent newtork. Records all values of
        interest.'''
        self.times += ['eval_start', time.time()]
        self.env.restart()
        self.times += ['env restart', time.time()]

        l_list = []
        ps_list = []
        V_list = []
        R_list = []
        pred_list = []
        pred_sig_list = []
        targ_list = []
        glimpse_list = []

        state1, state2 = self.policy.initial_lstm_states(low_res, imsize)
        c2 = state2[1]
        if samples is None:
            l, ps = self.policy.initial_action(c2)
        else:
            l, ps = samples[0]

        if self.ps_fixed:
            ps = self.ps
        pred = None
        targ = None
        pred_signal = torch.cuda.FloatTensor([0.])
        reward = 0
        terminal = False
        action = {'location': l.squeeze(),
                  'patch_size': ps.squeeze(),
                  'prediction': pred,
                  'pred_signal': pred_signal}

        self.times += ['setup action', time.time()]

        glimpse, reward, terminal, info = self.env.step(action)
        glimpse = Variable(glimpse)
        targ = info['target']
        self.times += ['env return', time.time()]

        max_iter = self.max_iter + 1 if samples is None else len(samples)
        for t in range(max_iter):

            V = self.baseline(state2[1].detach())
            l_list.append(l)
            ps_list.append(ps)
            V_list.append(V)
            R_list.append(reward)
            pred_list.append(pred)
            pred_sig_list.append(pred_signal)
            targ_list.append(targ)
            glimpse_list.append(glimpse)

            self.times += ['append to lists', time.time()]
            if bool((pred_signal > self.pred_thresh).cpu().numpy()):
                pred_signal.zero_()

            if terminal or t == max_iter - 1:
                break

            self.times += ['check out signal', time.time()]
            l, ps, pred, state1, state2 \
                = self.policy(l, ps, glimpse, state1, state2)

            if samples is not None:
                l, ps = samples[t + 1]

            if self.ps_fixed:
                ps = self.ps

            self.times += ['setup action', time.time()]
            action = {'location': l.squeeze(),
                      'patch_size': ps.squeeze(),
                      'prediction': pred.squeeze(),
                      'pred_signal': pred_signal}
            glimpse, reward, terminal, info = self.env.step(action)
            glimpse = Variable(glimpse)
            targ = info['target']
            self.times += ['env return', time.time()]

        self.times += ['eval end', time.time()]

        # with open(self.time_file, 'a') as f:
        #     f.write(', '.join([str(t) for t in times]) + '\n')

        return [l_list,
                ps_list,
                V_list,
                R_list,
                pred_list,
                pred_sig_list,
                targ_list,
                glimpse_list]

    def calc_returns(self, record):
        rewards = record[3]
        targets = record[6]
        returns = []
        R = 0
        for r, t in zip(rewards[::-1], targets[::-1]):
            if self.accumulate:
                # Return is discounted sum of futur rewards
                R += self.discount * r
                returns.append(R)
            else:
                # Return is one until correct prediction else 0
                if t is not None:
                    if r > 0:
                        R = 1
                    else:
                        R = 0
                returns.append(R)

        returns = returns[::-1]
        return returns

    def train_step(self, log):
        self.times = []
        self.times += ['start train step', time.time()]
        M = self.M
        s_e = self.sig_em

        low_res, imsize = self.env.reset()
        low_res = Variable(low_res, requires_grad=False)
        self.times += ['env reset low_res', time.time()]

        recording_init = self.eval_loop(low_res, imsize)
        recording_init.append(self.calc_returns(recording_init))

        ls = recording_init[0]
        pss = recording_init[1]
        recordings = []
        total_reinforce_loss = 0.
        total_classification_loss = 0.
        total_baseline_loss = 0.
        self.times += ['recording init', time.time()]
        for m in range(M):
            ls_s = []
            pss_s = []
            for ll, pp in zip(ls, pss):
                ll_s = ll + Variable(torch.randn(1, 2)).cuda() * s_e
                pp_s = pp + Variable(torch.randn(1, 1)).cuda() * s_e
                ls_s.append(ll_s)
                pss_s.append(pp_s)
            self.times += ['sampling', time.time()]

            samples = list(zip(ls_s, pss_s))
            recording = list(self.eval_loop(low_res, imsize, samples))
            returns = self.calc_returns(recording)
            recording.append(returns)
            self.times += ['returns', time.time()]

            recordings.append(recording)
            reinforce_loss = 0.
            classification_loss = 0.
            N_pred = 0
            baseline_loss = 0.
            for ts, (l, ps, V, r, pr, prs, t, g, R) \
                    in enumerate(zip(*recording)):
                l_hat = ls[ts]
                ps_hat = pss[ts]

                # Loss: grad[log(1/sqrt(2pi sig^2) * exp((s-m)^2)/2sig^2)]
                #     = grad[(s-m)^2/2sig^2 - log(sqrt(2pi sig^2)]
                log_pl = log_pdf(l.detach(), l_hat, self.sig_em)
                log_pps = log_pdf(ps.detach(), ps_hat, self.sig_em)

                eligibility = (R - V).detach()

                re_tmp = -torch.mean(log_pl * eligibility)
                if not self.ps_fixed:
                    re_tmp += -torch.mean(log_pps * eligibility)

                bl_tmp = (R - V)**2

                if t is not None:
                    target = Variable(t)

                    cl_tmp = self.crossE(pr, target)
                    N_pred += 1
                    classification_loss += cl_tmp

                reinforce_loss += re_tmp
                baseline_loss += bl_tmp

            N_ts = (ts + 1)
            N_pred = max(1, N_pred)
            total_baseline_loss += baseline_loss / N_ts
            total_classification_loss += classification_loss / N_pred
            total_reinforce_loss += reinforce_loss / N_ts

        total_baseline_loss /= M
        total_classification_loss /= M
        total_reinforce_loss /= M
        total_loss = total_baseline_loss + total_classification_loss
        total_loss += self.l * total_reinforce_loss
        self.times += ['loss calculation', time.time()]

        if log:
            re_loss = np.squeeze(to_array(total_reinforce_loss))
            cl_loss = np.squeeze(to_array(total_classification_loss))
            bl_loss = np.squeeze(to_array(total_baseline_loss))
            t_loss = np.squeeze(to_array(total_loss))
            glimpses = recording_init[-2]
            glimpses = [np.transpose(g.data.cpu().numpy()[0], [1, 2, 0])
                        for g in glimpses]
            glimpses = np.stack(glimpses)
            low_res = np.transpose(self.env.low_res.cpu().numpy()[0],
                                   [1, 2, 0])
            image = np.transpose(self.env.image.cpu().numpy()[0], [1, 2, 0])
            logs = dict(
                    images=dict(
                        image=image,
                        low_res=low_res,
                        glimpses=glimpses),
                    scalars=dict(
                        total_loss=t_loss,
                        total_reinforce_loss=re_loss,
                        total_baseline_loss=bl_loss,
                        total_classification_loss=cl_loss),
                )
        else:
            logs = None

        if self.store_for_log:
            self.recordings = recordings
            self.recording_init = recording_init

        return total_loss, logs

    def log(self, step, log_samples=False, test=False, index=None,
            make_plot=False):
        accuracy = 0.
        if not test:
            for m in range(self.M):
                recording = self.recordings[m]
                acc_ts = 0.
                N_pred = 0.
                for ts, (l, ps, V, r, pr, prs, t, g, R) \
                        in enumerate(zip(*recording)):
                    if t is not None:
                        _, pred = torch.max(pr, -1)
                        acc_ts += torch.equal(pred.data, t)
                        N_pred += 1
                N_pred = max(1, N_pred)
                acc_ts /= N_pred
                accuracy += acc_ts
            accuracy /= self.M
        else:
            accuracy = 0.
            N_pred = 0.
            for ts, (l, ps, V, r, pr, prs, t, g, R) \
                    in enumerate(zip(*self.recording_init)):
                if t is not None:
                    _, pred = torch.max(pr, -1)
                    accuracy += torch.equal(pred.data, t)
                    N_pred += 1
            N_pred = max(1, N_pred)
            accuracy /= N_pred

        accuracy *= 100.
        low_res = self.env.low_res.cpu().numpy()[0]
        image = self.env.image_orig.cpu().numpy()[0]
        low_res = np.transpose(low_res, [1, 2, 0])
        im = np.transpose(image, [1, 2, 0])
        targs = self.env.targets
        plot_rec = to_array(self.recording_init)
        mode = 'train' if not test else 'eval'

        if make_plot:
            self.plotter(plot_rec,
                         im,
                         low_res,
                         targs,
                         step=step,
                         accuracy=accuracy,
                         mode=mode,
                         index=index)

        if log_samples and not test:
            for s, rec in enumerate(self.recordings[:10]):
                plot_rec = to_array(rec)
                self.plotter(plot_rec, im, low_res, targs, step=step,
                             accuracy=accuracy,
                             sample=s)

        return accuracy
