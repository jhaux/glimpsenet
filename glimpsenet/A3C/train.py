import torch

import gym
import gym_multiMnist

from A3C.A3C import A3C_Worker
from util.tensor_ops import to_array


def share_grads(worker, shared_worker):
    params_iterator = zip(worker.parameters(), shared_worker.parameters())
    for param, shared_param in params_iterator:
        if shared_param.grad is not None:
            return
        shared_param._grad = param.grad


def train(rank, config, ckpt_path, shared_worker,
          counter,
          logging_counter=None,
          logging_queue=None):

    batch_size = config.getParameter('batchSize')
    fss = config.getParameter('HyperParameters/fixedStepSize')
    step_size = config.getParameter('HyperParameters/predSteps')

    n_o = config.getParameter('HyperParameters/maxObjs')
    dif = config.getParameter('Dataset/difficulty')

    env_params = {
        'N_obj': n_o,
        'difficulty': dif,
        'sorted_targets': True,
        'fss': None if not fss else step_size,
        'mode': 'train',
        'dset': config.getParameter('Dataset/dset'),
        'imsize': config.getParameter('HyperParameters/imsize')
        }

    env_id = 'MultiMnist-{}.{}-v1'.format(n_o, dif)
    gym.envs.register(id=env_id,
                      entry_point='gym_multiMnist.envs:MultiMnistEnv',
                      kwargs=env_params)

    env = gym.make(env_id)

    worker = A3C_Worker(env, config, ckpt_path)
    lr = config.getParameter('learningRate')

    optimizer = torch.optim.Adam(shared_worker.parameters(),
                                 lr=lr,
                                 weight_decay=0.001)

    n_logs = 0
    log = False
    while True:
        step = counter.increment()
        if logging_counter is not None:
            log = logging_counter.increment()

        # For debugging log always at first possibility
        if n_logs == 0 and logging_queue is not None:
            log = True

        worker.load_state_dict(shared_worker.state_dict())
        batch_loss = 0.
        for i in range(batch_size):
            return_log = log and i == batch_size - 1
            loss, logs = worker.train_step(return_log)
            batch_loss += loss

        batch_loss /= batch_size

        optimizer.zero_grad()
        batch_loss.backward()
        share_grads(worker, shared_worker)
        optimizer.step()

        if logs is not None:
            logs['step'] = step
            logs['mode'] = 'train'
            hists = {}
            for tag, value in shared_worker.named_parameters():
                tag = tag.replace('.', '/')
                hists[tag] = to_array(value)
                if value.grad is not None:
                    hists[tag+'/grad'] = to_array(value.grad)
                else:
                    print('No gradients for {}'.format(tag))

            logs['hists'] = hists
            logging_queue.put(logs)

            n_logs += 1
