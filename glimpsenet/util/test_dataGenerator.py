import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import time
import numpy as np
from dataGenerator import DataGenerator, get_generator_fn
import argparse
import tensorflow as tf
from tensorflow.python.ops import data_flow_ops
from tensorflow.python.estimator.inputs.queues.feeding_functions import \
        _GeneratorFeedFn as GeneratorFeedFn
from tensorflow.python.estimator.inputs.queues import \
        feeding_queue_runner as fqr
from tensorflow.contrib.learn.python.learn.dataframe.queues import \
        feeding_functions


def test_time():
    MAX_SAMPLES = 10000
    start = time.time()
    for i, d in enumerate(DataGenerator('mnist', 2, 1)):
        if i == MAX_SAMPLES:
            break
        pass
    duration = time.time() - start
    per_sample = duration / MAX_SAMPLES

    print 'Duration per samples: {}'.format(per_sample)


def generate_plots():
    generator_fn = get_generator_fn(None, 'test')
    while True:
        ims = []
        ls = []
        for i, d in enumerate(generator_fn()):
            ims.append(d['features'])
            ls.append(d['labels'])

            if i >= 2:
                break

        gs = gridspec.GridSpec(2, 3, height_ratios=[8, 1])
        f = plt.figure()

        cbar_ax = f.add_subplot(gs[1, :])

        for i in range(3):
            im = ims[i]
            im = np.reshape(im, [100, 100])
            ax = f.add_subplot(gs[0, i])
            im = ax.imshow(im, 'gray', vmin=0, vmax=1)

            label = np.argmax(ls[i], axis=-1)
            ax.set_title('label: {}'.format(label))

        f.colorbar(im, cax=cbar_ax, orientation='horizontal')

        plt.draw()
        plt.pause(1)  # <-------
        hit = raw_input("<Hit 'Enter' To Generate a new set or 'q' 'Enter' "
                        "to exit> ")
        plt.close(f)

        if hit == 'q':
            break


def make_and_run_graph_with_queue():
    generator_fn = get_generator_fn(None)
    g = generator_fn()
    d = next(g)
    im, l = d['features'], d['labels']
    dtypes = [arr.dtype for arr in [im, l]]
    shapes = [arr.shape for arr in [im, l]]

    print 'dtypes', dtypes
    print 'shapes', shapes

    batch_size = 50
    shuffle = True
    epochs = None
    queue_capacity = 2000

    queue = feeding_functions.enqueue_data(
            generator_fn,
            queue_capacity,
            shuffle=shuffle,
            num_threads=16,
            enqueue_size=2*batch_size,
            num_epochs=epochs)

    features = (queue.dequeue_many(batch_size) if epochs is None
                else queue.dequeue_up_to(batch_size))

    print features

    targets = features[1]
    features = features[0]
    targets = tf.transpose(targets, [1, 0, 2])
    # nameing
    name = 'targets'
    targets = tf.identity(targets, name=name)

    print features, targets

    with tf.Session() as sess:

        f, t = sess.run([features, targets])

        print f.shape, t.shape


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', action='store_true', help='start timing')
    parser.add_argument('-p', action='store_true', help='start plotting')
    parser.add_argument('-q', action='store_true', help='start with queue')

    args = parser.parse_args()

    if args.t:
        test_time()
    if args.p:
        generate_plots()
    if args.q:
        make_and_run_graph_with_queue()
