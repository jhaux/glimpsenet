import tensorflow as tf
from tensorflow.contrib.learn.python.learn.metric_spec import MetricSpec


def prediction_accuracy_fn(predictions, labels):
    '''Calculate accuracy  by comparing prediction and labels.
    Args:
        predictions: Tensor containing the class probabilities
            [maxObjs, bs, C]
        labels: Tensor of same shape as predictions, with groundtruth
            [bs, maxObjs, C]
    Returns:
        accuracy: scalar, mean of all predictions.'
    '''

    rank_asserts = [tf.assert_rank(predictions, 3), tf.assert_rank(labels, 3)]
    with tf.control_dependencies(rank_asserts):
        classIndex = tf.argmax(predictions, axis=-1)  # [maxObjs, bs]
        predictedClass = tf.transpose(classIndex, [1, 0])

        labels = tf.argmax(labels, axis=-1)
        # labels = tf.Print(labels, [tf.shape(labels)], 'labl shape: ')

        accuracy = tf.reduce_mean(tf.to_float(
                tf.equal(predictedClass, labels)))

    return accuracy


def mean_r_pred_accuracy_fn(predictions, labels):
    '''Calculate accuracy by looking at mean reward over time'''

    # accuracy is calculated during forward pass and then just handed over
    mean_r = predictions

    return mean_r


def get_accuracy_metric(Config):
    '''Setup a MetricSpec for use with tf.contrib.learn's Estimator class
    Args:
        Config: GlimpseNetConfig instance, for reliably knowing the tensor
            names.
    Returns:
        AccuracyMetric: MetricSpec instance, for accuracy calculation
    '''
    AccuracyMetric = MetricSpec(
            metric_fn=mean_r_pred_accuracy_fn,
            # prediction_key=Config.getParameter('Names/prediction'),
            prediction_key='accuracy',
            label_key=None)

    return AccuracyMetric


def get_metrics(Config):
    '''Helper Function, that returns all metrics, setup in this script
    Args:
        Config: GlimpseNetConfig to be passed to metric setup functions
    Returns:
        metrics: List of MetricSpecs
    '''
    metrics = {}
    metrics.update({'accuracy': get_accuracy_metric(Config)})
    return metrics
