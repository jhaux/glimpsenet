import tensorflow as tf


def summaries(tensor_dict, n_timesteps=None, split_time=False, at_obj=False):
    '''Make all those nice summaries'''
    for name, tensor in tensor_dict.iteritems():
        tf.summary.histogram(name, tensor)
        tf.summary.scalar(name+'_mean', tf.reduce_mean(tensor))
        if split_time:
            time_ax = -2
            slices = tf.unstack(tensor, num=n_timesteps, axis=time_ax)
            for ts, tensor_slice in enumerate(slices):
                prefix = 'ts_{}_' if not at_obj else 'pr_{}_'
                tf.summary.histogram(prefix.format(ts)+name, tensor_slice)


def getSummaries(Config):
    summaries = []
    verbose = Config.getParameter('verbose')
    if verbose in [1, 2]:
        summaries += ['loss', 'learning_rate']
    if verbose in [2]:
        summaries += ['gradients']

    return summaries
