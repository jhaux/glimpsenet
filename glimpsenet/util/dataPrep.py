import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
from tensorflow.contrib.learn.python.learn.dataframe.queues \
        import feeding_functions
import collections
import logging
import numpy as np
import os

from training.config import DramaConfig
from util.decorators import addVariableScope
from util.dataGenerator import get_generator_fn

logger = logging.getLogger('dataPrep')


def loadData(Config, dset='train'):
    ''' Helper function for loading datasets
    Args:
        Config: Config object which contains the name of the dataset
    '''

    name = Config.getParameter('Dataset/dset')
    subset, sort = Config.getParameter('Dataset/subset').split('-')
    maxObjs = Config.getParameter('HyperParameters/maxObjs')
    difficulty = Config.getParameter('Dataset/difficulty')

    job = Config.getParameter('type')
    task = Config.getParameter('task')
    master = job == 'worker' and task == 0
    evaluator = job == 'eval'
    print '+++++++++++++++++++++++'
    if not evaluator:
        print '+++   {}   +++'.format('  master  ' if master else 'not master')
    else:
        print '+++   {}   +++'.format('   eval   ')
    print '+++++++++++++++++++++++'

    # if dset == 'validation':
    #     name = 'Generator'

    if name == 'MM' or name == 'Multi_MNIST':
        dset_dir = os.path.join(
                os.environ['HOME'],
                'Documents/Uni HD/Masterarbeit/datasets/MultiMNIST/Latest')

        if dset == 'train':
            X = np.load(os.path.join(
                dset_dir, '{}-2X_{}.npy'.format(subset, sort)), 'r')
            Y = np.load(os.path.join(
                dset_dir, '{}-2Y_{}.npy'.format(subset, sort)), 'r')
        elif dset == 'eval':
            X = np.load(os.path.join(
                dset_dir, '{}-test2X_{}.npy'.format(subset, sort)), 'r')
            Y = np.load(os.path.join(
                dset_dir, '{}-test2Y_{}.npy'.format(subset, sort)), 'r')

    elif name == 'M' or name == 'MNIST':
        mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)

        if dset == 'train':
            X, Y = mnist.train.images, mnist.train.labels
            X = np.reshape(X, [-1, 28, 28, 1])
        elif dset == 'eval':
            X, Y = mnist.test.images, mnist.test.labels
            X = np.reshape(X, [-1, 28, 28, 1])

    elif name == 'tM' or name == 'translated_MNIST':
        dset_dir = os.path.join(
                os.environ['HOME'],
                'Documents/Uni HD/Masterarbeit/datasets/translatedMNIST_2/'
                'Latest')

        if dset == 'train':
            X = np.load(os.path.join(
                dset_dir, '{}-1X_{}.npy'.format(subset, sort)), 'r')
            Y = np.load(os.path.join(
                dset_dir, '{}-1Y_{}.npy'.format(subset, sort)), 'r')
        elif dset == 'eval':
            X = np.load(os.path.join(
                dset_dir, '{}-test1X_{}.npy'.format(subset, sort)), 'r')
            Y = np.load(os.path.join(
                dset_dir, '{}-test1Y_{}.npy'.format(subset, sort)), 'r')

    elif name == 'cM' or name == 'curriculum_MNIST':
        dset_dir = os.path.join(
                os.environ['HOME'],
                'Documents/Uni HD/Masterarbeit/datasets/curriculumSets/Latest')

        if dset == 'train':
            X = np.load(os.path.join(
                dset_dir, '{}_{}-{}X_{}.npy'.format(subset, difficulty,
                                                    maxObjs, sort)), 'r')
            Y = np.load(os.path.join(
                dset_dir, '{}_{}-{}Y_{}.npy'.format(subset, difficulty,
                                                    maxObjs, sort)), 'r')
        elif dset == 'eval':
            X = np.load(os.path.join(
                dset_dir, '{}_{}-test{}X_{}.npy'.format(subset, difficulty,
                                                        maxObjs, sort)), 'r')
            Y = np.load(os.path.join(
                dset_dir, '{}_{}-test{}Y_{}.npy'.format(subset, difficulty,
                                                        maxObjs, sort)), 'r')

    elif name == 'cars':
        dset_dir = os.path.join(
                os.environ['HOME'],
                'Documents/Uni HD/Masterarbeit/datasets/cars')
        mode = 'test' if dset == 'validation' else 'train'
        X = np.load(os.path.join(
            dset_dir, '{}_X.npy'.format(mode)),
            'r')
        Y = np.load(os.path.join(
            dset_dir, '{}_brands.npy'.format(mode)),
            'r')

    elif name == 'Generator' and (master or evaluator):
        dset_dir = os.path.join(
                os.environ['HOME'],
                'Documents/Uni HD/Masterarbeit/datasets/validation_sets')
        X = np.load(os.path.join(
            dset_dir, 'nO{}-d{}of{}_X_validation.npy'.format(maxObjs,
                                                             difficulty,
                                                             6)), 'r')
        Y = np.load(os.path.join(
            dset_dir, 'nO{}-d{}of{}_Y_validation.npy'.format(maxObjs,
                                                             difficulty,
                                                             6)), 'r')
    elif not master:
        X = np.zeros([100])
        Y = np.zeros([100])

    if Config.getParameter('sanity', dictName='Training'):
        size = 10
        X = X[:size]
        Y = Y[:size]
        X = X[:size]
        Y = Y[:size]

    # Ensure correct dimension
    if maxObjs == 1 and np.rank(Y) != 3:
        Y = np.expand_dims(Y, axis=-2)

    logger.debug('Data shapes:')
    logger.debug('{}X:\t\t{}'.format(dset, X.shape))
    logger.debug('{}Y:\t\t{}'.format(dset, Y.shape))
    print('{}X:\t\t{}'.format(dset, X.shape))
    print('{}Y:\t\t{}'.format(dset, Y.shape))
    return X, Y


def downsampling(images, config):
    '''Perform bilinear downsampling of images.

    Arguments:
        images: the images to be downsampled (4D tensor)
        config: GlimpseNetConfig object
    Returns:
        downsampled: images that are now smaller
    '''

    print 'images shape:', images.shape

    downsampling = 4  # TODO make that configable
    input_dim = config.getParameter('HyperParameters/inputDim')
    coarse_shape = [int(d/downsampling) for d in input_dim]
    coarse_shape = coarse_shape[:2]
    downsampled = tf.image.resize_images(
            images=images,
            size=coarse_shape,
            method=0  # Biliniear
    )

    tf.summary.image('downsampled', downsampled)

    return downsampled


def expand_targets(targets, config):
    '''Expand the targets, such that they match the predictions over time of
    the Network.

    Arguments:
        targets: [list of] groundtruth one-hot vectors
        config: GlimpseNetConfig
    Returns:
        targets_expand: Expanded version of the groundtruth
    '''

    predSteps = config.getParameter('HyperParameters/predSteps')
    stepWisePredictions = config.getParameter('stepWisePrediction')

    num_expand = predSteps if stepWisePredictions else 1

    targets = tf.transpose(targets, [1, 0, 2])

    targets = tf.unstack(targets, axis=0)  # 0 is batch size
    targets_expand = []
    for target in targets:
        single_expand = [target] * num_expand
        targets_expand += single_expand
    # stack -> [maxObjs*predSteps or maxObjs, bs, C]
    targets = tf.parallel_stack(targets_expand)

    targets = tf.transpose(targets, [1, 0, 2])

    return targets


def targets_preprocessing(targets, config):
    '''Called in model_fn to do operations on the targets depending on mode.

    Arguments:
        targets: [list of] groundtruth one-hot vectors
        config: GlimpseNetConfig
    Returns:
        targets: Manipulated version of targets
    '''

    # manage dtype. Wrong because numpy works with higher precision?
    targets = tf.to_float(targets)

    # If there is only one target the shape of the resulting targets tensor is
    # [bs, N_cl]. This is changed to [N_obj=1,, bs, N_cl]
    targets = tf.cond(tf.equal(tf.rank(targets), 2),
                      lambda: tf.expand_dims(targets, 0),
                      lambda: targets)

    N_cl = config.getParameter('ClassificationNet/fc2/nu')
    bs = config.getParameter('batchSize')
    targets.set_shape([None, bs, N_cl])

    # # manage dimension, such that it fits those of the prediction sequence
    # targets = expand_targets(targets, config)

    return targets


def data_preprocessing(features, config, mode='train'):
    '''Preprocess data and update features.

    Arguments:
        features: dict containing the original features
        config: GlimpseNetConfig
    Returns:
        features: dict with new and/or updated features
    '''

    # with tf.device('/cpu:0'):
    originals = features['input_original']
    originals = tf.to_float(originals)
    print 'originals shape:', originals.shape
    features['input_original'] = originals

    # downsampling for contextnet
    input_coarse = downsampling(originals, config)
    features['input_coarse'] = input_coarse

    targets = features['targets']
    targets = targets_preprocessing(targets, config)
    features['targets'] = targets

    return features


def get_input_fn(config, mode='train'):
    '''Sets up the input_fn for the train dataset using a custom input function
    and doing some data preparation.

    Args:
        config: GlimpseNetConfig object with all necessary information
        mode: String to determine if the train or eval function is needed
    Returns:
        train_input_fn: callable, that satisfies the requierements by the
            Estimator class.
    Raises:
        Value Error: if config is not GlimpseNetConfig
    '''

    if not isinstance(config, DramaConfig):
        raise ValueError('Must supply a GlimpseNetConfig, but got {}'
                         .format(type(config)))

    bs = config.getParameter('batchSize', dictName='Training')
    queue_capacity = 200
    dset = config.getParameter('Dataset/dset')

    print '++++++++++++++++++++++++'
    print '+++   ', mode, '   +++'
    print '++++++++++++++++++++++++'

    if mode == 'train':

        shuffle = config.getParameter('shuffle', dictName='Training')
        epochs = config.getParameter('numEpochs', dictName='Training')

        if dset == 'gM':
            data = get_generator_fn(config)
        else:
            X, Y = loadData(config, 'train')
            data = {'input_original': X, 'targets': Y}
            data = collections.OrderedDict(data)

        @addVariableScope
        def input_fn():
            queue = feeding_functions.enqueue_data(
                    data,
                    queue_capacity,
                    shuffle=shuffle,
                    num_threads=8,
                    enqueue_size=2*bs,
                    num_epochs=epochs)

            features = (queue.dequeue_many(bs) if epochs is None
                        else queue.dequeue_up_to(bs))

            # Remove the first `Tensor` in `features`, which is the row number.
            if len(features) > 0:
                features.pop(0)

            features = dict(zip(['input_original', 'targets'], features))
            features = data_preprocessing(features, config)

            targets = features.pop('targets')
            targets = tf.transpose(targets, [1, 0, 2])
            # nameing
            name = 'targets'
            targets = tf.identity(targets, name=name)

            return features, targets

    else:
        X, Y = loadData(config, 'validation')
        data_dict = {'input_original': X, 'targets': Y}
        ordered_dict = collections.OrderedDict(data_dict)

        shuffle = False
        epochs = None

        @addVariableScope
        def input_fn():
            queue = feeding_functions.enqueue_data(
                    ordered_dict,
                    queue_capacity,
                    shuffle=shuffle,
                    num_threads=1,
                    enqueue_size=bs,
                    num_epochs=epochs)

            features = (queue.dequeue_many(bs) if epochs is None
                        else queue.dequeue_up_to(bs))
            # Remove the first `Tensor` in `features`, which is the row number.
            if len(features) > 0:
                features.pop(0)

            features = dict(zip(ordered_dict.keys(), features))
            features = data_preprocessing(features, config, mode='test')

            targets = features.pop('targets')
            targets = tf.transpose(targets, [1, 0, 2])
            # nameing
            name = 'targets'
            targets = tf.identity(targets, name=name)

            return features, targets

    return input_fn
