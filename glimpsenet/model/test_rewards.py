import unittest
import numpy as np
import tensorflow as tf

import rewards


def construct_inputs(M=25, predSteps=3, middle=6, C=11):
    bs = 2 * middle

    maxObjs = 1
    empty_single = np.zeros([M, maxObjs*predSteps, bs, C])
    single_oneHot_0 = np.copy(empty_single)
    single_oneHot_0[..., 0] = 1
    single_oneHot_1 = np.copy(empty_single)
    single_oneHot_1[..., 1] = 1

    single_oneHot_0and1 = np.copy(empty_single)
    single_oneHot_0and1[..., :middle, 0] = 1
    single_oneHot_0and1[..., middle:, 1] = 1

    single_oneHot_0 = tf.constant(single_oneHot_0)
    single_oneHot_1 = tf.constant(single_oneHot_1)
    single_oneHot_0and1 = tf.constant(single_oneHot_0and1)

    single_reward_100 = np.ones([M, predSteps*maxObjs, bs])
    single_reward_0 = np.zeros([M, predSteps*maxObjs, bs])
    single_reward_50 = np.zeros([M, predSteps*maxObjs, bs])
    single_reward_50[..., :middle] = 1

    maxObjs = 3
    empty_multi = np.zeros([M, maxObjs*predSteps, bs, C])
    multi_oneHot_0 = np.copy(empty_multi)
    multi_oneHot_0[..., 0] = 1
    multi_oneHot_1 = np.copy(empty_multi)
    multi_oneHot_1[..., 1] = 1

    multi_oneHot_0and1 = np.copy(empty_multi)
    multi_oneHot_0and1[..., :middle, 0] = 1
    multi_oneHot_0and1[..., middle:, 1] = 1

    multi_oneHot_0 = tf.constant(multi_oneHot_0)
    multi_oneHot_1 = tf.constant(multi_oneHot_1)
    multi_oneHot_0and1 = tf.constant(multi_oneHot_0and1)

    multi_reward_100 = np.ones([M, predSteps*maxObjs, bs])
    multi_reward_0 = np.zeros([M, predSteps*maxObjs, bs])
    multi_reward_50 = np.zeros([M, predSteps*maxObjs, bs])
    multi_reward_50[..., :middle] = 1

    # (prediction, label) -> accuracy
    knownValues = [
            ((single_oneHot_0, single_oneHot_0), single_reward_100),
            ((single_oneHot_1, single_oneHot_0), single_reward_0),
            ((single_oneHot_0and1, single_oneHot_0), single_reward_50),
            ((multi_oneHot_0, multi_oneHot_0), multi_reward_100),
            ((multi_oneHot_1, multi_oneHot_0), multi_reward_0),
            ((multi_oneHot_0and1, multi_oneHot_0), multi_reward_50)]

    rank_1 = tf.constant(np.zeros([10]))
    rank_2 = tf.constant(np.zeros([50, 10]))
    rank_3 = tf.constant(np.zeros([1, 50, 10]))
    rank_4 = tf.constant(np.zeros([2, 1, 50, 10]))

    rank_2_tarr = tf.TensorArray(tf.float32, 1, dynamic_size=True)

    def cond(i, tarr, r2):
        return i < 3

    def body(i, tarr, r2):
        tarr = tarr.write(i, r2)
        return i, tarr, r2

    i_fin, rank_2_tarr_fin, r2 = \
        tf.while_loop(cond, body, [0, rank_2_tarr, rank_1])
    rank_2_dynamic = rank_2_tarr_fin.stack()

    rank_4_1 = tf.constant(np.zeros([10, predSteps, bs, C]))
    rank_4_2 = tf.constant(np.zeros([5, predSteps, bs, C]))

    non_matching_ranks = [
            (rank_3, rank_2),
            (rank_2, rank_4),
            (rank_2_dynamic, rank_2_dynamic)
            ]

    non_matching_shapes = [
            (rank_4_1, rank_4_2)
            ]

    return knownValues, non_matching_ranks, non_matching_shapes


class RewardsTestCase(unittest.TestCase):

    knownValues, non_matching_ranks, non_matching_shapes = construct_inputs()

    def test_correct_outputs(self):
        '''Runtime Error, that given correct input, output is correct.'''
        for (prediction, label), expected_reward in self.knownValues:
            R = rewards.rewards(prediction, label)
            with tf.Session() as sess:
                R_calc = sess.run(R)
            self.assertTrue(np.alltrue(R_calc == expected_reward))

    def test_nonmatching_input_ranks(self):
        '''Construction time error, wrong input ranks'''
        for pred, label in self.non_matching_ranks:
            with self.assertRaises(ValueError):
                r = rewards.rewards(pred, label)

    def test_nonmatching_input_shapes(self):
        '''Runtime Error, non matching shapes with correct rank.'''
        for pred, label in self.non_matching_shapes:
            with self.assertRaises(ValueError):
                r = rewards.rewards(pred, label)
                with tf.Session() as sess:
                    _ = sess.run(r)


if __name__ == '__main__':
    unittest.main()
