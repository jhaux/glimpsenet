#!/bin/bash -l
# This script starts the training


# Helper function to conveniently and reusably set training parameters outside
# of a config file
train_call() {
        cd ../../
        python3 distributed_playground.py \
                --dset gM \
                --maxObjs 2 \
                --difficulty 1 \
                --learningRate 1e-4 \
                --learningDecay 0.997 \
                --l 0.01 \
                --M 50 \
                --batchSize 16 \
                --sigma_em 15 \
                --patchSizes 11 \
                --verbose 2 \
                --lstmInit context \
                --reuse "$1" \
                --r "$2" \
                --ws "$3" \
                --job "$4"
}
                # --restore_path '/media/johannes/Data 1/Masterarbeit/Trainings/gn_v4_l1-p11-28-56-dgM/16-Styver-0' \
                # --ps-fixed \


# Argument Parsing
while [ $# -gt 0 ]; do
        case "$1" in
                --reset=*)
                  RESET="${1#*=}"
                  ;;
                --r=*)
                  RANK="${1#*=}"
                  ;;
                --w=*)
                  WORLDSIZE="${1#*=}"
                  ;;
                --job=*)
                  JOB="${1#*=}"
                  ;;
        esac
        shift
done


# Set implicit Arguments
if [[ $RESET == "r" || $RESET == "reset" ]]; then
        REUSE=False
else
        REUSE=True
fi

# Actually run the experiment
train_call "$REUSE" "$RANK" "$WORLDSIZE" "$JOB"
