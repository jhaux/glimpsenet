import torch
import time
import os
import glob


class CheckpointEvaluator(object):

    def __init__(self, model, eval_fn,
                 every_s=10, root='.', ftype='torch.tar'):
        '''Checks if a new checkpoint has been saved and then evaluates the
        model with this data.

        Arguments:
            model: Model to evaluate
            eval_fn: callable, used to evaluate the model
            every_s: Seconds to wait before evaluating the next checkpoint
            root: Path where to find the checkpoints
            ftype: File endinge of the checkpoint files
        '''

        self.every_s = every_s
        self.model = model
        self.eval_fn = eval_fn
        self.chkpt_root = os.path.join(root, '*'+ftype)

        self.last_checkpoint = None
        self.step = 0

    def scan(self):
        while True:
            latest = self.latest_checkpoint
            if latest == self.last_checkpoint or latest is None:
                time.sleep(self.every_s)
            else:
                restore_dict = torch.load(latest)
                self.model.load_state_dict(restore_dict)
                self.model.eval()
                self.eval_fn(self.model, self.step)
                self.last_checkpoint = latest

    @property
    def latest_checkpoint(self):
        list_of_checkpoints = glob.glob(self.chkpt_root)
        latest = None
        if list_of_checkpoints:
            latest = max(list_of_checkpoints, key=os.path.getctime)
            num, _ = latest.split('/')[-1].split('_')
            self.step = int(num)

        return latest
