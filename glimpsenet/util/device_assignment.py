'''
From https://github.com/tensorflow/tensorflow/issues/9517
'''

import tensorflow as tf
import logging

logger = logging.getLogger(__name__)


PS_OPS = [
      'Variable', 'VariableV2', 'AutoReloadVariable', 'MutableHashTable',
      'MutableHashTableOfTensors', 'MutableDenseHashTable'
]


def assign_to_device(device, ps_device=None):
    """Returns a function to place variables on the ps_device.

    Args:
        device: Device for everything but variables
        ps_device: Device to put the variables on.  Example values are gpu:0
        and cpu:0.

    If ps_device is not set then the variables will be placed on the device.
    The best device for shared varibles depends on the platform as well as the
    model.  Start with cpu:0 and then test gpu:0 to see if there is an
    improvement.

    """
    # device = device.lower() if device else None
    # ps_device = ps_device.lower() if ps_device else None
    def _assign(op):
        node_def = op if isinstance(op, tf.NodeDef) else op.node_def
        if node_def.op in PS_OPS:
            return ps_device
        else:
            return device
    return _assign
