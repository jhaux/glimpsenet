#!/bin/bash -l
# Used by cron_it_myself to start training, that uses one available GPU

NUM_GPUS=$1
RESET=$2
if [ -z $NUM_GPUS ]; then
        NUM_GPUS=1
        RESET=$1
        echo Starting training with $NUM_GPUS external gpus
fi

# Here are the scripts
cd ~/Documents/Uni\ HD/Masterarbeit/code/glimpsenet_copy/glimpsenet/training/distribution

# Kill running session
./auto_distribute.sh K

# Scan and write cluster spec
./scanner.sh
python make_all_server_cluster_spec.py --N $NUM_GPUS

# relink cluster spec for auto_distribute.sh
ln -sfn all_servers_spec.txt cluster_spec.txt

# Start new session
./auto_distribute.sh $RESET
