import tensorflow as tf
from collections import OrderedDict
import numpy as np

from util.layerutils import initializer, activation


class ValueEstimator(object):
    '''Class that takes care of calculating the values of a time sequence
    of DRAMA.'''

    def __init__(self, Config):
        self.Config = Config
        self.state_size = Config.getParameter('HyperParameters/nLstmUnits')
        self.lr = Config.getParameter('learningRateBl')

        self.make_template()
        self.build_graph()

    def __call__(self, state, sess):
        '''Given predictons and targets assigns a value to each timestep.

        Arguments:
            state: State from which to estimate its value.

        Returns:
            V: the scalar value of the state
        '''

        if len(state.shape) == 1:
            state = [state]

        V = sess.run(self.value, feed_dict={self.state: state})

        if V.shape[0] == 1:
            V = np.squeeze(V)

        return V

    def make_template(self):
        def value_fn(state):
            fc1 = tf.contrib.layers.fully_connected(
                    state,
                    num_outputs=100,
                    activation_fn=activation('elu'),
                    weights_initializer=initializer('xavier'),
                    biases_initializer=initializer('xavier'),
                    scope='fc1',
                    variables_collections=['VALUE']
                    )

            fc2 = tf.contrib.layers.fully_connected(
                    fc1,
                    num_outputs=1,
                    activation_fn=activation('linear'),
                    weights_initializer=initializer('xavier'),
                    biases_initializer=initializer('xavier'),
                    scope='fc2',
                    variables_collections=['VALUE']
                    )

            return fc2

        self.value_net_tl = tf.make_template(
                'value_net',
                value_fn,
                create_scope_now_=True)

    def build_graph(self):
        with tf.variable_scope('ValueEstimator'):
            self.state = tf.placeholder(tf.float32, [None, self.state_size],
                                        name='state')

            state = tf.expand_dims(self.state, axis=1)  # add bs dimension

            fc2 = self.value_net_tl(state)

            self.value = tf.squeeze(fc2, axis=1)

            self.Rs = tf.placeholder(tf.float32, [None, 1],
                                     name='Returns')
            Rs = tf.expand_dims(self.Rs, 1)

            loss = tf.reduce_mean((self.value - Rs)**2)

            self.optimizer = tf.train.AdamOptimizer(self.lr)
            parameters = tf.get_collection('VALUE')
            grads_and_vars = self.optimizer.compute_gradients(loss, parameters)

            # Store grads and vars in an ordered fashion, s.t. their shapes can
            # be used for creating the grad_update placeholders
            self.grads = OrderedDict()
            self.vars = OrderedDict()
            for [grad, var] in grads_and_vars:
                self.grads.update({var.name: grad})
                self.vars.update({var.name: var})

            self.grad_updates = OrderedDict()
            for n, g in self.grads.iteritems():
                if g is None:
                    print n
                placeholder = tf.placeholder(tf.float32, g.shape)
                self.grad_updates.update({n: placeholder})

            grads_and_vars = []
            for [n, g] in self.grad_updates.iteritems():
                grads_and_vars.append([g, self.vars[n]])

            for [g, v] in grads_and_vars:
                print v.name, g.shape, v.shape
            self.train_op = self.optimizer.apply_gradients(grads_and_vars)

    def calculate_gradients(self, states, Rs, sess):
        Rs = np.expand_dims(Rs, axis=-1)
        grads = sess.run(self.grads, feed_dict={self.Rs: Rs,
                                                self.state: states})
        grads = dict(grads)

        return grads

    def apply_gradients(self, grads, sess):
        feed_dict = {}
        for [n, g] in grads.iteritems():
            placeholder = self.grad_updates[n]
            value = g
            feed_dict.update({placeholder: value})
        sess.run(self.train_op, feed_dict=feed_dict)
