from tensorflow.contrib.learn.python.learn.learn_runner import run
from tensorflow.contrib.learn import Experiment
import logging
import traceback
import os
import json

from training.custom_estimator import make_estimator
from util.fileManagement import DirectoryManager
from util.dataPrep import get_input_fn
from training.config import configure
from training.hooks.setup_session_run_hooks import get_eval_hooks, get_monitors
from training.hooks.custom_metrics import get_metrics

import util.custom_logging


###########
# Logging #
###########

logger = logging.getLogger(__name__)


##########################
# Config and Directories #
##########################

# TF_CONFIG is not set when starting the evaluation experiment.
try:
    tf_config = os.environ['TF_CONFIG']
    tf_config = json.loads(tf_config) if tf_config else None
except KeyError:
    tf_config = None

Config = configure()

if tf_config:
    Config.setParameters({'type': tf_config['task']['type'],
                          'task': int(tf_config['task']['index']),
                          'cluster': tf_config['cluster']})


# Ensure that if resetting training only one new model directory is created
job = Config.getParameter('type')
task = Config.getParameter('task')

if task > 0 or job != 'worker':
    Config.setParameters({'reuse': True})

# Create or get directories
Dirs = DirectoryManager(Config)


######################
# Monitors and Hooks #
######################

monitors = []
if job == 'worker' and task == 0:
    monitors = get_monitors(Config, Dirs)
eval_hooks = get_eval_hooks(Config, Dirs)


######################
# Experiment and run #
######################


def experiment_fn(output_dir=Dirs.ckpt_path):
    experiment = Experiment(
            estimator=make_estimator(Config, Dirs.ckpt_path),
            train_input_fn=get_input_fn(Config, 'train'),
            eval_input_fn=get_input_fn(Config, 'eval'),
            eval_metrics=get_metrics(Config),
            train_steps=None,
            train_monitors=monitors,
            eval_hooks=eval_hooks,
            eval_delay_secs=0,
            min_eval_frequency=1  # everytime a checkpoint is created
            )
    return experiment


if __name__ == '__main__':
    print ''
    print 'SCHEDULE'
    print Config.getParameter('schedule')
    print ''
    try:
        run(
            experiment_fn=experiment_fn,
            output_dir=Dirs.ckpt_path,
            schedule=Config.getParameter('schedule'))
    except Exception as e:
        traceback.print_exc()
        raise e
    finally:
        Dirs.saveLog()
