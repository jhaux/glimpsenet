import torch
import torch.distributed as dist
import time
from collections import OrderedDict
import numpy as np

from util.tensor_ops import to_array


class DistributedTrainer(object):
    '''Trainer Class, that organizes the sending and receiving of gradients
    and model parameters in a distributed setting.
    '''

    def __init__(self, model, grad_fn, save_path,
                 lr=1e-4,
                 logger=None, M=50, bs=10,
                 step_init=0):
        '''
        Arguments:
            model: Model to train
            grad_fn: Function that returns a dict of gradients given the model
        '''
        self.rank = dist.get_rank()
        self.sender_rank = torch.IntTensor([self.rank])
        self.world_size = dist.get_world_size()
        print('Hello from rank {}'.format(self.rank))
        self.model = model
        self.grad_fn = grad_fn
        self.save_path = save_path
        if self.save_path is not None:
            self.save_path += '/{:7>0d}_model.torch.tar'
        self.lr = lr
        self.logger = logger
        self.M = M
        self.bs = bs
        self.step_init = step_init

        # These must be the same for all instances of the Trainer.
        state_dict = self.model.state_dict()
        self.parameters = OrderedDict()
        self.gradients = OrderedDict()
        for k in state_dict.keys():
            self.gradients[k] = torch.zeros(*state_dict[k].size())
            self.parameters[k] = torch.zeros(*state_dict[k].size())

        keys = list(state_dict.keys())
        # self.compkey = 'fc1.bias'
        self.compkey = keys[-1]

        sync_t = torch.IntTensor([1])
        dist.all_reduce(sync_t)
        print('Gather All: {}'.format(sync_t))
        dist.barrier()

    def send_grads(self, grads):
        for name, grad in grads.items():
            self.gradients[name] = grad

        dist.send(self.sender_rank, dst=0)
        for n, grad in self.gradients.items():
            dist.send(grad.cpu(), dst=0)
        return

    def receive_state(self):
        new_state = {}
        for n, p in self.parameters.items():
            dist.recv(p.cpu(), 0)
            p.cuda()
            new_state[n] = p
        return new_state

    def serve(self):
        optimizer = torch.optim.Adam(self.model.parameters(), lr=self.lr)

        t_old = time.time()
        global_step = step_old = self.step_init

        # Send Parameters to all
        for dst in range(1, self.world_size):
            for n, p in self.parameters.items():
                dist.send(p.cpu(), dst)

        t_old = t_start = time.time()
        while True:
            # Receive rank from any source
            dist.recv(self.sender_rank)
            src = int(self.sender_rank.numpy())
            # Receive from specified source
            for n, g in self.gradients.items():
                dist.recv(g, src)

            # Apply Gradients
            for n, v in self.model.named_parameters():
                v.grad = torch.autograd.Variable(self.gradients[n]).cuda()
            optimizer.step()

            # Send Parameters to source that supplied gradients
            parameters = self.model.state_dict()
            for n, p in self.parameters.items():
                self.parameters[n] = parameters[n]
            for n, p in self.parameters.items():
                dist.send(p.cpu(), src)

            t_new = time.time()
            time_save = t_new - t_old
            time_dur = t_new - t_start

            if global_step % 500 == 0:
                save_path = self.save_path.format(global_step)
                torch.save(self.model.state_dict(), save_path)
                print('Saved model to {}'.format(save_path))

            if global_step % 100 == 0:
                n_steps = global_step - step_old
                n_eps = self.M * self.bs * n_steps
                sps, eps = self.print_statistics(global_step,
                                                 time_save,
                                                 time_dur,
                                                 n_steps,
                                                 n_eps)
                log_samples = False
                # if global_step % 1000 == 0:
                #     log_samples = True
                loss, logs = self.model.train_step(True)
                accuracy = self.model.log(global_step,
                                          log_samples,
                                          make_plot=False)
                logs['scalars']['accuracy'] = accuracy
                logs['scalars']['steps_per_sec'] = sps
                logs['scalars']['eps_per_sec'] = eps
                hists = {}
                for name, value in self.model.named_parameters():
                    tag = name.replace('.', '/')
                    hists[tag] = to_array(value)
                    if value.grad is not None:
                        hists[tag+'/grad'] = to_array(self.gradients[name])
                    else:
                        print('No gradients for {}'.format(tag))

                logs['hists'] = hists
                for tag, value in logs['hists'].items():
                    self.logger.histo_summary(tag, value, global_step)
                # for tag, value in logs['images'].items():
                #     self.logger.image_summary(tag, value, global_step)
                for tag, value in logs['scalars'].items():
                    self.logger.scalar_summary(tag, value, global_step)
                print('{}-logging at global_step {:7> d}'.format('train',
                                                                 global_step))

            if time_save >= 20:
                n_steps = global_step - step_old
                n_eps = self.M * self.bs * n_steps
                self.print_statistics(global_step,
                                      time_save,
                                      time_dur,
                                      n_steps,
                                      n_eps)
                step_old = global_step
                t_old = t_new

            global_step += 1

    def fit(self):
        local_step = 0

        state_dict_old = self.model.state_dict()
        state_dict_old = {n: p for n, p in state_dict_old.items()}
        while True:
            # Synchronize local model with global model
            state_dict = self.receive_state()
            self.model.load_state_dict(state_dict)

            all_same = True
            for key in state_dict.keys():
                p = state_dict[key]
                p_old = state_dict_old[key]
                p = p.cpu().numpy()
                p_old = p_old.cpu().numpy()
                all_same = all_same and np.all(p == p_old)
            same = ' ' if all_same else ' not '
            print('{}: Old and new parameters are{}the same'.format(local_step,
                                                                    same))
            print(state_dict[self.compkey])
            state_dict_old = {n: p for n, p in state_dict.items()}

            # Calculate and send Gradients
            grads = self.grad_fn(self.model)
            self.send_grads(grads)

            local_step += 1

    def print_statistics(self, step, t_dur_step, t_dur_start, n_steps, n_ep):
            days, rem = divmod(t_dur_start, 24*3600)
            hours, rem = divmod(rem, 3600)
            mins, secs = divmod(rem, 60)

            steps_per_sec = n_steps / t_dur_step
            eps_per_sec = n_ep / t_dur_step
            outputs = []
            outputs.append('step {:7> d}'.format(step))
            outputs.append('elapsed: {:2> }, {:2>0}:{:2>0}:{:04.1f}'
                           .format(int(days), int(hours), int(mins), secs))
            outputs.append('{:4.2f} updates/s'.format(steps_per_sec))
            outputs.append('{:4.2f} episodes/s'.format(eps_per_sec))
            print(' - '.join(outputs))

            return steps_per_sec, eps_per_sec
