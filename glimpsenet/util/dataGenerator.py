import numpy as np
import os
from tensorflow.examples.tutorials.mnist import input_data


class DataGenerator(object):
    '''Data Generation object, that produces new image samples on the fly
    to ensure, no sample is seen twice. This should make overfitting more
    unlikely.'''

    def __init__(self,
                 dataset,
                 num_objs,
                 start_difficulty,
                 max_difficulty=6,
                 imsize=[100, 100],
                 mode='train'):
        '''
        Arguments:
            dataset: numpy array containing the base data (e.g MNIST numbers)
            numObjs: Number of objects to appear in the image
            difficulty: integer value, that detemines how the numbers are
                placed in the image
            imsize: size of the returned image
        '''

        self.num_objs = num_objs
        self.start_difficulty = start_difficulty
        self.max_difficulty = max_difficulty
        self.imsize = imsize
        self.possible_area = imsize  # for now
        self.sort_by = 'y'
        self.mode = mode
        self.num_classes = 11
        self.a_0 = 125
        self.m_0 = 0.5

        self.validation_set_dir = '/home/johannes/Documents/Uni HD/' \
                                  + 'Masterarbeit/datasets/validation_sets'

        if type(dataset) is str:
            if dataset == 'mnist' or dataset is None:
                mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)
                X, Y = getattr(mnist, mode).images, getattr(mnist, mode).labels
                self.images = np.reshape(X, [-1, 28, 28, 1])
                self.labels = Y

        self.num_samples = len(self.images)
        self.patch_size = self.images[0].shape[0]

        if self.patch_size != self.images[0].shape[1]:
            raise ValueError('Images supplied to this generator need to be'
                             'square for now, but are {}x{}'
                             .format(*self.images[0].shape[0:2]))
        self.save_validation_set()

    def __iter__(self):
        return self

    # Python 3 compatibility
    def __next__(self):
        return self.next()

    def __call__(self):
        return self

    @property
    def validation_set_string(self):
        return 'nO{}-d{}of{}_{}_validation.npy'

    def save_validation_set(self, size=2000):
        if not self.validation_set_exists:
            d = next(self)
            im = d['features']
            l = d['labels']

            X = np.zeros([size]+list(im.shape))
            Y = np.zeros([size]+list(l.shape))

            i = 0
            while i < size:
                d = next(self)
                im = d['features']
                l = d['labels']
                X[i] = im
                Y[i] = l

                i += 1

            name_X = self.validation_set_string.format(
                    self.num_objs,
                    self.start_difficulty,
                    self.max_difficulty,
                    'X')
            name_Y = self.validation_set_string.format(
                    self.num_objs,
                    self.start_difficulty,
                    self.max_difficulty,
                    'Y')

            path_X = os.path.join(self.validation_set_dir, name_X)
            path_Y = os.path.join(self.validation_set_dir, name_Y)

            try:
                print 'saving new validation sets at {}'.format(path_X)
                print 'and at {}'.format(path_Y)
                np.save(path_X, X)
                np.save(path_Y, Y)
            except:
                print 'Could not create validation set. This is a problem, if'\
                        + ' this worker is also the master'

    @property
    def validation_set_exists(self):
            name_X = self.validation_set_string.format(self.num_objs,
                                                       self.start_difficulty,
                                                       self.max_difficulty,
                                                       'X')
            name_Y = self.validation_set_string.format(self.num_objs,
                                                       self.start_difficulty,
                                                       self.max_difficulty,
                                                       'Y')

            path_X = os.path.join(self.validation_set_dir, name_X)
            path_Y = os.path.join(self.validation_set_dir, name_Y)

            if os.path.isfile(path_X) and os.path.isfile(path_Y):
                return True
            else:
                return False

    def next(self):
        difficulty = self.start_difficulty  # for now
        image = np.zeros(self.imsize + [1])
        label = np.zeros([self.num_objs, self.num_classes])
        # image, label index -> tuple in object dimension [n_objs, im_d, l_d]
        idx_tuple = np.random.choice(self.num_samples, [self.num_objs])

        location_tuple = self.generateDistinctLocations(self.num_objs,
                                                        difficulty)

        ps = self.patch_size
        for (x, y), idx in zip(location_tuple, idx_tuple):
            image[x:x+ps, y:y+ps] = self.images[idx]

        idx_tuple = self.sortIndexTuple(location_tuple, idx_tuple)
        label = np.vstack([self.labels[j] for j in idx_tuple])

        return {'features': image, 'labels': label}

    def generateDistinctLocations(self, numObjs, difficulty):
        '''Generates a location tuple (l_1, l_2) with l_i = (x_i, y_i)
        such that the images put at those locations do not overlap'''

        delta = (np.array(self.imsize) - np.array(self.possible_area)) / 2.

        # first round: random tuple
        max_x, max_y = np.array(self.possible_area) - self.patch_size + delta
        min_x, min_y = delta

        if numObjs == 1:
            # Location drawn from beta-distribution, that broadens with
            # difficulty
            if difficulty == 0:
                # Place exactly at center
                x1 = (max_x + min_x) / 2
                y1 = (max_y + min_y) / 2
            else:
                # Place Beta distributed
                a = self.alpha(difficulty)
                x1, y1 = np.random.beta(a=a, b=a, size=[2])
                # Scale to desired range
                x1 = x1 * (max_x - min_x) + min_x
                y1 = y1 * (max_y - min_y) + min_y

            locations = [[x1, y1]]
            return np.array(locations, dtype=int)

        elif numObjs > 1:
            # Initial location drawn from uniform distribution
            x1 = np.random.randint(min_x, max_x)
            y1 = np.random.randint(min_y, max_y)

            locations = [[x1, y1]]

        for i in range(numObjs - 1):
            m = self.steepnessFromDifficulty(difficulty)

            def line(x, x1, y1, m):
                offset = y1 - x1*m
                return m*x + offset

            def draw_y(y1, max_y=max_y):
                y2 = np.random.randint(min_y, max_y)
                while y2 <= y1+self.patch_size and y2 > y1-self.patch_size:
                    y2 = np.random.randint(min_x, max_y)
                return y2

            accept = False
            while not accept:
                y2 = draw_y(y1)
                x_stop1 = line(y2, y1, x1, -m)
                x_stop2 = line(y2, y1, x1, m)
                x_min, x_max = np.sort([x_stop1, x_stop2])

                x_min = np.ceil(x_min)
                x_min = np.max([0, x_min])
                x_max = np.floor(x_max)
                x_max = np.min([max_x, x_max])

                if x_min == x_max:
                    continue
                x2 = np.random.randint(x_min, x_max)
                accept = True

            locations.append([x2, y2])

            if np.sqrt((x1-x2)**2 + (y1-y2)**2) < 28:
                # Debug output - This should never occur
                print x1, y1
                print x2, y2
                print '\n'

        return np.array(locations, dtype=int)

    def sortIndexTuple(self, location_tuple, index_tuple):
        if self.sort_by is None:
            return index_tuple

        idx = 0 if self.sort_by == 'x' else 1
        l_slice = location_tuple[:, idx]

        indeces = l_slice.argsort()
        index_tuple = np.array(index_tuple)[indeces]

        return index_tuple

    def steepnessFromDifficulty(self, difficulty):
        return self.m_0 * 10 ** (2 * difficulty)

    def alpha(self, difficulty):
        d_range = self.max_difficulty
        d = difficulty
        a = self.a_0 * ((1 + float(d_range - d)) / float(d_range))**d

        return a


def get_generator_fn(Config, mode='train'):
    '''Sets up the DataGenerator as generator function according to the Config.

    Arguments:
        Config: DRAM Config

    Returns:
        generator_fn: Generator that yields an image, label pair
    '''
    if Config is not None:
        num_objs = Config.getParameter('HyperParameters/maxObjs')
        difficulty = Config.getParameter('Dataset/difficulty')
    else:
        print "+++++++++++++++++++++++++++++++++++++++++++++++"
        print "+  Warning: using generator_fn without config +"
        print "+++++++++++++++++++++++++++++++++++++++++++++++"
        num_objs = 1
        difficulty = 5

    dg = DataGenerator('mnist', num_objs, difficulty, mode=mode)

    def generator_fn():
        '''A Wrapper to make the DataGenerator work with the estimator
        feeding_functions library, that only recognises function types.'''
        while True:
            yield next(dg)

    return generator_fn
