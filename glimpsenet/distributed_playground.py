import torch.distributed as dist
from torch.autograd import Variable
import os

from gym_glimpseEnv import GlimpseEnv
from data_preparation import ScatterGenerator as ScatterGenerator

from A3C.A3C import A3C_Worker
from util.fileManagement import DirectoryManager
from util.logger import Logger
from util.tensor_ops import to_array
from training.distribution.distributedTrainer import DistributedTrainer
from training.distribution.checkpointEvaluator import CheckpointEvaluator
from training.config import configure

import time


def grad_fn(model):
    times = ['start grad_fn', time.time()]
    batch_size = 20
    batch_loss = 0.
    for i in range(batch_size):
        loss, logs = model.train_step(log=False)
        batch_loss += loss
        times += model.times
    # zero out gradients s.t. they are not accumulated
    # If there were an optimizer this would be optimizer.zero_grad()
    for n, p in model.named_parameters():
        if p.grad is not None:
            if p.grad.volatile:
                p.grad.data.zero_()
            else:
                data = p.grad.data
                p.grad = Variable(data.new().resize_as_(data).zero_())

    times += ['zero grads', time.time()]

    batch_loss /= batch_size
    batch_loss.backward()

    times += ['backward', time.time()]

    grads = {}
    for n, v in model.named_parameters():
        grads[n] = v.grad.data

    times += ['assign grads', time.time()]

    # with open(model.time_file, 'a') as f:
    #     f.write(', '.join([str(t) for t in times]) + '\n')

    return grads


def get_eval_fn(logger, batch_size):
    def eval_fn(model, global_step):
        accuracy = 0
        batch_loss = 0
        for b in range(batch_size):
            print(b)
            loss, logs = model.train_step(True)
            batch_loss += loss
            plot = False
            if b > batch_size - 10:
                plot = True
            accuracy += model.log(global_step,
                                  test=True,
                                  make_plot=plot,
                                  index=b)
        batch_loss /= batch_size
        accuracy /= batch_size

        logs['scalars']['accuracy'] = to_array(accuracy)
        logs['scalars']['total_loss'] = to_array(batch_loss)
        hists = {}
        for name, value in model.named_parameters():
            tag = name.replace('.', '/')
            hists[tag] = to_array(value)

        logs['hists'] = hists
        for tag, value in logs['hists'].items():
            logger.histo_summary(tag, value, global_step)
        # for tag, value in logs['images'].items():
        #     logger.image_summary(tag, value, global_step)
        for tag, value in logs['scalars'].items():
            logger.scalar_summary(tag, value, global_step)
        print('{}-logging at global_step {:7> d}'.format('test',
                                                         global_step))
    return eval_fn


def main(config):
    alternate_restore_path = config.getParameter('restorePath')

    rank = config.getParameter('r')
    job = config.getParameter('job')
    world_size = config.getParameter('ws')

    if job == 'worker':
        print('Hello from rank {}'.format(rank))
        init_m = 'tcp://129.206.117.34:2222'
        print('init_method: {}'.format(init_m))
        dist.init_process_group(backend='tcp',
                                init_method=init_m,
                                rank=rank,
                                world_size=world_size)
        print('Hello again from rank {}'.format(rank))

    fss = config.getParameter('HyperParameters/fixedStepSize')
    step_size = config.getParameter('HyperParameters/predSteps')
    n_o = config.getParameter('HyperParameters/maxObjs')
    dif = config.getParameter('Dataset/difficulty')
    M = config.getParameter('HyperParameters/M')
    bs = config.getParameter('batchSize')

    imsize = config.getParameter('HyperParameters/imsize')
    gen = ScatterGenerator('mnist', n_o, dif,
                           imsize=imsize)
    env = GlimpseEnv(gen,
                     ref_size=11,
                     extract_mode='bilinear',
                     fss=None if not fss else step_size,
                     use_cuda=True)

    ckpt = None
    if rank == 0 or job == 'eval':
        if job == 'eval':
            config.setParameters({'reuse': True})
        Dirs = DirectoryManager(config)
        ckpt = Dirs.ckpt_path
    ps = False
    if rank == 0:
        ps = True

    model = A3C_Worker(env, config, ckpt, parameter_server=ps)
    # model = ToyModel()
    model.cuda()

    # with open(model.time_file, 'w+') as f:
    #     f.write('')

    # train_toy(model)

    train_logger = None
    step = 0
    if job == 'worker':
        if rank == 0:
            train_logger = Logger(os.path.join(ckpt, 'logs/train'))
            model.initialize_variables(alternate_restore_path)
            step = model.step
        Trainer = DistributedTrainer(model, grad_fn, ckpt,
                                     logger=train_logger,
                                     M=M, bs=bs,
                                     lr=config.getParameter('learningRate'),
                                     step_init=step)

        if rank == 0:
            Trainer.serve()
        else:
            Trainer.fit()
    elif job == 'eval':
        eval_logger = Logger(os.path.join(ckpt, 'logs/eval'))
        eval_fn = get_eval_fn(eval_logger, 10)
        CheckEr = CheckpointEvaluator(model, eval_fn, root=ckpt)
        CheckEr.scan()


if __name__ == '__main__':
    config = configure()
    main(config)
