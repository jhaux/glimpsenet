import json
import numpy as np
import os


def store_summaries(step, time, Dirs,
                    gradients,
                    actions_policy,
                    states_policy,
                    returns_policy,
                    values_policy,
                    actions_eval,
                    states_eval,
                    returns_eval,
                    values_eval):

    summary_path = os.path.join(Dirs.ckpt_path, 'summaries')
    try:
        os.mkdir(summary_path)
    except:
        pass

    summary_dict = {
            'step': step,
            'time': time,
            'gradients': gradients,
            'actions_policy': actions_policy,
            'states_policy': states_policy,
            'returns_policy': returns_policy,
            'values_policy': values_policy,
            'actions_eval': actions_eval,
            'states_eval': states_eval,
            'returns_eval': returns_eval,
            'values_eval': values_eval
            }

    save_path = os.path.join(summary_path, '{:0>10}.sum.json'.format(step))
    with open(save_path, 'w+') as save_file:
        data = json.dumps(summary_dict)
        print 'Saving the following summary to {}:'.format(save_path)
        print data
        save_file.write(data)
