from subprocess import check_output
import re
import json


def smi_output():
    return check_output(['nvidia-smi'])


def gpus():
    start = 6
    pos = 2
    step = 3
    current = start

    smi = smi_output()
    lines = smi.split('\n')

    interesting_lines = {}

    line = lines[current+step]
    uniq_set = set(line)
    device = 0
    while '+' in uniq_set and '-' in uniq_set:
        interesting_lines[device] = lines[current+pos]
        device += 1
        current += step
        line = lines[current+step]
        uniq_set = set(line)

    free_space = {}
    pattern = re.compile('\s[0-9]+MiB')
    p2 = re.compile('\d+')
    for dev, line in interesting_lines.iteritems():
        occupied, complete = pattern.findall(line)
        occupied = int(p2.findall(occupied)[0])
        complete = int(p2.findall(complete)[0])
        free = complete - occupied
        free_space[dev] = {'free': free, 'total': complete}

    return json.dumps(free_space)


if __name__ == '__main__':
    print gpus()
