import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import gc
from my_glimpse_extractor import extract_glimpses

bs = 10
imsize = 100
gs = np.array([[0, 2, 5, 10, 25, 50, 75, 80, 90, 100]]).T
neg_gs = -gs

ggs = np.tile(gs, [1, 2])
N_gs = len(ggs)
ggs = tf.constant(ggs, dtype=tf.float32)

neg_ggs = np.tile(neg_gs, [1, 2])
neg_ggs = tf.constant(neg_ggs, dtype=tf.float32)

max_off = imsize
Xs = np.linspace(-max_off, max_off, num=5)
Ys = np.linspace(-max_off, max_off, num=5)
Y, X = np.meshgrid(Xs, Ys)
norm = False
cent = True

images = np.zeros([bs, imsize, imsize, 1], dtype=float)
val = 0.1
step = int(imsize / 5)
for i in range(5):
    if i == 1 or i == 3:
        continue
    for j in range(5):
        if j == 1 or j == 3:
            continue
        s1, e1 = i*step, (i+1)*step
        s2, e2 = j*step, (j+1)*step
        images[:, s1:e1, s2:e2, :] = val
        val += 0.1
I = tf.constant(images, dtype=tf.float32)

all_glimpses = []
for Xlist, Ylist in zip(X, Y):
    glimpses = []
    for x, y in zip(Xlist, Ylist):
        print x, y
        g = extract_glimpses(
            image=I,
            sizes=ggs,
            offsets=tf.constant(np.array([[x, y]]*bs), dtype=tf.float32),
            centered=cent,
            normalized=norm)
        glimpses.append(g)
    all_glimpses.append(glimpses)

all_neg_glimpses = []
for Xlist, Ylist in zip(X, Y):
    neg_glimpses = []
    for x, y in zip(Xlist, Ylist):
        print x, y
        ng = extract_glimpses(
            image=I,
            sizes=neg_ggs,
            offsets=tf.constant(np.array([[x, y]]*bs), dtype=tf.float32),
            centered=cent,
            normalized=norm)
        neg_glimpses.append(ng)
    all_neg_glimpses.append(glimpses)

large_size = np.array([imsize+4, imsize+4], dtype=np.float32)
large_size = np.expand_dims(large_size, 0)
large_size = np.tile(large_size, [bs, 1])
large_size = tf.constant(large_size)
large = extract_glimpses(
    image=I,
    sizes=large_size,
    offsets=tf.constant(np.array([[0, 0]]*bs), dtype=tf.float32),
    centered=cent,
    normalized=norm)

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    G = sess.run(all_glimpses)
    nG = sess.run(all_neg_glimpses)
    print '==========================='
    print '========== LARGE =========='
    print '==========================='
    L = sess.run(large)

basename = 'my_glimpse_extractor_{}.png'
print np.shape(G)
plt.imshow(images[0, ..., 0], vmin=0, vmax=1, interpolation='nearest')
plt.title('original image')
plt.savefig(basename.format('orig'))
plt.close()
gc.collect()

print np.shape(L)
plt.imshow(L[0, ..., 0], vmin=0, vmax=1, interpolation='nearest')
plt.title('What the network can see')
plt.savefig(basename.format('net_view'))
plt.close()
gc.collect()

print 'Glimpses'
for gg, GG, sign in [[gs, G, 1], [neg_gs, nG, -1]]:
    for i in range(N_gs):
        print i
        f, AX = plt.subplots(len(X), len(X), figsize=(18, 18))
        for X_list, Y_list, g_list, ax_list in zip(X, Y, GG, AX):
            for x, y, g, ax in zip(X_list, Y_list, g_list, ax_list):
                im = g[i, ..., 0]
                _ = ax.imshow(im, vmin=0, vmax=1,
                              interpolation='nearest')
                _ = ax.axis('off')
                _ = ax.set_title('{0}, {1} - {2}x{2}'
                                 .format(round(x, 1), round(y, 1), gg[i]))
        f.savefig(basename.format('split_view_gs-{}'.format(sign*i)))
        f.clf()
        plt.close()
        gc.collect()
