import tensorflow as tf
import numpy as np
from tensorflow.python.training.session_run_hook import SessionRunArgs
from tensorflow.python.framework import ops
from tensorflow.python.training.basic_session_run_hooks \
    import SecondOrStepTimer
import six
import logging
import gc

from plots import PlotGenerator

logger = logging.getLogger(__name__)


def _as_graph_element(obj):
    """Retrieves Graph element.
    Directly copied from the tensorflow github as I was to dumb to import it.
    """
    graph = ops.get_default_graph()
    if not isinstance(obj, six.string_types):
        print obj
        print obj.graph
        if not hasattr(obj, "graph") or obj.graph != graph:
            raise ValueError("Passed %s should have graph attribute that is "
                             "equal to current graph %s." % (obj, graph))
        return obj
    if ":" in obj:
        element = graph.as_graph_element(obj)
    else:
        element = graph.as_graph_element(obj + ":0")
        # Check that there is no :1 (e.g. it's single output).
        try:
            graph.as_graph_element(obj + ":1")
        except (KeyError, ValueError):
            pass
        else:
            raise ValueError("Name %s is ambiguous, "
                             "as this `Operation` has multiple outputs "
                             "(at least 2)." % obj)
    return element


class PlotHook(tf.train.SessionRunHook):

    def __init__(self,
                 tensors,
                 config,
                 savePath,
                 every_n=5000,
                 every_s=None,
                 mode='train',
                 N_show=10):
        '''Supply the hook with the neccesary tensors for calculating the
        locations and predictions during evaluation. Also supply all
        necessary additional information for the plotting class.

        This hook is very similar to the LoggingTensorHook, with the only
        difference, that it does not log the supplied tensors, but that it
        plots them.
        '''

        # Tensors
        if not isinstance(tensors, dict):
            tensors = {item: item for item in tensors}

        self._tensors = tensors

        # Plotting needs
        self.bs = bs = config.getParameter('batchSize', dictName='Training')
        if bs < N_show:
            N_show = bs

        self.mode = mode
        self.mode_contr = tf.contrib.learn.ModeKeys.TRAIN if mode == 'train' \
            else tf.contrib.learn.ModeKeys.EVAL
        self.config = config
        self.savePath = savePath
        self.N_show = N_show

        # Timer
        self.alwaysTrigger = True if every_n is None else False
        if not self.alwaysTrigger:
            self.timer = SecondOrStepTimer(
                    every_secs=every_s,
                    every_steps=every_n)

        # bs location
        self.bs_loc = 1

    def begin(self):
        self.iterations = 0

        self.tensors = {tag: _as_graph_element(tensor)
                        for tag, tensor in self._tensors.items()}

        # Placeholders where all calculated data will be stored
        self.prediction = []
        self.prediction_ts = []
        self.prediction_seq = []
        self.emissions = []
        self.signals = []
        self.sig_single = []
        self.accuracy = []
        self.images = []
        self.low_res = []
        self.targets = []
        self.glimpses = []
        self.values = []
        self.baselines = []
        self.c2_seqs = []

    def squeeze_bs(self, array, bs_index):
        '''Batch size of one is located at 1'''

        array = np.squeeze(array, axis=bs_index)
        return array

    def before_run(self, run_context):
        '''Make sure locations and predictions are passed to run()'''

        self.internal_iterations = 0
        if self.alwaysTrigger:
            self.trigger = True
        else:
            self.trigger = self.timer.should_trigger_for_step(self.iterations)
        if self.trigger:
            logger.debug('triggerd in before_run, '
                         'mode is {}'.format(self.mode))
            return SessionRunArgs(self.tensors)
        else:
            return None

    def after_run(self, run_context, run_values):
        '''Gather tensors'''
        bs_l = 1
        bs_i = 0
        if self.trigger:
            # Get the tensors as np arrays by using the dict keys
            results = run_values[0]
            self.step = results['step']

            for i in range(self.bs):
                if i == 0:
                    print results['prediction_{}'.format(i)].shape
                    print results['image_{}'.format(i)].shape
                self.prediction.append(self.squeeze_bs(
                    results['prediction_{}'.format(i)],
                    bs_l))
                self.prediction_ts.append(self.squeeze_bs(
                    results['prediction_ts_{}'.format(i)],
                    bs_l))
                self.prediction_seq.append(self.squeeze_bs(
                    results['prediction_seq_{}'.format(i)],
                    bs_l))
                self.emissions.append(self.squeeze_bs(
                    results['emission_{}'.format(i)],
                    bs_l))
                self.signals.append(self.squeeze_bs(
                    results['signal_{}'.format(i)],
                    bs_l))
                self.sig_single.append(self.squeeze_bs(
                    results['sig_single_{}'.format(i)],
                    bs_l))
                self.images.append(self.squeeze_bs(
                    results['image_{}'.format(i)],
                    bs_i))
                self.low_res.append(self.squeeze_bs(
                    results['low_res_{}'.format(i)],
                    bs_i))
                self.targets.append(results['target_{}'.format(i)])
                self.glimpses.append(self.squeeze_bs(
                    results['glimpses_{}'.format(i)],
                    bs_l))
                self.values.append(results['value_{}'.format(i)])
                self.baselines.append(self.squeeze_bs(
                    results['baseline_{}'.format(i)],
                    bs_l))
                self.c2_seqs.append(self.squeeze_bs(
                    results['c2_seq_{}'.format(i)],
                    bs_l))

                self.accuracy.append(results['accuracy'])

            if self.mode == tf.contrib.learn.ModeKeys.TRAIN:
                self._make_plots()
                if not self.alwaysTrigger:
                    self.timer.update_last_triggered_step(self.iterations)

                self.iterations += 1

            self.internal_iterations += 1

    def get_glimpses(self, results):
        '''Retrive tensors that represent glimpses.
        Naming conventions are: glimpses_{ts}'''

        keys = np.array(results.keys())
        mask = ['glimpse' in k for k in keys]
        new_keys = keys[mask]

        max_ts = 0
        for k in new_keys:
            _, ts = k.split('_')
            ts = int(ts)
            if max_ts < ts:
                max_ts = ts

        samples = []
        for ts in range(max_ts + 1):
            samples.append(results['glimpses_{}'.format(ts)])

        samples = np.array(samples)
        samples = self.bs_to_front(samples, 2)

        return samples

    def end(self, session):
        '''Make the plots with the gathered information'''

        self._make_plots()

        if self.mode == tf.contrib.learn.ModeKeys.EVAL:
            if not self.alwaysTrigger:
                self.timer.update_last_triggered_step(self.iterations)

            self.iterations += 1

        del self.images
        del self.targets
        del self.glimpses
        del self.emissions
        del self.prediction

    def _make_plots(self):
        self.accuracy = round(np.mean(self.accuracy), 2)
        plotMachine = PlotGenerator(
                emissions=self.emissions,
                signals=self.signals,
                sig_single=self.sig_single,
                predictions=self.prediction,
                predictions_ts=self.prediction_ts,
                prediction_seqs=self.prediction_seq,
                glimpses=self.glimpses,
                values=self.values,
                baselines=self.baselines,
                images=self.images,
                low_res=self.low_res,
                target=self.targets,
                mode=self.mode,
                accuracy=self.accuracy,
                globalStep=self.step,
                config=self.config,
                savepath=self.savePath)
        plotMachine.plotTrajectory()
        # plotMachine.plotLocations()
        # plotMachine.net_view()

        del plotMachine
        gc.collect()


class InitHook(tf.train.SessionRunHook):
    '''Restore variables as defined by a dict, supplied in __init__.'''

    def __init__(self, var_dict, save_path):
        self._var_dict = var_dict
        self.save_path = save_path

    def begin(self):
        '''Set up Saver'''
        for key, val in self._var_dict.iteritems():
            self._var_dict[key] = _as_graph_element(val)
        self.saver = tf.train.Saver(self._var_dict)

    def after_create_session(self, session, coord):
        '''Initialize Variables'''
        save_path = self.save_path

        session.run(tf.global_variables_initializer())
        self.saver.restore(session, save_path)
