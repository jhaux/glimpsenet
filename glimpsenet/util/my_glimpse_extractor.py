import tensorflow as tf
import logging

logger = logging.getLogger(__name__)


def extract_glimpses(image, sizes, offsets,
                     reference_size=[11, 11],
                     normalized=False,
                     centered=False,
                     name='glimpse_extract'):
    '''Op to extract small image patches of different sizes given their centers.

    Arguments:
        image: 4D Tensor with batch_size many images -> batch size must be 1
        sizes: 2D Tensor with the edge lengthes of the patches [num_boxes, 2]
            The first element is the reference size, to wich all glimpses will
            be resized.
        offsets: 2D Tesor containing the patch centers [batch_size, 2]
        normalized: Bool, True if the inputs are in normalized coordinates
        centered: Bool, True if (0,0) is in the center of the image. If the
            image is of size [height, width], the top left corner has the
            coordinate [-height, -width] and the bottom right [height, width].
        name: Name of the op

    Returns:
        glimpses: Tensor containing all glimpses. Shape:
            [num_sizes, batch_size, ref_size[0], ref_size[1], num_channels]'''

    with tf.variable_scope(name):
        imsize = image.shape.as_list()[1:3]
        if imsize[0] == imsize[1]:
            imsize = imsize[0]
        else:
            raise NotImplementedError(
                    'Right now only squared images are allowed for use with '
                    'extract_glimpses. The images supplied have a shape of '
                    '{}.'.format(imsize))

        if not normalized:
            offsets = offsets / (imsize - 1)
            sizes = sizes / (imsize - 1)

        if centered:
            offsets = offsets / 2. + 0.5

        starts = offsets - sizes / 2.
        ends = offsets + sizes / 2.

        boxes = tf.stack([starts[:, 0], starts[:, 1], ends[:, 0], ends[:, 1]])
        boxes = tf.transpose(boxes)

        box_ind = tf.zeros_like(boxes[:, 0], dtype=tf.int32)

        crop_size = tf.to_int32(reference_size)

        glimpses = tf.image.crop_and_resize(image,
                                            boxes,
                                            box_ind,
                                            crop_size,
                                            method=None,
                                            extrapolation_value=0,
                                            name=None)

        glimpses = tf.stop_gradient(glimpses)
        glimpses = tf.identity(glimpses, name='Glimpses')

        return glimpses
