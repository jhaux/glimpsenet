# DRAMA
## Reimplementing and enhancing the deep recurrent visual attention model

My attempt at recreating and enhancing the GlimpseNetwork seen in the paper
[Multiple Object Recognition with Visual Attention](
https://arxiv.org/abs/1412.7755v2 "Multiple Object Recognition with Visual 
Attention").
