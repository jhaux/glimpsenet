#!/bin/bash -l

# Here are the scripts
cd ~/Documents/Uni\ HD/Masterarbeit/code/glimpsenet_copy/glimpsenet/training/distribution

# Kill running session
./auto_distribute.sh K

# relink cluster spec for auto_distribute.sh
ln -sfn cluster_spec_no_remote.txt cluster_spec.txt

# Start new session
./auto_distribute.sh $1
