import os  # to list files and safe modelcheckpoints in different folders
import numpy as np  # array operations and load nameslist
import shutil       # move files
import hashlib      # create unique run directories based on config class
import json
import logging

logger = logging.getLogger('fileManager')


class DirectoryManager(object):
    ''' Checks if there have been runs of this model before and creates
    directories to store the training data for the new run, without overwriting
    the old data.

    Sets up directories with the following structure:

    home l[0-9]+ p[0-9]+ d[M,MM]/
    |--config-[0-9]+-'name'.json
    |--[0-9]+-'name'/
    |--basen...

    differentHome
    |-- ...
    ...
    '''

    def __init__(self, Config):
        '''
        Args:
            basename: string, name of the model
            config: Config object to be stored
        '''

        self.Config = Config

        nl = Config.getParameter('HyperParameters/numLocations')
        ps = Config.getParameter('HyperParameters/patchSizes')
        baseName = Config.getParameter('baseName', dictName='Training')
        dset = Config.getParameter('Dataset/dset')
        self.home_dir = '{}_l{}-p{}-d{}'.format(baseName,
                                                nl,
                                                '-'.join([str(p) for p in ps]),
                                                dset)

        self.home = os.path.abspath(self.home_dir)
        try:
            os.mkdir(self.home)
        except OSError:
            logger.debug('Directory exists: {}'.format(self.home))

        self.saveConfig()

        self.setNewBase()

        # Put this all in one directory
        self.setCkptPath()
        self.makeDirs()
        self.saveTrainConfig()
        self.printDirs()

    def saveConfig(self):
        save_path = os.path.join(self.home, 'c.json.TEMP')
        with open(save_path, 'w') as file:
            json.dump(self.Config.architecture,
                      file,
                      sort_keys=False,
                      indent=4)
        logger.debug('TEMP save path: {}'.format(save_path))

        exists, self.configName = self.nameConfig()
        path = os.path.join(self.home, self.configName)
        if not exists:
            shutil.move(save_path, path)
        self.config_path = path

        self.config_path = os.path.abspath(save_path)

    def nameConfig(self):
        # Give each config a real name! => config-1-Xavier.json
        exists = False   # set to True if exists -> no new file will be created

        # get the list of names
        namesPath = os.path.join(
                os.getenv('HOME'),
                'Documents/Uni HD/Masterarbeit/datasets/movie-characters'
                )
        namesList = np.genfromtxt(namesPath, dtype=str)
        # get list of already existent configs
        configList = [f for f in os.listdir(self.home)
                      if '.json' in f and 'TEMP' not in f]
        if len(configList) is not 0:
            cSplit = [n.split('-') for n in configList]
            for i, splitSet in enumerate(cSplit):
                if len(splitSet) > 3:
                    splitSet[2] = '-'.join(splitSet[2:])
                    cSplit[i] = splitSet[:3]
            logger.debug('fileManagement->nameConfig->cSplit: {}'
                         .format(cSplit))

            cSplit = np.dstack(cSplit)
            _, nums, names = np.reshape(cSplit, cSplit.shape[1:])
            nums = np.array(nums, dtype=int)
            # get rid of file ending
            names = [n[:-5] for n in names]

            for config in configList:
                with open(os.path.abspath(os.path.join(self.home, config)),
                          'r') as file:
                    c = json.load(file)
                if self.Config.architecture == c:
                    exists = True
                    idx = configList.index(config)

                    num = nums[idx]
                    name = names[idx]
                    break

            if not exists:
                num = np.max(nums) + 1
                name = namesList[np.random.randint(namesList.shape[0])]
                while name in names:
                    name = namesList[np.random.randint(namesList.shape[0])]
        else:
            num = 0
            name = namesList[np.random.randint(namesList.shape[0])]

        # get full name
        if '/' in name:
            idx = name.find('/')
            if type(idx) is not list:
                idx = [idx]

            name_new = name
            for idx_i in idx:
                name_new = name_new[:idx_i] + '-' + name_new[idx_i+1:]
            name = name_new
        fullName = 'config-{0}-{1}.json'.format(num, name)
        if exists:
            logger.info('Config exists: {}'.format(fullName))
        return exists, fullName

    def getHashes(self, configList):
        hashList = []
        for config in configList:
            hasher = hashlib.md5()
            with open(os.path.join(self.home, config), 'r') as afile:
                buf = afile.read()
                hasher.update(buf)
            hashList.append(hasher.hexdigest())

        return hashList

    def setNewBase(self):
        filelist = os.listdir(self.home)

        basename = self.configName[len('config-'):-len('.json')] + '-'

        reuse = self.Config.getParameter('reuse', dictName='Training')

        self.highNum = 0

        if type(reuse) is int:
            requestedNum = reuse
            possibleNums = []
            for name in filelist:
                base = name[0:len(basename)]
                if base == basename:
                    try:
                        newNum = int(name[len(basename):])
                    except ValueError:
                        logger.debug('fileManagement->setNewBase->ValueError')
                        continue
                    possibleNums.append(newNum)
                    if newNum == requestedNum:
                        self.highNum = newNum
            if requestedNum not in possibleNums:
                # Number could not be found -> Error
                raise ValueError('Training run with number {} could not be '
                                 'found.\n'
                                 'Possible Training runs are: {}\n'
                                 'BaseName is {}'.format(requestedNum,
                                                         np.sort(possibleNums),
                                                         basename))
        elif not reuse:
            for name in filelist:
                base = name[0:len(basename)]
                if base == basename:
                    try:
                        newNum = int(name[len(basename):])
                    except ValueError:
                        logger.debug('fileManagement->setNewBase->ValueError')
                        continue
                    if newNum >= self.highNum:
                        self.highNum = newNum + 1

        elif reuse:
            for name in filelist:
                base = name[0:len(basename)]
                if base == basename:
                    try:
                        newNum = int(name[len(basename):])
                    except ValueError:
                        logger.debug('fileManagement->setNewBase->ValueError')
                        continue
                    if newNum >= self.highNum:
                        self.highNum = newNum
        else:
            raise ValueError('reuse must be of type bool or int, but is {}'
                             .format(type(reuse)))

        self.new_base = basename + str(self.highNum)

        self.base = os.path.join(self.home, self.new_base)

    def saveTrainConfig(self):
        tcBaseName = 'TrainRun_'
        configList = [f for f in os.listdir(self.base) if tcBaseName in f]
        logger.debug('config_list: {}'.format(configList))
        highNum = 0
        if len(configList) != 0:
            numList = [int(f[len(tcBaseName):]) for f in configList]
            logger.debug('numList: {}'.format(numList))
            highNum = np.max(numList) + 1
        trainName = tcBaseName + str(highNum)
        save_path = os.path.join(self.base, trainName)
        logger.debug('Saving train config to {}'.format(save_path))
        with open(save_path, 'w') as file:
            json.dump(self.Config.trainDict, file, sort_keys=False, indent=4)

        thBaseName = 'HyperParameters_'
        hyperName = thBaseName + str(highNum)
        save_path = os.path.join(self.base, hyperName)
        logger.debug('Saving train hyperParameters to {}'.format(save_path))
        with open(save_path, 'w') as file:
            json.dump(self.Config.hyperParameterDict, file, sort_keys=False,
                      indent=4)

    def setCkptPath(self):
        self.ckpt_name = self.base  # +'.tfl.ckpt'
        self.ckpt_path = os.path.join(self.base, self.ckpt_name)
        self.profiling_train = os.path.join(self.base, 'profiling_train')
        self.profiling_eval = os.path.join(self.base, 'profiling_eval')

    def makeDirs(self):
        try:
            os.mkdir(self.base)
        except OSError as e:
            logger.debug(e)

        try:
            os.mkdir(self.profiling_train)
            os.mkdir(self.profiling_eval)
        except OSError as e:
            logger.debug(e)

    def printDirs(self):
        logger.info('home\t{}'.format(self.home))
        logger.info('new_base\t{}'.format(self.new_base))

    def saveLog(self):
        ''' Save logfile with the same number as the corrsponding train run
        config file'''

        logName = 'trainLog.log'
        logPath = os.path.abspath(logName)

        tcBaseName = 'TrainRun_'
        configList = [f for f in os.listdir(self.base) if tcBaseName in f]
        highNum = 0
        if len(configList) != 0:
            numList = [int(f[len(tcBaseName):]) for f in configList]
            logger.debug('numList: {}'.format(numList))
            highNum = np.max(numList)
        logSaveName = 'trainLog_{}.log'.format(highNum)
        save_path = os.path.join(self.base, logSaveName)

        logger.debug('Saving Logfile to {}'.format(save_path))
        print('Saving Logfile to {}'.format(save_path))

        shutil.move(logPath, save_path)
