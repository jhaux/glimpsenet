#!/bin/bash -l
# This script uses tmux and ssh to setup all servers needed for training, that
# are specified in a config file called cluster_spec.txt. Each server is started
# a different tmux pane in the same tmux session. 
# To only kill the current training execute this script with the argument 'k'
# To not load old weights, thus restarting training pass the argument 'r'

# Argument parsing
while [ $# -gt 0 ]; do
        case "$1" in
                k)
                  KILLIT=true
                  ;;
                K)
                  HARDKILL=true
                  ;;
        esac
        shift
done

# kill old training session and ensure that all running processes are killed
SESS_NAME=testing

tmux kill-session -t $SESS_NAME

if [[ $HARDKILL = true ]]; then
    # Exit without making plots
    exit
fi

# Create tmux Session
tmux new-session -d -s $SESS_NAME
while ! tmux has-session -t $SESS_NAME; do sleep 1; done

PANE=0
N_PANES=$(wc -l < remotes.txt)
echo $N_PANES
P_WIDTH=$(echo "1 / $N_PANES" | bc -l)
P_WIDTH=$(echo "($P_WIDTH * 100)/1" | bc)
echo $P_WIDTH
tmux new-window
tmux rename-window STATS

# Given the config file setup all servers
tail -n +1 remotes.txt |  while read -r line
do
        REMOTE=$(echo $line | awk 'BEGIN { FS="|"} {print $1}')
        COUNT=$(echo $line | awk 'BEGIN { FS="|"} {print $2}' | tr -d '[:space:]')
        
        tmux selectp -t $PANE
        tmux send-keys "ssh $REMOTE" 'C-m'
        tmux send-keys "htop" 'C-m'
        tmux splitw -h -p $P_WIDTH
        PANE=$(( $PANE + 1 ))
done

PANE=0
tail -n +1 remotes.txt |  while read -r line
do
        REMOTE=$(echo $line | awk 'BEGIN { FS="|"} {print $1}')
        COUNT=$(echo $line | awk 'BEGIN { FS="|"} {print $2}' | tr -d '[:space:]')
        
        tmux selectp -t $PANE
        tmux splitw -h -p 50
        tmux send-keys "ssh $REMOTE" 'C-m'
        tmux send-keys "gpu" 'C-m'
        PANE=$(( $PANE + 1 ))
done

tmux select-window -t worker-0
tmux -2 attach-session -t $SESS_NAME
