import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.gridspec as gridspec
import matplotlib.ticker as ticker
import matplotlib.colors
import matplotlib.cm as cm
import os
import logging
import scipy.misc
from scipy.misc import imresize
from glimpsenet.util.plotting import grid

logger = logging.getLogger('Plots')


class PlotGenerator(object):

    def __init__(self,
                 savepath,
                 normalized=False,
                 centered=True):

        self.savepath = savepath
        self.normalized = normalized
        self.centered = centered
        self.pred_thresh = 10

        # Set colormaps
        # For each location
        self.linecolors = ['lime', 'gold', 'fuchsia', 'yellow']
        self.edgecolors = ['green', 'orange', 'red', 'gold']
        self.signalcolors = ['orange', 'red', 'gold', 'green']
        self.facecolors = ['lime', 'sandybrown', 'salmon', 'palegoldenrod']
        self.markercolors = ['green', 'orange', 'red', 'gold']
        self.textcolors = ['red', 'red', 'red', 'red']

        norm = mpl.colors.Normalize(vmin=0, vmax=1)
        cmap = plt.get_cmap('RdYlGn')
        self.get_color = cm.ScalarMappable(norm=norm, cmap=cmap)

        self.csetsize = len(self.linecolors)

        # For timesequence first and rest
        self.markers = {'start': '^', 'seq': 'o'}

        self.prepareDirectories()

    def __call__(self,
                 record,
                 image,
                 low_res,
                 target,
                 accuracy=-1,
                 step=-1,
                 mode='train',
                 sample=None,
                 index=None):
        '''Make all plots.

        Arguments:
            record: contains all recorded information during one episode
            image: Image used as background
            target: True labels of image
            glimpse: Glimpse sequence
            accuracy: float value
        '''
        self.locations, \
            self.patch_sizes, \
            self.values, \
            self.rewards, \
            self.predictions, \
            self.pred_signals, \
            self.proposed_targets, \
            self.glimpses, \
            self.returns = record

        self.image = image
        self.low_res = low_res
        self.target = target  # targets in record can be subset of all targets
        self.acc = accuracy

        self.mode = mode
        self.step = step
        self.sample = sample
        self.index = index

        self.prepareData()
        # self.plotLocations()  # Heatmaps
        self.plotTrajectory()  # One image with all visited locations

    def prepareData(self):
        '''Set values, that are always the same.'''
        # lengths of sequences
        self.N_ts = len(self.locations)

        self.locations = np.squeeze(np.vstack(self.locations))
        self.patch_sizes = np.squeeze(np.hstack(self.patch_sizes))
        self.values = np.squeeze(np.hstack(self.values))
        gls = []
        for glimpse in self.glimpses:
            glimpse = np.transpose(glimpse[0], [1, 2, 0])
            gls.append(glimpse)
        self.glimpses = gls

        if self.normalized:
            if self.centered:
                self.extent = [-1, 1, -1, 1]
            else:
                self.extent = [0, 1, 0, 1]
        else:
            dx = self.image.shape[0]
            dy = self.image.shape[1]
            if self.centered:
                self.extent = [-dy, dy, -dx, dx]
            else:
                self.extent = [0, dy, 0, dx]

    def prepareDirectories(self):
        '''Setup subdirectories, where the plots will be saved according to
        their purpose.'''

        sp = self.savepath
        plots = os.path.join(sp, 'plots')
        evalDir = os.path.join(plots, 'eval')
        trainDir = os.path.join(plots, 'train')
        dataDir = os.path.join(sp, 'raw_data')
        subdirs = ['heatmaps', 'trajectories', 'views']
        allPaths = [plots, evalDir, trainDir, dataDir]
        self.dirDict = {'eval': {}, 'train': {}, 'raw': dataDir}
        for sub in subdirs:
            evalSub = os.path.join(evalDir, sub)
            trainSub = os.path.join(trainDir, sub)
            allPaths += [evalSub, trainSub]
            self.dirDict['eval'][sub] = evalSub
            self.dirDict['train'][sub] = trainSub

        for directory in allPaths:
            try:
                os.mkdir(directory)
            except Exception:
                pass

    def plotTrajectory(self):
        '''Plots of trajectories over the respective images'''

        # Helpers and data preparation
        if self.image.shape[-1] == 1:
            image = np.squeeze(self.image, -1)  # [N_show, dimx, dimy, ch]
        else:
            image = self.image
        try:
            labels = np.squeeze(np.asarray(self.target.cpu().numpy()))
        except Exception as e:
            print(e)
            labels = ['?']
        labels = [labels] if np.ndim(labels) == 0 else labels

        loc_seq = self.locations  # [N, 2]
        patch_seq = self.patch_sizes  # [N, 1]
        glimpses = self.glimpses
        preds = []
        pred_seq = []
        p_ts = []

        # TODO: get rid of this hack!!!
        if len(self.predictions) == 0 or self.predictions is None:
            self.predictions = [[-1]]
            self.proposed_targets = [[-1]]
        for ts, (p, t) in enumerate(zip(self.predictions,
                                        self.proposed_targets)):
            pred = np.squeeze(np.argmax(p, -1), 0)
            pred_seq.append(pred)
            if t is not None:
                preds.append(pred)
                p_ts.append(ts)
        o_c = self.pred_signals
        acc = self.acc   # scalar

        fig = plt.figure(figsize=(10, 10))

        gs = gridspec.GridSpec(2, 2,
                               height_ratios=[1, 0.4],
                               width_ratios=[1, 1.5])

        glx = gly = int(np.ceil(np.sqrt(len(glimpses) + 1)))
        gs_glimpses = gridspec.GridSpecFromSubplotSpec(
                glx,
                gly,
                subplot_spec=gs[:, 1],
                wspace=0.4)

        ax = plt.subplot(gs_glimpses[0])
        ax.imshow(np.squeeze(self.low_res), cmap='gray')

        for i in range(glx * gly - 1):
            ax = plt.subplot(gs_glimpses[i+1])
            if i >= len(glimpses):
                ax.axis('off')
                continue
            glimpse = glimpses[i]
            if glimpse.shape[-1] == 1:
                glimpse = np.squeeze(glimpse, -1)
            elif glimpse.shape[-1] == 3:
                pass
            else:
                glimpse = grid(np.transpose(glimpse, [2, 0, 1]))
            ax.imshow(glimpse, cmap='gray')
            ax.set_xticks([])
            ax.set_yticks([])
            lw = 4 if self.proposed_targets[i] is not None else 2
            lc = self.get_color.to_rgba(self.returns[i])
            for axis in ['top', 'bottom', 'left', 'right']:
                ax.spines[axis].set_linewidth(lw)
                ax.spines[axis].set_color(lc)
                # ax.spines[axis].set_linestyle('dashed')
                ax.spines[axis].set_capstyle('round')
            ax.text(-0.2, 1.0, str(pred_seq[i]),
                    verticalalignment='top', horizontalalignment='right',
                    transform=ax.transAxes,
                    fontsize=15)

        gs_timelines = gridspec.GridSpecFromSubplotSpec(
                2, 2,
                subplot_spec=gs[1, 0],
                width_ratios=[0.8, 0.2],
                hspace=0)

        ax_img = plt.subplot(gs[0, 0])
        # ax_img.axis('off')
        ps = ', '.join([str(p) for p in preds])
        ls = ', '.join([str(l) for l in labels])
        title = 'pred {} vs truth {} - {}-acc.: {:5.1f}%'\
                .format(ps, ls, self.mode, acc)
        logger.debug('image titel: {}'.format(title))
        ax_img.set_title(title)

        # Background image
        newExtent = np.copy(self.extent)
        # newExtent[2] = -newExtent[2]
        # newExtent[3] = -newExtent[3]
        ax_img.imshow(image,
                      cmap='gray',
                      interpolation='nearest',
                      origin='lower',
                      extent=newExtent)

        # all colors are the same -> no locations to iterate over anymore
        idx = 0
        # lines
        ax_img.plot(loc_seq[:, 1], loc_seq[:, 0],
                    c=self.linecolors[idx],
                    lw=3,
                    marker=self.markers['seq'])
        # start
        ax_img.plot(loc_seq[:, 1][0], loc_seq[:, 0][0],
                    c=self.linecolors[idx],
                    lw=3,
                    marker=self.markers['start'],
                    markersize=8)

        for ts, l in enumerate(loc_seq):
            l = np.array(l[::-1])  # [y, x]
            l = np.reshape(l, [2])
            # predictions per timestep
            p_t = pred_seq[ts]
            ax_img.text(l[0], l[1], p_t, color=self.textcolors[idx])

            p = patch_seq[ts]
            p_x, p_y = 2 * p, 2 * p

            shift_x = p + 0.5
            shift_y = p + 0.5

            shift = np.array([shift_y, shift_x])
            shift = np.reshape(shift, [2])

            if ts in p_ts:
                sig_o = True
            else:
                sig_o = False

            self.addPatch(ax_img, l-shift, p_x, p_y, sig_o, ts=ts)

        ax_img.set_xlim(self.extent[0], self.extent[1])
        ax_img.set_ylim(self.extent[3], self.extent[2])

        sample = self.sample if self.sample is not None else 'central'
        idx = self.index if self.index is not None else 0
        nameOfPlot = '{m} - step {num:07d} traj-{t:03d}--{s}.png'\
                     .format(m=self.mode, num=self.step, t=idx, s=sample)
        saveName = os.path.join(self.dirDict[self.mode]['trajectories'],
                                nameOfPlot)

        ax_val = plt.subplot(gs_timelines[1, 0])
        ax_sig = plt.subplot(gs_timelines[0, 0])

        for t in p_ts:
            ax_val.axvline(t, ls='-', color='red')
            ax_sig.axvline(t, ls='-', color='red')

        ax_sig.plot(o_c, label='output')
        # ax_sig.plot(e_c, label='eos')
        ax_sig.set_ylabel('Signal')
        ax_sig.set_xticks([])
        ax_sig.set_ylim(0, None)
        ax_sig.set_yticks(ax_sig.get_yticks()[1:])
        ax_sig.axhline(self.pred_thresh,
                       ls='--', color='gray')

        ax_val.plot(self.values, label='values')
        ax_val.plot(self.returns, label='returns')
        ax_val.set_ylabel('R, V')
        ax_val.set_xlabel('Time')
        top_lim = None if ax_val.get_ylim()[-1] > 1.5 else 1.5
        bot_lim = None if ax_val.get_ylim()[0] < -1.5 else -1.5
        ax_val.set_ylim(bot_lim, top_lim)
        ax_val.xaxis.set_minor_locator(ticker.NullLocator())
        ax_val.xaxis.set_major_locator(ticker.MultipleLocator(1))
        ax_val.xaxis.set_major_formatter(ticker.ScalarFormatter())

        h, l = ax_sig.get_legend_handles_labels()
        h_, l_ = ax_val.get_legend_handles_labels()
        h += h_
        l += l_
        ax_leg = plt.subplot(gs_timelines[0, 1])
        ax_leg.legend(h, l)
        ax_leg.axis('off')

        for ax in [ax_sig, ax_val]:
            ax.yaxis.set_label_coords(-0.15, 0.5)

        fig.savefig(saveName, dpi=300)
        logger.debug('plot saved to \'{}\''.format(saveName))
        fig.clf()
        plt.close('all')

    def addPatch(self, ax, l, p_x, p_y, signal, idx=0, ts=0):
        # filled, translucent patch
        fc = self.facecolors[idx]
        ec = self.get_color.to_rgba(self.returns[ts])
        ax.add_patch(
                patches.Rectangle(
                    l, p_y, p_x,
                    fc=fc,
                    ec=ec,
                    alpha=0.1))
        # solid edge
        ax.add_patch(
                patches.Rectangle(
                    l, p_y, p_x,
                    fill=False,
                    ec=ec,
                    alpha=1))
        if signal:
            # solid edge
            ax.add_patch(
                    patches.Rectangle(
                        l, p_y, p_x,
                        fill=False,
                        ec=ec,
                        ls='--',
                        lw=4,
                        alpha=1))

    def getCmap(self, cmap_name, max_val):
        ''' Make a custom cmap and make it binned to integer steps

        Args:
            cmap_name: name of the colormap to use. must be a mpl colormap
            max_val: highest possible value in the plot
        Returns:
            cmap: colormap
            bounds: bins where the color stays constant
            ticks: where to draw the lines
            norm: norm to use when plotting
        '''

        cmap = getattr(plt.cm, cmap_name)
        cmaplist = [cmap(i) for i in range(cmap.N)]
        # cmaplist[0] = (0.5, 0.5, 0.5, 1.0) # gray
        cmap = matplotlib.colors.ListedColormap(
                name='custom_cmap',
                colors=cmaplist,
                N=cmap.N)

        bounds = np.linspace(0, max_val, num=max_val+1, dtype=float)
        ticks = np.copy(bounds)
        bounds = np.insert(bounds, 1, 0.05)  # make sure, 0 has zero-color!
        logger.debug('bounds[:4]: {}'.format(bounds[:4]))
        norm = matplotlib.colors.PowerNorm(0.3, vmin=0, vmax=max_val)

        return cmap, bounds, ticks, norm

    def plotLocations_oneTimeline(self, AX, heatmaps, edges, cmap, norm):
        '''Plot heatmaps on given axes for one timeline'''
        minX, maxX, minY, maxY = edges
        logger.debug('edges: {}'.format(edges))

        for j in range(len(heatmaps)):
            ax = AX[j]
            ax.imshow(
                    np.swapaxes(heatmaps[j], 0, 1),
                    cmap=cmap,
                    interpolation='nearest',
                    extent=self.extent,
                    origin='lower',
                    norm=norm)
            ax.axis('off')
            ax.set_xlim(minX, maxX)
            ax.set_ylim(maxY, minY)
            ax.set_aspect(float(self.imsize[0])/float(self.imsize[1]))

    def setTitlesAndLabels(self, AX):
        shape = AX.shape
        if len(shape) == 1:
            AX = np.expand_dims(AX, axis=0)
            shape = AX.shape

        for label in range(shape[0]):
            labelText = 'All labels'
            if shape[0] != 1:
                labelText = 'label {}'.format(label)
            for j in range(shape[1]):
                ax = AX[label, j]
                if j == 0:
                    ax.text(0.1,
                            0.9,
                            labelText,
                            color='white',
                            ha='left',
                            va='top',
                            transform=ax.transAxes)
                if label == 0:
                    ax.set_title('t={0}'.format(j))

    def generateHeatmaps(self, locs, label=None):
        '''Given some location predictions give me a heatmap.

        Args:
            locs: location predictions
        Returns:
            heatmaps: 2d histograms of locations predictions
            edges: for setting x and y lims
        '''

        minX, maxX, minY, maxY = self.extent

        # generate heatmaps and track highest possible value
        heatmaps = []
        for i, ls in enumerate(locs):
            if label is None:
                x = ls[1]
                y = ls[0]
            else:
                x = ls[1][self.labels == label]
                y = ls[0][self.labels == label]
            gridx = np.linspace(minX, maxX, num=self.imsize[0]+1)
            gridy = np.linspace(minY, maxY, num=self.imsize[1]+1)

            heatmap, xedges, yedges = np.histogram2d(x, y, bins=[gridx, gridy])
            heatmaps.append(heatmap)

        edges = [xedges[0], xedges[-1], yedges[0], yedges[-1]]
        return heatmaps, edges

    def getColorBar(self, ax, cmap, ticks, bounds, norm, maxTicks=10):
        '''Generate a colorbar

        Args:
            ax: axes in which the colorbar will be
            cmap: colormap
            ticks: all the ticks you want to draw (will be altered if too many)
            bounds: where the colors will be
            norm: manipulation to the distribution of color
            maxTicks: total number of ticks, you want to have
        Returns:
            cbar: mpl.color.ColorbarBase instance
        '''

        if len(ticks) > maxTicks:
            start = ticks[0]
            end = ticks[-1]
            ticks = np.linspace(
                    start,
                    end,
                    num=maxTicks,
                    endpoint=True,
                    dtype=int)

            order = 0
            someNumber = np.mean(ticks)
            while someNumber > 1:
                someNumber /= 10.
                order += 1

            ticks = np.around(ticks, decimals=-(order-1))

        cbar = mpl.colorbar.ColorbarBase(
                ax=ax,
                cmap=cmap,
                norm=norm,
                spacing='proportional',
                ticks=ticks,
                boundaries=bounds,
                format='%1i',
                orientation='horizontal')

        return cbar

    def plotLocations(self):
        locs = self.locs  # shape: [bs, timesteps, 2]
        locs = np.swapaxes(locs, 0, 1)
        locs = np.swapaxes(locs, 1, 2)  # shape: [timesteps, 2, bs]
        logger.debug('heatmap locations shape: {}'.format(locs.shape))

        self.plotLocationsOverview(locs)
        # self.plotLocationsPerLabel(locs)

    def plotLocationsOverview(self, locs):

        acc = self.acc

        minX, maxX, minY, maxY = self.extent

        fig, AX = plt.subplots(
                1,
                self.time_steps,
                figsize=(16, 7))
        AX = np.reshape(AX, [self.time_steps])
        plt.figtext(
                0.1,
                0.9,
                'Patch location predicted by the emission network\n'
                '{}-accuracy={}%'
                .format(self.mode, acc),
                color='black',
                ha='left',
                va='top',
                transform=AX[0].transAxes)

        heatmaps, edges = self.generateHeatmaps(locs)
        globalVmax = np.max(heatmaps)

        cmap, bounds, ticks, norm = self.getCmap('viridis', globalVmax)

        self.plotLocations_oneTimeline(AX, heatmaps, edges, cmap, norm)

        cbar_ax = fig.add_axes([0.18, 0.1, 0.665, 0.05])
        cbar = self.getColorBar(cbar_ax, cmap, ticks, bounds, norm)
        cbar.set_label('location counts per pixel')

        self.setTitlesAndLabels(AX)

        savename = os.path.join(
                self.dirDict[self.mode]['heatmaps'],
                '{m} - step {num:07d} Locations_Overview.png'
                .format(m=self.mode, num=self.step))
        fig.savefig(savename, dpi=90)
        fig.clf()
        plt.close()

    def plotLocationsPerLabel(self, locs):
        '''Give an overview of location predictions filtered by the
        corresponding true label.'''

        numLabels = self.numLabels

        plots_in_y = numLabels*self.numLocations

        fig, AX = plt.subplots(
                plots_in_y,
                self.time_steps,
                figsize=(9, 16))

        AX = np.reshape(AX, [numLabels, self.time_steps])

        heatmaps = []
        for label in range(AX.shape[0]):
            heat, edges = self.generateHeatmaps(locs, label=label)
            heatmaps.append(heat)

        globalVmax = np.max(heatmaps)
        cmap, bounds, ticks, norm = self.getCmap('viridis', globalVmax)

        for label, subAX in enumerate(AX):
            self.plotLocations_oneTimeline(
                    AX=subAX,
                    heatmaps=heatmaps[label],
                    edges=edges,
                    cmap=cmap,
                    norm=norm)

        plt.figtext(
                0.1,
                0.95,
                'Patch location predicted by the emission network\npatch '
                'size={}, {}-accuracy={}%'
                .format(self.patchSizes, self.mode, self.acc),
                color='black')

        fig.subplots_adjust(hspace=0.02, wspace=0.05)

        cbar_ax = fig.add_axes([0.18, 0.07, 0.665, 0.025])
        cbar = self.getColorBar(
                cbar_ax,
                cmap=cmap,
                ticks=ticks,
                bounds=bounds,
                norm=norm,
                maxTicks=6)
        cbar.set_label('location counts per pixel and true label')

        self.setTitlesAndLabels(AX)

        savename = os.path.join(
                self.dirDict[self.mode]['heatmaps'],
                '{m} - step {num:07d} Locations_perLabel.png'
                .format(m=self.mode, num=self.step))
        fig.savefig(savename, dpi=90)
        fig.clf()
        plt.close()

    def what_does_the_net_see(self, glimpses, patch_sizes):
        '''Generate as timeline a stacked and upscaled view, of what the glimpse
        net gets as input.

        Args:
            glimpses: [nested] list of all glimpses.
            patch_sizes: list of integer patch sizes, corresponding to the
                    glimpses.

        Returns:
            im_array: numpy array, containing the timeline

        Example:
            If there are three glimpses per timestep the shapes of glimpses and
            patch_sizes are:
            glimpses:       [ts, 3, ...]
            patch_sizes:    [3]
            The sizes supplied in patch_sizes will correspond to the glimpse
            with the same last index:
            glimpse[i,j] -> patch_sizes[j]
        '''

        max_size = np.max(patch_sizes)
        num_ts = np.shape(glimpses)[0]

        # sort glimpses by size from large to small
        indeces = np.argsort(patch_sizes)
        indeces = indeces[::-1]  # reverse order
        patch_sizes = patch_sizes[indeces]
        num_channels = np.shape(glimpses)[-1]
        if num_channels == 1:
            im_array = np.zeros([max_size, num_ts*max_size])
        else:
            im_array = np.zeros([max_size, num_ts*max_size, num_channels])

        start = np.array([0, 0])
        for ts, G in enumerate(glimpses):
            # sort glimpses by size from large to small
            G = np.copy(G)[indeces]
            for i, g in enumerate(G):
                if num_channels == 1:
                    g = np.squeeze(g)
                ps = patch_sizes[i]
                sx, sy = start
                g_new = imresize(g, size=tuple([ps, ps]), interp='bilinear')
                if num_channels == 1:
                    im_array[sx:sx+ps, sy:sy+ps] = g_new
                else:
                    im_array[sx:sx+ps, sy:sy+ps, :] = g_new

                next_patch_index = i + 1
                if not next_patch_index >= len(indeces):
                    start += int((ps - patch_sizes[i+1])/2 + 1)
            start = np.array([0, (ts+1) * patch_sizes[0]])

        return im_array

    def net_view(self):
        '''Plot the timelines generated by what_does_the_net_see'''

        N = self.N_show
        samples = self.glimpseSamples[:N]
        patch_sizes = self.config.getParameter('HyperParameters/patchSizes')
        patch_sizes = np.array(patch_sizes)

        num_samples = np.shape(samples)[0]
        print(num_samples)
        print(samples)
        f, AX = plt.subplots(num_samples, 1, figsize=(3, 0.8*num_samples))

        for i, (sample, ax) in enumerate(zip(samples, AX)):
            im_array = self.what_does_the_net_see(sample, patch_sizes)

            # Save single image
            name = '{m} - step {num:07d} net_view_{v:03d}.jpeg'\
                   .format(m=self.mode, num=self.step, v=i)
            save_path = os.path.join(self.dirDict[self.mode]['views'], name)
            scipy.misc.imsave(save_path, im_array)

            ax.imshow(im_array, interpolation='nearest', cmap='gray')
            ax.axis('off')

        nameOfPlot = '{m} - step {num:07d} net_view.png'\
                     .format(m=self.mode, num=self.step)
        f.savefig(os.path.join(self.dirDict[self.mode]['views'], nameOfPlot),
                  dpi=90)
