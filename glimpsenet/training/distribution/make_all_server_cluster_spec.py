import argparse
import json
import numpy as np


def load_free(path_to_file):
    with open(path_to_file) as json_data:
        data = json.loads(json_data.read())
    return data


def make_line(*args):
    args = [str(a) for a in args]
    line = ' | '.join(args)
    line += '\n'

    return line


def write_to_file(line_list, file_name):
    with open(file_name, 'w+') as file:
        for line in line_list:
            file.write(make_line(*line))


def gpu_dict(raw_data, min_space, max_per_gpu=6):
    workable_gpu_dict = {}
    for adress, values in raw_data.iteritems():
        name = values['name']
        gpus = values['space']
        if gpus is not None:
            for dev, space in gpus.iteritems():
                free = space['free']
                total = space['total']

                workable_gpu_dict['{}_{}'.format(adress, dev)] = []
                worker = 0
                while free >= min_space:
                    frac = round(float(min_space) / float(total), 2)
                    workable_gpu_dict['{}_{}'.format(adress, dev)] \
                        += [[name, adress, dev, frac]]
                    free -= min_space
                    worker += 1
                    if worker >= max_per_gpu:
                        break
    return workable_gpu_dict


def priority_sorted_list(gpu_dict, N_worker):
    sorted_gpu_list = []
    N = N_worker
    while gpu_dict:
        lengths = []
        names = []
        for iden, entries in gpu_dict.iteritems():
            lengths.append(len(entries))
            names.append(iden)
        max_l = max(lengths)
        idx = lengths.index(max_l)
        sorted_gpu_list += gpu_dict[names[idx]]
        N -= max_l
        del gpu_dict[names[idx]]

    return sorted_gpu_list


def prettify_line_list(line_list):
    line_list = np.array(line_list, dtype=str)
    for i in range(line_list.shape[1]):
        max_l = max([len(entry) for entry in line_list[:, i]])
        for j, entry in enumerate(line_list[:, i]):
            l = len(entry)
            d = max_l - l
            entry += ' ' * int(d)
            line_list[j, i] = entry

    return line_list


def translate_to_spec(data,
                      num_gpu=None,
                      spec_name='all_servers_spec.txt',
                      remote_name='remotes.txt'):
    min_space = 1000
    server_home = "~/glimpsenet/glimpsenet/training/distribution"
    server_preamble = "source ~/virtualenv/pytorch-0.2/bin/activate"

    local_name = 'johannes'
    local_adress = '129.206.117.34'
    local_home = '~/Documents/Uni\ HD/Masterarbeit/code/' \
                 + 'glimpsenet_copy/glimpsenet/training/distribution'
    local_preamble = ''

    line_list = []
    remote_list = {}

    line_list.append(['# name', 'adress', 'port', 'job', 'task', 'dev', 'frac',
                      'home', 'preamble'])
    task_index = 0
    port = 2222
    # Local parameter server and workers
    # ps
    line_list.append([local_name,
                      local_adress,
                      port,
                      'ps',
                      0,
                      'None',
                      0.1,
                      local_home,
                      local_preamble])
    port += 1
    # Titan X
    remote_list['{}@{}'.format(local_name, local_adress)] = 0
    for i in range(5):
        line_list.append([local_name,
                          local_adress,
                          port,
                          'worker',
                          task_index,
                          0,
                          0.4,
                          local_home,
                          local_preamble])
        remote_list['{}@{}'.format(local_name, local_adress)] += 1
        task_index += 1
        port += 1
    # GTX 980
    line_list.append([local_name,
                      local_adress,
                      port,
                      'worker',
                      task_index,
                      1,
                      0.8,
                      local_home,
                      local_preamble])
    remote_list['{}@{}'.format(local_name, local_adress)] += 1
    task_index += 1
    port += 1

    # Remote workers
    num_external = 0

    # Store information about remote gpus in a dict, to later make more
    # sophisticated decisions
    workable_gpu_dict = gpu_dict(data, min_space)

    # Put the gathered information in a list sorted by priority (how many
    # trainings can fit on one gpu?)
    sorted_gpu_list = priority_sorted_list(workable_gpu_dict, min_space)

    # Iterate over that list until all or sufficiantly many gpus are
    # occupied
    port = 2222
    last_name = sorted_gpu_list[0][0]
    for [name, adress, dev, frac] in sorted_gpu_list:
        if num_gpu is not None and num_gpu == num_external:
            break
        if '{}@{}'.format(name, adress) not in remote_list:
            remote_list['{}@{}'.format(name, adress)] = 0

        line_list.append([name,
                          adress,
                          port,
                          'worker',
                          task_index,
                          dev,
                          frac,
                          server_home,
                          server_preamble])
        remote_list['{}@{}'.format(name, adress)] += 1
        if name == last_name:
            port += 1
        else:
            port = 2222
        task_index += 1
        num_external += 1
        last_name = name

    line_list.append([local_name,
                      local_adress,
                      9999,
                      'eval',
                      0,
                      1,
                      0.1,
                      local_home,
                      local_preamble])

    line_list = prettify_line_list(line_list)

    write_to_file(line_list, file_name=spec_name)
    write_to_file(list(remote_list.items()), file_name=remote_name)


def load_and_translate(path_to_file, num_gpus=None):
    data = load_free(path_to_file)
    translate_to_spec(data, num_gpus)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--N', type=int,
                        help='Number of GPUs to grab.')

    args = parser.parse_args()
    num_gpus = args.N

    load_and_translate('free_space.json', num_gpus)
