#!/bin/bash -l
# This script starts the training


# Helper function to conveniently and reusably set training parameters outside
# of a config file
train_call() {
        cd ../..
        python A3C_Worker.py \
                --dset gM \
                --subset small-sort \
                --maxObjs 1 \
                --difficulty 1 \
                --learningRate 1e-4 \
                --learningDecay 0.997 \
                --l 0.01 \
                --M 30 \
                --batchSize 1 \
                --sigma_em 15 \
                --sigma_sig 0.2 \
                --patchSizes 11 28 56 \
                --verbose 2 \
                --lstmInit context \
                --reuse "$1" \
                --type "$2" \
                --task "$3" \
                --schedule "$4" \
                --gpuFraction "$5"
}

                # --restore_path /media/johannes/Data\ 1/Masterarbeit/Trainings/gn_v4_l1-p11-28-56-dgM/0-Proprietor-1/model.ckpt-302369 \
                # --restore_path /media/johannes/Data\ 1/Masterarbeit/Trainings/gn_v4_l1-p11-28-56-dgM/0-Proprietor-0/model.ckpt-320783 \


# Argument Parsing
while [ $# -gt 0 ]; do
        case "$1" in
                --reset=*)
                  RESET="${1#*=}"
                  ;;
                --type=*)
                  TYPE="${1#*=}"
                  ;;
                --task=*)
                  TASK="${1#*=}"
                  ;;
                --gf=*)
                  GF="${1#*=}"
                  ;;
        esac
        shift
done


# Set implicit Arguments
if [[ $RESET == "r" || $RESET == "reset" ]]; then
        REUSE=False
else
        REUSE=True
fi

if [[ $TYPE == "eval" ]]; then
        SCHEDULE=continuous_eval
else
        SCHEDULE=train
fi


# Actually run the experiment
train_call "$REUSE" "$TYPE" "$TASK" "$SCHEDULE" "$GF"
