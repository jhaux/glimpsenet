import torch
import torch.nn as nn
import torch.nn.functional as func
import logging

from util.layerutils import activation

logger = logging.getLogger(__name__)


class GlimpseNet(nn.Module):
    ''' The Glimpse Network

    Inputs:
        x: image patch
        l: location of patch
        ps: size of patch

    outputs:
        g = G_image(x|W_image) * G_loc(l, ps|W_loc)
    '''

    def __init__(self, architecture):
        '''
        Arguments:
            architecture: config dict containing all parameters
        '''
        super(GlimpseNet, self).__init__()

        a = architecture
        ai = a['ImageNet']
        self.aic1 = ai['conv1']
        self.aic2 = ai['conv2']
        self.aic3 = ai['conv3']
        self.aif1 = ai['fc1']
        al = a['LocationNet']
        self.alf1 = al['fc1']

        # Conv2d inputs: n_in, n_out, k_size
        self.im_conv1 = nn.Conv2d(self.aic1['n_in'], self.aic1['n_out'],
                                  self.aic1['fs'])
        self.aic1 = activation(self.aic1['act'])
        self.im_conv2 = nn.Conv2d(self.aic2['n_in'], self.aic2['n_out'],
                                  self.aic2['fs'])
        self.aic2 = activation(self.aic2['act'])
        self.im_conv3 = nn.Conv2d(self.aic3['n_in'], self.aic3['n_out'],
                                  self.aic3['fs'])
        self.aic3 = activation(self.aic3['act'])
        self.im_fc1 = nn.Linear(self.aif1['n_in'], self.aif1['n_out'])
        self.aif1 = activation(self.aif1['act'])

        self.l_fc1 = nn.Linear(self.alf1['n_in'], self.alf1['n_out'])
        self.alf1 = activation(self.alf1['act'])

    def forward(self, x, l, ps):
        # ensure that imputs are viewed as leaves
        x = x.detach()
        l = l.detach()
        ps = ps.detach()

        im_net = self.aic1(self.im_conv1(x))
        im_net = self.aic2(self.im_conv2(im_net))
        im_net = self.aic3(self.im_conv3(im_net))
        im_net = im_net.view(im_net.size(0), -1)
        im_net = self.aif1(self.im_fc1(im_net))

        l_ps = torch.cat([l, ps], 1)
        l_net = self.alf1(self.l_fc1(l_ps))

        g = l_net * im_net

        return g


class EmissionNet(nn.Module):
    ''' The Emission Network

    Inputs:
        c2: Feature vector of the 2nd reccurent Layer

    Outputs:
        l: \in [0,1] encoded location of the next glimpse
    '''

    def __init__(self, architecture):
        '''
        Argmuents:
        architecture: config dict containing all parameters
        '''
        super(EmissionNet, self).__init__()

        self.a = architecture
        self.af1 = self.a['fc1']
        self.af2 = self.a['fc2']

        self.fc1 = nn.Linear(self.af1['n_in'], self.af1['n_out'])
        self.af1 = activation(self.af1['act'])
        self.fc2 = nn.Linear(self.af2['n_in'], self.af2['n_out'])

        self.ps_act = activation('sigmoid')
        self.l_act = activation('tanh')
        self.sig_scale = self.a['signalScale']
        self.em_scale = self.a['unit_pixels']

    def forward(self, h2):
        e = self.af1(self.fc1(h2))
        e = self.fc2(e)

        # Get emissions
        x, y, ps = e.split(1, dim=1)

        # Project onto correct spaces
        l = torch.cat([x, y], 1)
        l = self.l_act(l) * self.em_scale
        ps = self.ps_act(ps) * self.em_scale

        return l, ps


class ContextNet(nn.Module):
    ''' The Context Network

    Inputs:
        image_coarse: Downscaled version of the image of interest

    Outputs:
        c2: initialization of the state of the second lstm cell
    '''

    def __init__(self, architecture):
        '''
        Arguments:
            architecture: config dict containing all parameters
        '''
        super(ContextNet, self).__init__()

        a = architecture
        self.ac1 = a['image_net']['conv1']
        self.ac2 = a['image_net']['conv2']
        self.ac3 = a['image_net']['conv3']
        self.af1 = a['image_net']['fc1']
        self.afs1 = a['size_net']['fc1']
        self.afs2 = a['size_net']['fc2']

        # Conv2d inputs: n_in, n_out, k_size
        self.conv1 = nn.Conv2d(self.ac1['n_in'], self.ac1['n_out'],
                               self.ac1['fs'])
        self.ac1 = activation(self.ac1['act'])
        self.conv2 = nn.Conv2d(self.ac2['n_in'], self.ac2['n_out'],
                               self.ac2['fs'])
        self.ac2 = activation(self.ac2['act'])
        self.conv3 = nn.Conv2d(self.ac3['n_in'], self.ac3['n_out'],
                               self.ac3['fs'])
        self.ac3 = activation(self.ac3['act'])
        self.fc1 = nn.Linear(self.af1['n_in'], self.af1['n_out'])
        self.af1 = activation(self.af1['act'])

        # self.fcs1 = nn.Linear(self.afs1['n_in'], self.afs1['n_out'])
        # self.afs1 = activation(self.afs1['act'])
        # self.fcs2 = nn.Linear(self.afs2['n_in'], self.afs2['n_out'])
        # self.afs2 = activation(self.afs2['act'])

    def forward(self, image_coarse, imsize):
        im = self.ac1(self.conv1(image_coarse.detach()))
        im = self.ac2(self.conv2(im))
        im = self.ac3(self.conv3(im))
        im = im.view(im.size(0), -1)
        im = self.af1(self.fc1(im))

        # size = self.afs1(self.fcs1(imsize.detach()))
        # size = self.afs2(self.fcs2(size))

        c2 = im  # * size

        return c2


class ClassificationNet(nn.Module):
    ''' The Classification Network

    Inputs:
        c1: Output of the bottom LSTM cell

    Outputs:
        pred: prediction vector with probabilities for labels per element
    '''

    def __init__(self, architecture):
        '''
        Arguments:
            architecture: config dict containing all parameters
        '''
        super(ClassificationNet, self).__init__()

        self.a = architecture
        self.af1 = self.a['fc1']
        self.af2 = self.a['fc2']

        self.fc1 = nn.Linear(self.af1['n_in'], self.af1['n_out'])
        self.a1 = activation(self.af1['act'])
        self.fc2 = nn.Linear(self.af2['n_in'], self.af2['n_out'])
        self.a2 = activation(self.af2['act'])

    def forward(self, c1):
        pred = self.a1(self.fc1(c1))
        pred = self.a2(self.fc2(pred))

        return pred


class Baseline(nn.Module):
    '''Mapping the hidden state of the top LSTM cell to the expected reward.

    Arguments:
        c2: hidden state of the top LSTM cell of the DRAM

    Outputs:
        pred: prediction vector with probabilities for labels per element
    '''

    def __init__(self, architecture):
        '''
        Arguments:
            architecture: config dict containing all parameters
        '''
        super(Baseline, self).__init__()

        self.a = architecture
        self.af1 = self.a['fc1']
        self.af2 = self.a['fc2']

        self.fc1 = nn.Linear(self.af1['n_in'], self.af1['n_out'])
        self.a1 = activation(self.af1['act'])
        self.fc2 = nn.Linear(self.af2['n_in'], self.af2['n_out'])
        self.a2 = activation(self.af2['act'])

    def forward(self, c2):
        baseline = self.fc1(c2)
        baseline = self.a1(baseline)
        baseline = self.fc2(baseline)
        baseline = self.a2(baseline)

        return baseline
