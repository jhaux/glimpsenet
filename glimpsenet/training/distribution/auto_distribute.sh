#!/bin/bash -l
# This script uses tmux and ssh to setup all servers needed for training, that
# are specified in a config file called cluster_spec.txt. Each server is started
# a different tmux pane in the same tmux session. 
# To only kill the current training execute this script with the argument 'k'
# To not load old weights, thus restarting training pass the argument 'r'

# Argument parsing
while [ $# -gt 0 ]; do
        case "$1" in
                k)
                  KILLIT=true
                  ;;
                K)
                  HARDKILL=true
                  ;;
                kk)
                  HARDKILL=true
                  ;;
                r)
                  RESET=$1
                  ;;
                reset)
                  RESET=$1
                  ;;
        esac
        shift
done

# kill old training session and ensure that all running processes are killed
SESS_NAME=training

tmux kill-session -t $SESS_NAME

if [[ $HARDKILL = true ]]; then
    # Exit without making plots
    exit
fi

if [[ $KILLIT = true ]]; then
    # Used to only stop the training and not start a new one
    # When training is killed by hand: make videos from plots
    cd ../../util
    ./make_heatmap_video.sh
    ./make_trajectory_video.sh
    exit
fi

# push changes in the repo
# git checkout training
git update-index -q --refresh
CHANGED=$(git diff-index --name-only HEAD --)
if [[ -n $CHANGED ]]; then
    git add cluster_spec.txt  # Only automatically add cluster_spec
    git add all_servers_spec.txt  # Only automatically add cluster_spec
    git add one_remote_spec.txt  # Only automatically add cluster_spec
    git add ../train.sh  # train.sh contains updates to config
    git add ../config.py  # config.py contains updates to config
    git commit -m "automated push of cluster_spec when starting training."
    git push
fi

# Create tmux Session
tmux new-session -d -s $SESS_NAME
while ! tmux has-session -t $SESS_NAME; do sleep 1; done

# Given the config file setup all servers
tail -n +2 cluster_spec.txt |  while read -r line
do
        NAME=$(echo $line | awk 'BEGIN { FS="|"} {print $1}')
        ADDRESS=$(echo $line | awk 'BEGIN { FS="|"} {print $2}')
        JOB=$(echo $line | awk 'BEGIN { FS="|"} {print $4}' | tr -d '[:space:]')
        TASK=$(echo $line | awk 'BEGIN { FS="|"} {print $5}')
        PARENT=$(echo $line | awk 'BEGIN { FS="|"} {print $8}')
        PREAMBLE="$(echo $line | awk 'BEGIN { FS="|"} {print $9}')"
        REMOTE=$(echo "$NAME@$ADDRESS" | tr -d '[:space:]')

        tmux send-keys "ssh $REMOTE" 'C-m'
        tmux send-keys "$PREAMBLE"
        tmux send-keys 'C-m'
        tmux send-keys "cd $PARENT" 'C-m' 
        tmux send-keys "git pull" 'C-m'
        tmux send-keys "sleep 10" 'C-m'
        tmux send-keys "nice -2 bash start_local_server.sh $JOB $TASK $RESET" 'C-m'
        tmux rename-window $(echo "$JOB-$TASK" | tr -d '[:space:]')
        tmux new-window
done

# in the last window start tensorboard after all directories are setup
tmux send-keys 't' 'C-m'
tmux send-keys 'sleep 30' 'C-m'
tmux send-keys 'cd $(find . -maxdepth 2 -type d -exec stat -c "%y %n" {} + | sort -r | head -n1 | cut -d " " -f 4-)' 'C-m'
tmux send-keys 'tb' 'C-m'

tmux select-window -t worker-0
tmux -2 attach-session -t $SESS_NAME
