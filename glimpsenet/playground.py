import torch
import torch.multiprocessing as mp
import gym
import gym_multiMnist
import time
import os
from multiprocessing import set_start_method
from A3C.A3C import A3C_Worker
from A3C.train import train
from A3C.test import test
from training.config import configure
from util.fileManagement import DirectoryManager
from util.multiprocessing_ import Counter, LoggingCounter
from util.logger import Logger


def count(counter, step_init=0, M=50, bs=10):
    t_start = time.time()
    step_old = step_init
    while True:
        t_interval = 20.
        time.sleep(t_interval)
        step_new = counter.value()

        # If the algo is so slow that one cannot even get an update in 20 Secs
        # Just wait..........................................................
        if step_new == step_old:
            continue

        n_steps = step_new - step_old
        step_old = step_new
        t_dur_s = time.time() - t_start
        days, rem = divmod(t_dur_s, 24*3600)
        hours, rem = divmod(rem, 3600)
        mins, secs = divmod(rem, 60)

        outputs = []
        outputs.append('step {:7> d}'.format(step_new))
        outputs.append('elapsed: {:2> d}, {:2>0d}:{:2>0d}:{:04.1f}'
                       .format(int(days), int(hours), int(mins), secs))
        outputs.append('{:4.2f}updates/s'.format(n_steps / t_interval))
        outputs.append('{:4.2f}episodes/s'.format(n_steps*M*bs / t_interval))
        print(' - '.join(outputs))


def save(counter, worker, ckpt_path):
    save_path = ckpt_path + '/{:7>0d}_model.torch.tar'
    step_old = counter.value()
    while True:
        t_interval = 10.
        time.sleep(t_interval)
        step_new = counter.value()
        n_steps = step_new - step_old

        if n_steps >= 2000:
            step_old = step_new
            torch.save(worker.state_dict(), save_path.format(step_new))
            print('Saved model to {}'.format(save_path.format(step_new)))


def plot(counter, worker):
    step_old = counter.value()
    delta_n_logs = 0
    while True:
        t_interval = 10.
        time.sleep(t_interval)
        step_new = counter.value()
        n_steps = step_new - step_old
        log_samples = False

        if n_steps >= 100:
            if delta_n_logs >= 10:
                delta_n_logs = 0
                log_samples = True
            step_old = step_new
            worker.train_step(True)
            worker.log(step_new, log_samples, make_plot=True)
            delta_n_logs += 1


if __name__ == '__main__':
    set_start_method('spawn')

    num_processes = 3
    config = configure()
    alternate_restore_path = config.getParameter('restorePath')
    M = config.getParameter('HyperParameters/M')
    bs = config.getParameter('batchSize')

    Dirs = DirectoryManager(config)

    ckpt = Dirs.ckpt_path

    fss = config.getParameter('HyperParameters/fixedStepSize')
    step_size = config.getParameter('HyperParameters/predSteps')

    n_o = config.getParameter('HyperParameters/maxObjs')
    dif = config.getParameter('Dataset/difficulty')

    env_params = {
        'N_obj': n_o,
        'difficulty': dif,
        'sorted_targets': True,
        'fss': None if not fss else step_size,
        'mode': 'train',
        'dset': config.getParameter('Dataset/dset'),
        'imsize': config.getParameter('HyperParameters/imsize')
        }

    env_id = 'MultiMnist-{}.{}-v1'.format(n_o, dif)
    gym.envs.register(id=env_id,
                      entry_point='gym_multiMnist.envs:MultiMnistEnv',
                      kwargs=env_params)

    env = gym.make(env_id)
    shared_worker = A3C_Worker(env, config, ckpt, parameter_server=True)
    shared_worker.initialize_variables(alternate_restore_path)
    # shared_worker.set_as_trainable('fcs', 'baseline')
    shared_worker.share_memory()

    step_init = shared_worker.step
    counter = Counter(step_init)
    logging_counter = LoggingCounter(step_init)
    logging_queue = mp.Queue()

    processes = []

    p_test = mp.Process(target=test, args=(-1, config, ckpt,
                                           shared_worker,
                                           counter,
                                           logging_queue))
    p_test.start()
    processes.append(p_test)

    for rank in range(num_processes):
        if rank == 0:
            lc = logging_counter
            lq = logging_queue
        else:
            lc, lq = None, None
        p = mp.Process(target=train, args=(rank, config, ckpt,
                                           shared_worker,
                                           counter,
                                           lc,
                                           lq))
        p.start()
        processes.append(p)

    counter_prc = mp.Process(target=count, args=(counter, step_init, M, bs))
    counter_prc.start()
    processes.append(counter_prc)

    save_prc = mp.Process(target=save, args=(counter, shared_worker, ckpt))
    save_prc.start()
    processes.append(save_prc)

    plot_prc = mp.Process(target=plot, args=(counter, shared_worker))
    plot_prc.start()
    processes.append(plot_prc)

    train_logger = Logger(os.path.join(ckpt, 'logs/train'))
    test_logger = Logger(os.path.join(ckpt, 'logs/test'))

    while True:
        logs = logging_queue.get(1)
        step = logs['step']
        mode = logs['mode']
        if mode == 'train':
            logger = train_logger
            for tag, value in logs['hists'].items():
                logger.histo_summary(tag, value, step)
            for tag, value in logs['images'].items():
                logger.image_summary(tag, value, step)
        else:
            logger = test_logger

        for tag, value in logs['scalars'].items():
            logger.scalar_summary(tag, value, step)
        print('{}-logging at step {:7> d}'.format(mode, step))

    for p in processes:
        p.join()
