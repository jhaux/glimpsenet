import logging
import tensorflow as tf
from trainParser import getParser

args = getParser().parse_args()
if args.debug == 0:
    tf_verbose = tf.logging.ERROR
    my_verbose = logging.ERROR
elif args.debug == 1:
    tf_verbose = tf.logging.INFO
    my_verbose = logging.INFO
elif args.debug >= 2:
    tf_verbose = tf.logging.DEBUG
    my_verbose = logging.DEBUG

tf.logging.set_verbosity(tf_verbose)

logging.basicConfig(
        level=logging.DEBUG,
        format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
        datefmt='%m-%d %H:%M',
        filename='trainLog.log',  # must not be changed -> used in Dirs.saveLog
        filemode='w',
        propagate=False)
# define a Handler which writes INFO messages or higher to the sys.stderr
console = logging.StreamHandler()
console.setLevel(my_verbose)
# set a format which is simpler for console use
formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
# tell the handler to use this format
console.setFormatter(formatter)
# add the handler to the root logger

logger = logging.getLogger('custom logging')
logger.debug('set up logging')
