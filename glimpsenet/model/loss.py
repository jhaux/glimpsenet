# -*- coding: utf-8 -*-

import tensorflow as tf
import numpy as np
import logging

from monte_carlo_sampler import MonteCarloSampler
from rewards import rewards, combined_rewards, expand_rewards
from util.decorators import addVariableScope
from util.summary_helper import summaries

logger = logging.getLogger(__name__)


class Loss(object):
    '''Sets up loss calculation for a DRAM.'''

    def __init__(self, DRAM, Baseline, targets, Config):
        '''Set Attributes:

        Arguments:
            DRAM: Deep Recurrent Attention Model
            targets: Groundtruth for the class predictions
            Config: Config instance, that contains neccessacry sampling
                parameters etc.
        '''

        self.DRAM = DRAM
        self.targets = targets
        self.Config = Config
        self.Sampler = MonteCarloSampler(Config)
        self.Baseline = Baseline

        getP = self.Config.getParameter
        self.prediction_interval = getP('HyperParameters/predSteps')
        self.max_objects = getP('HyperParameters/maxObjs')
        self.n_timesteps = getP('HyperParameters/n_timesteps')
        self.M = getP('HyperParameters/M')
        self.sigma = getP('HyperParameters/sigma')
        self.var_dev = getP('HyperParameters/variableDevice')

    @addVariableScope
    def getLoss(self, mode):
        '''Return scalar loss for one training step.

        Arguments:
            mode: Distinguish between train and eval

        Returns:
            F: scalar value of Free energy lower bound
        '''

        self.mode = mode
        emissions = self.DRAM.emission_sequence
        tf.summary.histogram('trajectory', self.DRAM.trajectory)
        tf.summary.histogram('patch_sizes', self.DRAM.patchSizes)

        if self.mode == tf.contrib.learn.ModeKeys.TRAIN:
            F, bl_regr = self.sample_and_evaluate(emissions)
        else:
            F = self.evalFreeEnergy()
            bl_regr = 0

        return -F, bl_regr

    @addVariableScope
    def evalFreeEnergy(self):
        '''Loss for Evaluation'''
        # Max of the corresponding gaussian pdf
        p_l = tf.reduce_mean(1.0 / (np.sqrt(2.0 * np.pi) * self.sigma))
        p_l = tf.to_float(p_l)
        log_pl = tf.log(p_l + 1e-10)

        # The original trajectory
        p_y = self.DRAM.prediction
        crossE = self.targets * tf.log(p_y + 1e-10)
        log_py = tf.reduce_sum(crossE, axis=-1)

        R = rewards(p_y, self.targets)
        R = expand_rewards(R, self.Config)
        b = tf.to_float(1)
        l = self.Config.getParameter('HyperParameters/l')

        F = self.free_energy(log_py, log_pl, R, b, l)

        return F

    @addVariableScope
    def sample_and_evaluate(self, emission):
        '''Evaluate samples and return results.

        Arguments:
            trajectory: Original Trajectories. Shape: [ts, bs, 2]

        Returns:
            F: scalar free Energy
        '''

        # sample around base trajectory
        sampled_emissions = self.Sampler(emission, self.M)
        # log probs
        log_pl_seqs = self.Sampler.logProbs(sampled_emissions)
        # iterate over all trajectories and evaluate
        preds, _, _, b_seqs = self.evaluateTrajectories(sampled_emissions)
        # calculate free energy
        targets = tf.tile(tf.expand_dims(self.targets, 0), [self.M, 1, 1, 1])
        F, bl_regr = self.scalarFreeEnergyAndBaselineLoss(
                preds,
                log_pl_seqs,
                b_seqs,
                targets)

        axis = len(sampled_emissions.shape.as_list()) - 1
        trajectories, patchSizes = tf.split(sampled_emissions, [2, 1], axis)
        tf.summary.histogram('sampled_trajectories', trajectories)
        tf.summary.histogram('sampled_patch_sizes', patchSizes)
        tf.summary.histogram('baselines', b_seqs)

        print 'Loss calculated'
        return F, bl_regr

    @addVariableScope
    def evaluateTrajectories(self, emissions):
        '''Build a tf.while_loop to evaluate all given trajectories

        Arguments:
            emissions: Tensor containing several Trajectories and corresponding
                       patch sizes.

        Returns:
            predictions: labels for images per trajectory
            p_y: Tensor with class probabilities
            c2: Tensor with hidden states of the top LSTM cell
            b: baselines asociated with c2
        '''

        M_dev = emissions.get_shape().as_list()[0]

        def getTensorArrays():
            names = ['preds_arr', 'pred_seqs_arr', 'c2_seqs_arr', 'b_seqs_arr']
            tas = []
            for i, name in enumerate(names):
                ta = tf.TensorArray(
                    dtype=tf.float32,
                    size=M_dev,
                    dynamic_size=False,
                    clear_after_read=True,
                    tensor_array_name=name)
                tas.append(ta)
            return tas

        def cond(m, *args):
            return m < M_dev

        def body(m, preds, p_y_seqs, c2_seqs, b_seqs):
            '''Arguments/Returns:
                m: sample index
                p_y_seqs: tensorArray storing class probability sequences
                c2_seqs: tensorArray storing hidden state sequences
            '''
            pred, p_y_seq, _, c2_seq = self.DRAM.eval(
                    emissions[m],
                    'Evaluate_Emission')
            b_seq = self.Baseline.mapTrajectory(c2_seq)

            preds = preds.write(m, pred)
            p_y_seqs = p_y_seqs.write(m, p_y_seq)
            c2_seqs = c2_seqs.write(m, c2_seq)
            b_seqs = b_seqs.write(m, b_seq)

            m += 1

            return m, preds, p_y_seqs, c2_seqs, b_seqs

        m_init = 0
        preds, p_y_seqs_init, c2_seqs_init, b_seqs_init = getTensorArrays()
        loop_vars = [m_init, preds, p_y_seqs_init, c2_seqs_init, b_seqs_init]

        results = tf.while_loop(cond, body, loop_vars)

        _, pred, p_y, c2, b = results  # index not needed

        predictions = pred.stack()
        rank = len(predictions.get_shape().as_list())
        # merged with existing shape -> None does not change already exs. value
        M = emissions.get_shape().as_list()[0]
        update_shape = [M] + [None] * (rank-1)

        predictions.set_shape(update_shape)
        p_y = p_y.stack()
        c2 = c2.stack()
        b = b.stack()

        # bookkeeping
        p_y.set_shape(update_shape)
        c2.set_shape(update_shape)
        b.set_shape(update_shape[:-1])

        logger.debug("predictions: {}".format(predictions))
        logger.debug("p_y: {}".format(p_y))
        logger.debug("c2: {}".format(c2))
        logger.debug("b: {}".format(b))

        return predictions, p_y, c2, b

    @addVariableScope
    def scalarFreeEnergyAndBaselineLoss(self, p_y, log_pl, b, targets):
        '''Calculate scalar Free Energy.
        Also take care of Baseline loss calculation.

        Arguments:
            p_y: Class Probabilities, shape: [M_dev, ts, bs, C]
            log_pl: Location LogProbabilities, shape: [M_dev, ts, bs]
            c2: Hidden states of top LSTM cell, shape: [M_dev, ts, bs, c2_size]
            M: number of samples

        Returns:
            F: Scalar Free Energy for Sample Batch
        '''

        logger.debug('p_y: {}'.format(p_y))
        logger.debug('targets: {}'.format(targets))

        with tf.variable_scope('Softmax_Cross_Entropy'):
            log = tf.log(tf.clip_by_value(p_y, 1e-8, 1.0))
            diff = log * targets

            log_py = tf.reduce_sum(diff, axis=-1)

        R = rewards(p_y, targets)
        tf.summary.scalar('train_accuracy', tf.reduce_mean(tf.to_float(R)))
        R = expand_rewards(R, self.Config)
        if self.Config.getParameter('HyperParameters/cumulativeReward'):
            R = combined_rewards(R)
        l = self.Config.getParameter('HyperParameters/l')

        F = self.free_energy(log_py, log_pl, R, b, l)

        # Also calculate Baseline loss and store it in the Baseline object
        bl_regr = self.Baseline.getLoss(R, b)

        return F, bl_regr

    @addVariableScope
    def free_energy(self, log_py, log_pl, R, b, l):
        '''Estimator for the free energy lower bound of the log likelihood
        \log{p(y|I,W)} = \log{ \sum_l p(l|I,W)p(y|l,I,W) }.
        Compute gradients, as described in arxiv.org/pdf/1412.7755v2.pdf

        Arguments:
            log_p_y = log(p(y|l, I, W)): probability distribution for correct
                label
                shape: [M, num_timesteps, batch, num_classes]
            log_p_l = p(l|I,W): probability distribution for trajectories
                shape: [M, num_timesteps, batch]
            R: Rewards, shape: [M, num_timesteps, batch]
            b: baselines, shape: [M, num_timesteps, batch]
            l: scaling factor

        Returns:
            F: Free Energy: F = 1/M \sum_{m=1}^M [log(p_y) + l(R-b)log(p_l)]
        '''

        logger.debug('Tensors supplied to the Free Energy function:')
        logger.debug('log_py:\t{}'.format(log_py))
        logger.debug('log_pl:\t{}'.format(log_pl))
        logger.debug('R:\t{}'.format(R))
        logger.debug('b:\t{}'.format(b))
        logger.debug('l:\t{}'.format(l))

        with tf.variable_scope('Free_Energy'):
            with tf.variable_scope('Prefactor'):
                b = tf.stop_gradient(b)
                R = tf.stop_gradient(R)
                reinforce_prefactor = tf.stop_gradient(l * (R - b))
                logger.debug('Reinforce prefactor tensor: {}'
                             .format(reinforce_prefactor))

            with tf.variable_scope('Trajectory_Loss'):
                F_tr = reinforce_prefactor * log_pl
                # F = self.curriculumMask(F, R)
                F_tr = tf.reduce_mean(F_tr)

            with tf.variable_scope('Classification_Loss'):
                F_cl = log_py
                F_cl = tf.reduce_mean(F_cl)

            with tf.variable_scope('Combined_loss'):
                F = F_cl + F_tr

            # Make summaries during training
            if self.mode == tf.contrib.learn.ModeKeys.TRAIN:
                sum_dict = {'Trajectory_Loss': F_tr}
                sum_dict_atobj = {'Prediction_Loss': log_py}
                summaries(sum_dict)
                summaries(sum_dict_atobj, at_obj=True)

                sum_dict_2 = {'rewards': R,
                              'baselines': b}
                summaries(sum_dict_2, split_time=True)

                tf.summary.scalar('F', F)

            return F

    @addVariableScope
    def curriculumMask(self, tensor, rewards, pos_reward=1):
        '''Generates a mask, that is 1 until the first negative reward and
        multiplies it to the supplied tensor.

        Arguments:
            tensor: tensor to which the mask is applied
            rewards: rewards tensor, that determines the values of the mask
            pos_reward: Value of the positive reward

        Returns:
            tensor: tensor multiplied elementwise with the generate mask
        '''

        # 1 where positive reward is given
        pos_rewards = tf.to_float(tf.equal(rewards, pos_reward))
        mask = tf.cumprod(pos_rewards, axis=-2)  # reduce along time dimension

        return tensor * mask

    @property
    @addVariableScope
    def baselineTrainOp(self):
        if self.mode == tf.contrib.learn.ModeKeys.TRAIN:
            return self.Baseline.getTrainOp()
        else:
            return tf.no_op()

###############################################################################
###########################    currently unusable    ##########################
###############################################################################

    @addVariableScope
    def sortToMinimumClassificationLoss(self, predictions, targets):
        '''Compare predictions and targets and sort the targets, such that
        the cross entropy is minimal.
        This can be done by for example by comparing their distance.

        Arguments:
            predictions: shape [(M), maxObjs, batch, N_classes]
            targets: shape [(M), maxObjs, batch, N_classes]

        Returns:
            prediction: same as input
            targets: same as before but sorted along `maxObjs` dimension

        TODO:
            For this to work it is still neccessary to also iterate over the
            batch of samples. Also the permution of objects does not cover all
            possibilities as it is done right now.
        '''

        train = len(targets.shape.as_list()) == 4
        num_obj = targets.shape.as_list()[-3]
        diff = (predictions - targets) ** 2  # [(M), mO, bs, N_cl]
        mag = tf.reduce_sum(diff, axis=-3)

        def cond(i, *args):
            return i < num_obj

        def body(i, targets, mag):
            if train:
                first_target = targets[:, 0, ...]
                targets_new = tf.stack([targets[:, 1:, ...], first_target],
                                       axis=-3)
            else:
                first_target = targets[0, ...]
                targets_new = tf.stack([targets[1:, ...], first_target],
                                       axis=-3)

            diff_new = (predictions - targets_new) ** 2
            mag_new = tf.reduces_sum(diff_new, axis=-1)

            def true_fn():
                return targets_new, mag_new

            def false_fn():
                return targets, mag

            targets, mag = tf.cond(mag_new < mag, true_fn, false_fn)

            return targets, mag

        loop_vars = [0, targets, mag]

        _, targets, _ = tf.while_loop(cond, body, loop_vars)

        return predictions, targets
