# For this script to work there must be the python script
# <nvidia-smi-scanner.py> on all servers specified in all_servers.csv.

i=1
MAX_LINES=$(( $(cat all_servers.csv | wc -l) - 1))
COMMA=','

echo "{" > free_space.json

tail -n +2 all_servers.csv |  while read -r line
do
        echo $line
        NAME=$(echo $line | awk 'BEGIN { FS=","} {print $1}' | tr -d '[:space:]')
        ADDRESS=$(echo $line | awk 'BEGIN { FS=","} {print $2}' | tr -d '[:space:]')
        REMOTE=$(echo "$NAME@$ADDRESS" | tr -d '[:space:]')
        
        NAV="cd ~/glimpsenet/glimpsenet/training/distribution;"
        SYNC="$(echo $NAV git pull)"

        VIRT="source ~/virtualenv/tf12/bin/activate;"
        START="python nvidia-smi-scanner.py"
        SCAN="$(echo $VIRT $NAV $START)"

        ssh -n $REMOTE $SYNC
        FREESPACE="$(ssh -n $REMOTE $SCAN)"
        if [ -z "$FREESPACE" ]; then
                FREESPACE="null"
        fi

        if [[ "$i" -ge "$MAX_LINES" ]]; then
                echo "last line"
                COMMA=""
        fi

        echo "\"$ADDRESS\": {\"name\": \"$NAME\", \"space\": $FREESPACE}$COMMA" >> free_space.json
        i=$(( $i + 1 ))
done

echo "}" >> free_space.json

cat free_space.json

python make_all_server_cluster_spec.py

NUM_FREE_GPUS=$(( $(cat all_servers_spec.txt | wc -l) - 5 ))
echo $NUM_FREE_GPUS free gpus
cat all_servers_spec.txt
