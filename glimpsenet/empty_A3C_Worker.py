import json
import logging
import os
import tensorflow as tf

from training.config import configure


logger = logging.getLogger(__name__)

# TF_CONFIG is not set when starting the evaluation experiment.
try:
    tf_config = os.environ['TF_CONFIG']
    tf_config = json.loads(tf_config) if tf_config else None
except KeyError:
    tf_config = None

Config = configure()

if tf_config:
    Config.setParameters({'type': tf_config['task']['type'],
                          'task': tf_config['task']['index'],
                          'cluster': tf_config['cluster']})


# Ensure that if resetting training only one new model directory is created
job = Config.getParameter('type')
task = Config.getParameter('task')
chief = task == 0 and job == 'worker'

if int(task) > 0 or job != 'worker':
    Config.setParameters({'reuse': True})

# Set up server
server = tf.train.Server(tf_config['cluster'],
                         job_name=job,
                         task_index=int(task))

# Create Session config
sess_config = tf.ConfigProto(allow_soft_placement=True)

with tf.train.MonitoredTrainingSession(
        master=server.target,
        is_chief=chief,
        config=sess_config) as mon_sess:
    print 'Starting Training'
    episode = 0
    # Training loop, written as explicit as possible, while staying as concise
    # as possible
    while not mon_sess.should_stop():

        print episode
        episode += 1
