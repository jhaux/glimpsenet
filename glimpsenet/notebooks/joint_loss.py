import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

plt.switch_backend('Agg')

e = 1e-10


def model(inputs):
    fc11 = tf.contrib.layers.fully_connected(inputs[0], 100)
    fc12 = tf.contrib.layers.fully_connected(fc11, 10,
                                             activation_fn=tf.nn.softmax)

    fc21 = tf.contrib.layers.fully_connected(inputs[1], 100)
    fc22 = tf.contrib.layers.fully_connected(fc21, 10,
                                             activation_fn=tf.nn.softmax)

    pred = tf.stack([fc12, fc22], axis=0)

    return pred


def data(bs=100, permute=False):
    while True:
        data = np.zeros([2, bs, 100])
        targs = np.zeros([2, bs, 10])
        idxs = np.random.randint(0, 10, size=[2, bs], dtype=int)

        for i in range(2):
            for j in range(data.shape[1]):
                start = idxs[i, j] * 10
                stop = idxs[i, j] * 10 + 10
                data[i, j, start:stop] = 1
                targs[i, j, idxs[i, j]] = 1
        if permute:
            targs = np.random.permutation(targs)

        yield data, targs


in_pl = tf.placeholder(tf.float32, shape=[2, None, 100])
in_pl += 0.1 * tf.random_normal(shape=[2, 100, 100])
y_pl = tf.placeholder(tf.float32, shape=[2, None, 10])

preds = model(in_pl)

sum_pred = tf.reduce_sum(preds, axis=0)
sum_pred /= tf.reduce_sum(sum_pred)
sum_targ = tf.reduce_sum(y_pl, axis=0)
sum_targ /= tf.reduce_sum(sum_targ)
loss_new = tf.reduce_sum(-tf.log(sum_pred + e) * sum_targ)

loss_reg = tf.reduce_mean((sum_pred - sum_targ)**2)

loss_old = -tf.reduce_mean(tf.reduce_sum(tf.log(preds + e) * y_pl, axis=1))

for name, loss, p in [['new', loss_new, False],
                      ['new', loss_new, True],
                      ['reg', loss_reg, False],
                      ['reg', loss_reg, True],
                      ['old', loss_old, False]]:

    max_l = tf.shape(preds)[0]
    l_pm = tf.transpose(tf.argmax(preds, axis=-1), [1, 0])
    l_tm = tf.transpose(tf.argmax(y_pl, axis=-1), [1, 0])
    print l_pm.shape
    l_p, _ = tf.nn.top_k(l_pm, max_l)
    l_t, _ = tf.nn.top_k(l_tm, max_l)

    eq = tf.equal(l_p, l_t)

    accuracy = tf.reduce_mean(tf.to_float(eq))

    accuracy = tf.Print(accuracy, [l_p], 'l_p: ', 10, 100)
    accuracy = tf.Print(accuracy, [l_t], 'l_t: ', 10, 100)
    accuracy = tf.Print(accuracy, [eq], 'eq: ', 10, 100)
    step = tf.train.get_global_step()
    lr = 1e-4
    train_op = tf.contrib.layers.optimize_loss(loss, step, lr, 'SGD')

    with tf.Session() as sess:
        i = 0
        MAX_ITER = 100000
        g = data()
        sess.run(tf.global_variables_initializer())
        print 'starting...'
        d, targs = g.next()
        acc = sess.run(accuracy, feed_dict={in_pl: d, y_pl: targs})
        print 'accuracy:', acc
        accs = []
        while i < MAX_ITER:
            d, targs = g.next()
            _, acc = sess.run([train_op, accuracy],
                              feed_dict={in_pl: d, y_pl: targs})
            i += 1
            print 'accuracy:', acc

            accs += [acc]
            if np.mean(accs[-10:]) == 1.:
                break

        perm = 'with' if p else 'without'
        plt.plot(accs, label='{} loss {} permutation'.format(name, perm))

plt.xlabel('step')
plt.ylabel('accuracy')
plt.legend()
plt.gcf().savefig('joint_loss.png')
