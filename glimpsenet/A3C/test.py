import numpy as np
import gym
import gym_multiMnist
from A3C.A3C import A3C_Worker
from util.tensor_ops import to_array


def share_grads(worker, shared_worker):
    params_iterator = zip(worker.parameters(), shared_worker.parameters())
    for param, shared_param in params_iterator:
        if shared_param.grad is not None:
            return
        shared_param._grad = param.grad


def test(rank, config, ckpt_path, shared_worker,
         counter,
         logging_queue):

    fss = config.getParameter('HyperParameters/fixedStepSize')
    step_size = config.getParameter('HyperParameters/predSteps')

    n_o = config.getParameter('HyperParameters/maxObjs')
    dif = config.getParameter('Dataset/difficulty')

    env_params = {
        'N_obj': n_o,
        'difficulty': dif,
        'sorted_targets': True,
        'fss': None if not fss else step_size,
        'mode': 'train',
        'dset': config.getParameter('Dataset/dset'),
        'imsize': config.getParameter('HyperParameters/imsize')
        }

    env_id = 'MultiMnist-{}.{}-v1'.format(n_o, dif)
    gym.envs.register(id=env_id,
                      entry_point='gym_multiMnist.envs:MultiMnistEnv',
                      kwargs=env_params)

    env = gym.make(env_id)
    worker = A3C_Worker(env, config, ckpt_path, test_worker=True)
    n_samples = 100

    n_logs = 0
    log = False
    step_old = counter.value()
    while True:
        step_new = counter.value()
        log = step_new - step_old >= 500

        # For debugging log always at first possibility
        if n_logs == 0 and logging_queue is not None:
            log = True

        if log:
            step_old = step_new
            worker.load_state_dict(shared_worker.state_dict())
            worker.env.hard_restart()

            losses = []
            accuracies = []
            logs = []
            for i in range(n_samples):
                plot = i == n_samples - 1
                loss, log = worker.test_step()
                accuracy = worker.log(test=True, step=step_new, index=i,
                                      make_plot=plot)
                losses.append(to_array(loss))
                accuracies.append(accuracy)
                logs.append(log)
            accuracy = np.mean(accuracies)
            loss = np.mean(losses)

            scalars = {'accuracy': accuracy,
                       'total_loss': loss,
                       'total_classification_loss': 0.,
                       'total_baseline_loss': 0.}

            for l in logs:
                l = l['scalars']
                scalars['total_classification_loss'] \
                    += float(l['total_classification_loss'])
                scalars['total_baseline_loss'] \
                    += float(l['total_baseline_loss'])
            scalars['total_classification_loss'] /= n_samples
            scalars['total_baseline_loss'] /= n_samples

            logs = {'scalars': scalars,
                    'step': step_new,
                    'mode': 'test'}

            logging_queue.put(logs)

            n_logs += 1
