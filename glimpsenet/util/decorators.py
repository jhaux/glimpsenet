import time
import torch.multiprocessing as mp


def time_it(logging_queue):
    def wrapper(func):
        def fun_wrapped_with_timer(self, *args, **kwargs):
            t_0 = time.time()
            output = func(self, *args, **kwargs)
            t_end = time.time()
            t_dur = t_end - t_0

            tag = self.__class__.__name__ + '-' + str(self.rank)
            tag += '-' + func.__name__
            logging_queue.put([tag, [t_0, t_end, t_dur]])

            return output
        return fun_wrapped_with_timer
    return wrapper


if __name__ == '__main__':
    import matplotlib.pyplot as plt
    import numpy as np
    q = mp.Queue()

    class TestClass(object):
        @time_it(q)
        def __init__(self, rank):
            self.rank = rank
            time.sleep(1)

        @time_it(q)
        def m1(self, t):
            time.sleep(t)

    def process(rank):
        tc = TestClass(rank)
        for i in range(10):
            tc.m1(2)

    processes = []
    for i in range(3):
        p = mp.Process(target=process, args=(i,))
        p.start()
        processes.append(p)

    results = []
    t_start = time.time()
    t_dur = 0
    while t_dur < 10:
        output = q.get()
        results.append(output)
        print(output)
        t_dur = time.time() - t_start
        print(t_dur)

    f, ax = plt.subplots(1, 1)
    Xs = {}
    Ys = {}
    for r in results:
        tag = r[0]
        name, rank, func = tag.split('-')
        t_start, t_end, t_dur = r[1]
        if tag in Xs:
            Xs[tag].append(t_start)
        else:
            Xs[tag] = [t_start]
        if tag in Ys:
            Ys[tag].append(t_dur)
        else:
            Ys[tag] = [t_dur]
    for k, v in Xs.items():
        plt.plot(v, Ys[k], label=k)

    Y_means = {}
    for k, v in Ys.items():
        name, rank, func = k.split('-')
        tag = name + '-' + func
        mean = np.mean(v)
        if tag in Y_means:
            Y_means[tag].append(mean)
        else:
            Y_means[tag] = [mean]
    for k, v in Y_means.items():
        Y_means[k] = np.mean(v)

    plt.pie(list(Y_means.values()), labels=list(Y_means.keys()))
    print(results)
    plt.show()
    for p in processes:
        p.join()
