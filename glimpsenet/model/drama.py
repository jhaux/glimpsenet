# -*- coding: utf-8 -*-

import torch
import torch.nn as nn
from torch.autograd import Variable
import logging

from .buildingBlocks import GlimpseNet, EmissionNet, ClassificationNet
from .buildingBlocks import ContextNet

logger = logging.getLogger(__name__)


class DeepRecurrentAttentionModel(nn.Module):
    '''DRAM - an Attention Network trained with REINFORCE.
    Based on "Multiple Object Recognition with visual Attention"
    by Jimmy Lei Ba et al. (https://arxiv.org/abs/1412.7755)
    '''

    def __init__(self, Config):
        '''Set up the network and create attributes to access placeholders and
        outputs.

        Arguments:
            Config: config, that holds information about all necessary
                parameters to build this model. See training.config
        '''
        super(DeepRecurrentAttentionModel, self).__init__()
        # Set Parameters
        self.Config = Config
        getP = self.Config.getParameter
        self.arc = Config.architecture
        self.n_lstm_units = getP('HyperParameters/nLstmUnits')

        self.glimpseNet = GlimpseNet(getP('GlimpseNet'))
        self.emissionNet = EmissionNet(getP('EmissionNet'))
        self.contextNet = ContextNet(getP('ContextNet'))
        self.classificationNet \
            = ClassificationNet(getP('ClassificationNet'))

        self.lstm1 = nn.LSTMCell(self.n_lstm_units, self.n_lstm_units)
        self.lstm2 = nn.LSTMCell(self.n_lstm_units, self.n_lstm_units)

        self.use_cuda = True

    def forward(self, l, ps, glimpse, state1, state2):
        '''
        Arguments:
            l: location tuple (x, y)
            ps: patch size
            glimpse: Tensor containing (batch) of glimpses
            state1 = (h1, c1): Tuple containing the state of LSTM cell 1
            state2 = (h2, c2): Tuple containing the state of LSTM cell 2

        Returns:
            l: location of the next glimpse, and samples of it
            ps: patch size of the next glimpse
            o: prediction signal
            pred: prediction
            state1, state2: lstm states (see above)
        '''

        # Observe glimpse and walk through network
        g = self.glimpseNet(glimpse, l, ps)

        h1, c1 = state1 = self.lstm1(g, state1)
        h2, c2 = state2 = self.lstm2(h1, state2)

        pred = self.classificationNet(h1)
        l, ps = self.emissionNet(c2)

        return l, ps, pred, state1, state2

    def initial_lstm_states(self, low_res, imsize):
        '''Supplies the initial lstm states.
        Arguments:
            low_res: Downsampled version of the image to explore.
        '''

        h1 = Variable(torch.zeros([1, self.n_lstm_units]))
        c1 = Variable(torch.zeros([1, self.n_lstm_units]))

        h2 = Variable(torch.zeros([1, self.n_lstm_units]))
        c2 = self.contextNet(low_res, imsize)

        if self.use_cuda:
            h1 = h1.cuda()
            c1 = c1.cuda()
            h2 = h2.cuda()
            c2 = c2.cuda()

        return (h1, c1), (h2, c2)

    def initial_action(self, c2):
        return self.emissionNet(c2)
