'''Unit test for custom_metrics.py'''

import custom_metrics
import unittest
import numpy as np
import tensorflow as tf


class PredictionAccuracyFnTestCase(unittest.TestCase):
    middle = 6
    bs = 2 * middle
    C = 10

    maxObjs = 1
    empty_single = np.zeros([maxObjs, bs, C])
    single_oneHot_0 = np.copy(empty_single)
    single_oneHot_0[..., 0] = 1
    single_oneHot_1 = np.copy(empty_single)
    single_oneHot_1[..., 1] = 1

    single_oneHot_0and1 = np.copy(empty_single)
    single_oneHot_0and1[...,:middle,0] = 1
    single_oneHot_0and1[...,middle:,1] = 1

    label_single_oneHot_0 = np.zeros([bs, maxObjs, C])
    label_single_oneHot_0[...,0] = 1

    maxObjs = 3
    empty_multi = np.zeros([maxObjs, bs, C])
    multi_oneHot_0 = np.copy(empty_multi)
    multi_oneHot_0[..., 0] = 1
    multi_oneHot_1 = np.copy(empty_multi)
    multi_oneHot_1[..., 1] = 1

    multi_oneHot_0and1 = np.copy(empty_multi)
    multi_oneHot_0and1[...,:middle,0] = 1
    multi_oneHot_0and1[...,middle:,1] = 1

    label_multi_oneHot_0 = np.zeros([bs, maxObjs, C])
    label_multi_oneHot_0[...,0] = 1

    # (prediction, label) -> accuracy
    single = [((tf.constant(single_oneHot_0), tf.constant(label_single_oneHot_0)), 1),
        ((tf.constant(single_oneHot_1), tf.constant(label_single_oneHot_0)), 0), 
        ((tf.constant(single_oneHot_0and1), tf.constant(label_single_oneHot_0)), 0.5)]
    multi = [((tf.constant(multi_oneHot_0), tf.constant(label_multi_oneHot_0)), 1), 
        ((tf.constant(multi_oneHot_1), tf.constant(label_multi_oneHot_0)), 0),
        ((tf.constant(multi_oneHot_0and1), tf.constant(label_multi_oneHot_0)), 0.5)]
    
    rank_2 = tf.constant(np.zeros([50, 10]))
    rank_3 = tf.constant(np.zeros([1, 50, 10]))
    rank_4 = tf.constant(np.zeros([2, 1, 50, 10]))
    rank_5 = tf.constant(np.zeros([30, 2, 1, 50, 10]))

    wrong_shapes = [
            (rank_4, rank_2),
            (rank_4, rank_4),
            (rank_4, rank_5),
            (rank_5, rank_3),
            (rank_2, rank_2),
            ]

    def test_single_object_input(self):
        '''One object'''
        for (prediction, label), expected_accuracy in self.single:
            acc = custom_metrics.prediction_accuracy_fn(prediction, label)
            with tf.Session() as sess:
                calculated_accuracy = sess.run(acc)
            self.assertEqual(
                    calculated_accuracy, 
                    expected_accuracy, 
                    'incorrect ouput with correct inputs.')

    def test_multi_objects_input(self):
        '''Multiple objects'''
        for (prediction, label), expected_accuracy in self.multi:
            acc = custom_metrics.prediction_accuracy_fn(prediction, label)
            with tf.Session() as sess:
                calculated_accuracy = sess.run(acc)
            self.assertEqual(calculated_accuracy, 
                    expected_accuracy,
                    'incorrect ouput with correct inputs.')

    def test_wrong_input_shape(self):
        '''Test rank assertion.'''
        fn = custom_metrics.prediction_accuracy_fn
        for pred, label in self.wrong_shapes:
            with self.assertRaises(ValueError) as cm:
                custom_metrics.prediction_accuracy_fn(pred, label)


if __name__ == '__main__':
    unittest.main()
