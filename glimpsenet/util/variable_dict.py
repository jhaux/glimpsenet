restore_vars_dict = {
    'GlimpseNet/Image_Net/conv_1/biases':
    'Generative_Model/GlimpseNet/Image_Net/conv_1/biases',
    'GlimpseNet/Image_Net/conv_1/weights':
    'Generative_Model/GlimpseNet/Image_Net/conv_1/weights',
    'GlimpseNet/Image_Net/conv_2/biases':
    'Generative_Model/GlimpseNet/Image_Net/conv_2/biases',
    'GlimpseNet/Image_Net/conv_2/weights':
    'Generative_Model/GlimpseNet/Image_Net/conv_2/weights',
    'GlimpseNet/Image_Net/conv_3/biases':
    'Generative_Model/GlimpseNet/Image_Net/conv_3/biases',
    'GlimpseNet/Image_Net/conv_3/weights':
    'Generative_Model/GlimpseNet/Image_Net/conv_3/weights',
    'GlimpseNet/Image_Net/fc/biases':
    'Generative_Model/GlimpseNet/Image_Net/fc/biases',
    'GlimpseNet/Image_Net/fc/weights':
    'Generative_Model/GlimpseNet/Image_Net/fc/weights',
    'GlimpseNet/Location_Net/fc/biases':
    'Generative_Model/GlimpseNet/Location_Net/fc/biases',
    'GlimpseNet/Location_Net/fc/weights':
    'Generative_Model/GlimpseNet/Location_Net/fc/weights',
    'OptimizeLoss/GlimpseNet/Image_Net/conv_1/biases/Adam':
    'OptimizeLoss/Generative_Model/GlimpseNet/Image_Net/conv_1/biases/Adam',
    'OptimizeLoss/GlimpseNet/Image_Net/conv_1/biases/Adam_1':
    'OptimizeLoss/Generative_Model/GlimpseNet/Image_Net/conv_1/biases/Adam_1',
    'OptimizeLoss/GlimpseNet/Image_Net/conv_1/weights/Adam':
    'OptimizeLoss/Generative_Model/GlimpseNet/Image_Net/conv_1/weights/Adam',
    'OptimizeLoss/GlimpseNet/Image_Net/conv_1/weights/Adam_1':
    'OptimizeLoss/Generative_Model/GlimpseNet/Image_Net/conv_1/weights/Adam_1',
    'OptimizeLoss/GlimpseNet/Image_Net/conv_2/biases/Adam':
    'OptimizeLoss/Generative_Model/GlimpseNet/Image_Net/conv_2/biases/Adam',
    'OptimizeLoss/GlimpseNet/Image_Net/conv_2/biases/Adam_1':
    'OptimizeLoss/Generative_Model/GlimpseNet/Image_Net/conv_2/biases/Adam_1',
    'OptimizeLoss/GlimpseNet/Image_Net/conv_2/weights/Adam':
    'OptimizeLoss/Generative_Model/GlimpseNet/Image_Net/conv_2/weights/Adam',
    'OptimizeLoss/GlimpseNet/Image_Net/conv_2/weights/Adam_1':
    'OptimizeLoss/Generative_Model/GlimpseNet/Image_Net/conv_2/weights/Adam_1',
    'OptimizeLoss/GlimpseNet/Image_Net/conv_3/biases/Adam':
    'OptimizeLoss/Generative_Model/GlimpseNet/Image_Net/conv_3/biases/Adam',
    'OptimizeLoss/GlimpseNet/Image_Net/conv_3/biases/Adam_1':
    'OptimizeLoss/Generative_Model/GlimpseNet/Image_Net/conv_3/biases/Adam_1',
    'OptimizeLoss/GlimpseNet/Image_Net/conv_3/weights/Adam':
    'OptimizeLoss/Generative_Model/GlimpseNet/Image_Net/conv_3/weights/Adam',
    'OptimizeLoss/GlimpseNet/Image_Net/conv_3/weights/Adam_1':
    'OptimizeLoss/Generative_Model/GlimpseNet/Image_Net/conv_3/weights/Adam_1',
    'OptimizeLoss/GlimpseNet/Image_Net/fc/biases/Adam':
    'OptimizeLoss/Generative_Model/GlimpseNet/Image_Net/fc/biases/Adam',
    'OptimizeLoss/GlimpseNet/Image_Net/fc/biases/Adam_1':
    'OptimizeLoss/Generative_Model/GlimpseNet/Image_Net/fc/biases/Adam_1',
    'OptimizeLoss/GlimpseNet/Image_Net/fc/weights/Adam':
    'OptimizeLoss/Generative_Model/GlimpseNet/Image_Net/fc/weights/Adam',
    'OptimizeLoss/GlimpseNet/Image_Net/fc/weights/Adam_1':
    'OptimizeLoss/Generative_Model/GlimpseNet/Image_Net/fc/weights/Adam_1',
    'OptimizeLoss/GlimpseNet/Location_Net/fc/biases/Adam':
    'OptimizeLoss/Generative_Model/GlimpseNet/Location_Net/fc/biases/Adam',
    'OptimizeLoss/GlimpseNet/Location_Net/fc/biases/Adam_1':
    'OptimizeLoss/Generative_Model/GlimpseNet/Location_Net/fc/biases/Adam_1',
    'OptimizeLoss/GlimpseNet/Location_Net/fc/weights/Adam':
    'OptimizeLoss/Generative_Model/GlimpseNet/Location_Net/fc/weights/Adam',
    'OptimizeLoss/GlimpseNet/Location_Net/fc/weights/Adam_1':
    'OptimizeLoss/Generative_Model/GlimpseNet/Location_Net/fc/weights/Adam_1'
    }

correct_names = [
    # 'Baseline/fc1/biases',
    # 'Baseline/fc1/biases/Adam',
    # 'Baseline/fc1/biases/Adam_1',
    # 'Baseline/fc1/weights',
    # 'Baseline/fc1/weights/Adam',
    # 'Baseline/fc1/weights/Adam_1',
    # 'Baseline/fc2/biases',
    # 'Baseline/fc2/biases/Adam',
    # 'Baseline/fc2/biases/Adam_1',
    # 'Baseline/fc2/weights',
    # 'Baseline/fc2/weights/Adam',
    # 'Baseline/fc2/weights/Adam_1',
    'Generative_Model/ClassificationNet/fc_1/biases',
    'Generative_Model/ClassificationNet/fc_1/weights',
    'Generative_Model/ClassificationNet/fc_2/biases',
    'Generative_Model/ClassificationNet/fc_2/weights',
    'Generative_Model/ContextNet/conv1/biases',
    'Generative_Model/ContextNet/conv1/weights',
    'Generative_Model/ContextNet/conv2/biases',
    'Generative_Model/ContextNet/conv2/weights',
    'Generative_Model/ContextNet/conv3/biases',
    'Generative_Model/ContextNet/conv3/weights',
    'Generative_Model/ContextNet/fc1/biases',
    'Generative_Model/ContextNet/fc1/weights',
    'Generative_Model/EmissionNet/fc1/biases',
    'Generative_Model/EmissionNet/fc1/weights',
    'Generative_Model/LSTM1/lstm_cell/biases',
    'Generative_Model/LSTM1/lstm_cell/weights',
    'Generative_Model/LSTM2/lstm_cell/biases',
    'Generative_Model/LSTM2/lstm_cell/weights',
    'OptimizeLoss/Generative_Model/ClassificationNet/fc_1/biases/Adam',
    'OptimizeLoss/Generative_Model/ClassificationNet/fc_1/biases/Adam_1',
    'OptimizeLoss/Generative_Model/ClassificationNet/fc_1/weights/Adam',
    'OptimizeLoss/Generative_Model/ClassificationNet/fc_1/weights/Adam_1',
    'OptimizeLoss/Generative_Model/ClassificationNet/fc_2/biases/Adam',
    'OptimizeLoss/Generative_Model/ClassificationNet/fc_2/biases/Adam_1',
    'OptimizeLoss/Generative_Model/ClassificationNet/fc_2/weights/Adam',
    'OptimizeLoss/Generative_Model/ClassificationNet/fc_2/weights/Adam_1',
    'OptimizeLoss/Generative_Model/ContextNet/conv1/biases/Adam',
    'OptimizeLoss/Generative_Model/ContextNet/conv1/biases/Adam_1',
    'OptimizeLoss/Generative_Model/ContextNet/conv1/weights/Adam',
    'OptimizeLoss/Generative_Model/ContextNet/conv1/weights/Adam_1',
    'OptimizeLoss/Generative_Model/ContextNet/conv2/biases/Adam',
    'OptimizeLoss/Generative_Model/ContextNet/conv2/biases/Adam_1',
    'OptimizeLoss/Generative_Model/ContextNet/conv2/weights/Adam',
    'OptimizeLoss/Generative_Model/ContextNet/conv2/weights/Adam_1',
    'OptimizeLoss/Generative_Model/ContextNet/conv3/biases/Adam',
    'OptimizeLoss/Generative_Model/ContextNet/conv3/biases/Adam_1',
    'OptimizeLoss/Generative_Model/ContextNet/conv3/weights/Adam',
    'OptimizeLoss/Generative_Model/ContextNet/conv3/weights/Adam_1',
    'OptimizeLoss/Generative_Model/ContextNet/fc1/biases/Adam',
    'OptimizeLoss/Generative_Model/ContextNet/fc1/biases/Adam_1',
    'OptimizeLoss/Generative_Model/ContextNet/fc1/weights/Adam',
    'OptimizeLoss/Generative_Model/ContextNet/fc1/weights/Adam_1',
    'OptimizeLoss/Generative_Model/EmissionNet/fc1/biases/Adam',
    'OptimizeLoss/Generative_Model/EmissionNet/fc1/biases/Adam_1',
    'OptimizeLoss/Generative_Model/EmissionNet/fc1/weights/Adam',
    'OptimizeLoss/Generative_Model/EmissionNet/fc1/weights/Adam_1',
    'OptimizeLoss/Generative_Model/LSTM1/lstm_cell/biases/Adam',
    'OptimizeLoss/Generative_Model/LSTM1/lstm_cell/biases/Adam_1',
    'OptimizeLoss/Generative_Model/LSTM1/lstm_cell/weights/Adam',
    'OptimizeLoss/Generative_Model/LSTM1/lstm_cell/weights/Adam_1',
    'OptimizeLoss/Generative_Model/LSTM2/lstm_cell/biases/Adam',
    'OptimizeLoss/Generative_Model/LSTM2/lstm_cell/biases/Adam_1',
    'OptimizeLoss/Generative_Model/LSTM2/lstm_cell/weights/Adam',
    'OptimizeLoss/Generative_Model/LSTM2/lstm_cell/weights/Adam_1',
    'OptimizeLoss/beta1_power',
    'OptimizeLoss/beta2_power',
    'beta1_power',
    'beta2_power'
]

for name in correct_names:
    restore_vars_dict[name] = name

global_init_vars = [
    'Baseline/fc1/biases',
    'Baseline/fc1/biases/Adam',
    'Baseline/fc1/biases/Adam_1',
    'Baseline/fc1/weights',
    'Baseline/fc1/weights/Adam',
    'Baseline/fc1/weights/Adam_1',
    'Baseline/fc2/biases',
    'Baseline/fc2/biases/Adam',
    'Baseline/fc2/biases/Adam_1',
    'Baseline/fc2/weights',
    'Baseline/fc2/weights/Adam',
    'Baseline/fc2/weights/Adam_1',
    'global_step'
]
