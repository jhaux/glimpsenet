from config import GlimpseNetConfig
import os
import pickle


def config1():
    '''Larger LSTM units!'''

    config1 = GlimpseNetConfig(dset='M')
    update1 = {'featureDim': 128,
               'predSteps': 6,
               'numLocations': 1,
               'maxObjs': 1}
    config1.setArchitectureParameters(update1)
    config1.updateDependentParameters()

    pickle.dump(config1, open('customConfigs/largeLSTM.p', 'wb'))


def config2():
    '''Larger LSTM units!
    skip layers, that are more easily trained'''

    config2 = GlimpseNetConfig(dset='M')
    update2 = {'featureDim': 128,
               'GlimpseNet/train': False,
               'EmissionNet/train': False,
               'ClassificationNet/train': False,
               'predSteps': 6,
               'numLocations': 1,
               'maxObjs': 1}
    config2.setArchitectureParameters(update2)
    config2.updateDependentParameters()

    pickle.dump(config2, open('customConfigs/largeLSTM_skipLayers.p', 'wb'))


def config3():
    '''Make coordinates centered'''

    pass


if __name__ == '__main__':
    try:
        os.mkdir('customConfigs')
    except OSError:
        print 'Directory already created'

    config1()
    config2()
