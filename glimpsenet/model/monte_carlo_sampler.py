import tensorflow as tf
import numpy as np
import logging

logger = logging.getLogger(__name__)


class MonteCarloSampler(object):
    '''Take a trajectory and sample around it in a gaussian manner'''

    def __init__(self, Config):
        '''
        Arguments:
            Config: DRAM-Config instance containing the sampling parameters.
        '''
        getP = Config.getParameter
        self.sigma_em = tf.to_float(getP('HyperParameters/sigma_em'))
        self.sigma_sig = tf.to_float(getP('HyperParameters/sigma_sig'))
        self.M = getP('HyperParameters/M')

    def __call__(self, episode):
        '''Take one trajectory of locations and sample around them in a
        gaussian way

        Arguments:
            emission: List or Tensor of shape [timesteps, batch, emission_size]
            M: Number of samples to be drawn

        Returns:
            sampledEmissions: List of new trajectories of shape
                                 [M, timesteps, batch, 2]
        '''
        emission, signal = episode

        with tf.variable_scope('Sampling_em'):
            samples_em = self.sample(emission, self.sigma_em)
            # Clip patch sizes
            axis = len(samples_em.shape.as_list())-1
            l, ps = tf.split(samples_em, [2, 1], axis)
            ps = tf.clip_by_value(ps, 0, 10e10)
            samples_em = tf.concat([l, ps], axis=-1)

        with tf.variable_scope('Sampling_sig'):
            samples_sig = self.sample(signal, self.sigma_sig)
            # Clip to values greater zero
            samples_sig = tf.clip_by_value(samples_sig, 0, 1)

        return samples_em, samples_sig

    def sample(self, mean, sigma):
        M = self.M

        sample_shape = tf.concat([[M], tf.shape(mean)], axis=0)
        samples = mean + sigma * tf.random_normal(sample_shape)

        return samples

    def getLogProbablities(self, old_episode, new_episodes):
        mean_em, mean_sig = old_episode
        emissions, signals = new_episodes

        logP_em = self.logProbs(emissions, mean_em, self.sigma_em)
        logP_sig = self.logProbs(signals, mean_sig, self.sigma_sig)

        return logP_em, logP_sig

    def logProbs(self, samples, mean, sigma):
        '''Returns:
            Log Probabilities of the sampled trajectories.
        '''

        with tf.variable_scope('Normal_log_pdf'):
            samples = tf.stop_gradient(samples)
            log_pdf = -(samples - mean)**2 / (2*sigma ** 2) \
                - tf.log(tf.sqrt(2*np.pi)*sigma)
            log_pl = tf.reduce_sum(log_pdf, axis=-1)

        return log_pl
