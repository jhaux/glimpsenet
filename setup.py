from setuptools import setup

setup(name='glimpsenet',
      version='0.5.1',
      install_requires=['numpy',
                        'torch',
                        'torchvision',
                        'matplotlib'])
