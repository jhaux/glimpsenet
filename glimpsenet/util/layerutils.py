import torch
import torch.nn.functional as func


# Helper functions
# def initializer(name):
#     if name == "zeros":
#         return init_ops.zeros_initializer()
#     elif name == 'xavier':
#         return tf.contrib.layers.xavier_initializer()
#     elif name == 'xavier_conv':
#         return tf.contrib.layers.xavier_initializer_conv2d()
#     else:
#         return None


# def regularizer(reg):
#     name, scale = reg
#     if name == 'L1':
#         return tf.contrib.layers.l1_regularizer(scale)
#     elif name == 'L2':
#         return tf.contrib.layers.l2_regularizer(scale)
#     else:
#         return None


class LinearAct(torch.nn.Module):

    def __init__(self):
        super(LinearAct, self).__init__()

    def forward(self, x):
        return x


def lin(x):
    return x


def activation(name):
    if name is None:
        return lin
    else:
        return getattr(func, name)
