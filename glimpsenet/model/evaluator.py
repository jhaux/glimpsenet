# -*- coding: utf-8 -*-

import tensorflow as tf
from model.monte_carlo_sampler import MonteCarloSampler
from util.decorators import addVariableScope
from util.reinforcement_utils import time_neg_reward


def get_tensor_arrays(names, size=1, dynamic=True, dtype=tf.float32):
    tas = []
    for name in names:
        if isinstance(name, list):
            name, dtype = name
        else:
            dtype = tf.float32
        ta = tf.TensorArray(dtype=dtype,
                            size=size,
                            dynamic_size=dynamic,
                            infer_shape=False,
                            clear_after_read=True,
                            tensor_array_name=name)
        tas.append(ta)

    return tas


class Evaluator(object):
    '''Takes care of recording episodes and evaluating those for all images in
    one batch using the DRAMA network'''

    def __init__(self, DRAMA, Baseline, Config, mode):
        '''Arguments:
            DRAMA: Deep Rec...
            Baseline: Baseline calculation and loss
            Config: Containing all Parameters
            mode: Distinguish between training and evaluation
        '''

        self.DRAMA = DRAMA
        self.Baseline = Baseline
        self.Config = Config
        self.mode = mode

        self.Sampler = MonteCarloSampler(Config)

    @addVariableScope
    def record_epsiode(self, image, low_res):
        '''Record original episode based purely on decisions done by DRAMA.

        Arguments:
            images: a single image in full resolution , shape [1, x, y, ch]
            low_res: a single image in low resolution , shape [1, x_l, y_l, ch]

        Returns:
            episode: list of Tensors: [em, signal] (see status report kw28)
                Each have a shape of [ts, 1, {3, 2}]
            pred: Predictions signaled during episode
            pred_seq: Predictions at each time step
        '''

        if self.mode == tf.contrib.learn.ModeKeys.TRAIN:
            pred, pred_ts, pred_seq, em, signal, _, _ \
                = self.DRAMA.forward(image, low_res)

            episode = (em, signal)

            return episode, pred, pred_ts, pred_seq

        else:
            pred, pred_ts, pred_seq, em, signal, c2_seq, \
                sig_single_seq, glimpse_seq, entropy_seq \
                = self.DRAMA.forward(image, low_res)

            episode = (em, signal)

            return episode, pred, pred_ts, pred_seq, c2_seq, \
                sig_single_seq, glimpse_seq, entropy_seq

    @addVariableScope
    def sample_episodes(self, episode):
        '''Generates new episodes given an original one.

        Arguments:
            episode: list of tensors: [em, signal]
                Each have a shape of [ts, 1, {3, 2}]

        Returns:
            new_episodes: list of tensors: [L, P, O, E]
                Each have a shape of [ts, 1, {3, 2}]
            log_probs: log probabilities of the episode tensors
        '''

        new_episodes = self.Sampler(episode)
        log_probs = self.Sampler.getLogProbablities(episode, new_episodes)

        return new_episodes, log_probs

    @addVariableScope
    def evaluate_episodes(self, image, low_res, episodes, logP_E, target):
        '''Get the predictions of the given episodes.

        Arguments:
            image: Image to label
            low_res: low resoultion version of image for context net
            episodes: list of tensors: [emissions, signals]
                Each have a shape of [M, ts, 1, {3, 2}]
            logP_E: log probs of theses episodes
                Each have a shape of [M, ts, 1]
            target: Ground truth label for this set of episodes,
                to be compared with the predictions.

        Returns:
            predictions: classifications over time. Shape [M, ts, 1, N_cl]
            L_re: reinforcement loss
        '''
        emissions, signals = episodes
        logP_em, logP_sig = logP_E

        N_examples = tf.shape(emissions)[0]

        def cond(idx, *args):
            return idx < N_examples

        def body(idx, L_cum, L_bl, summaries):
            results = self.DRAMA.eval(image,
                                      low_res,
                                      emissions[idx],
                                      signals[idx],
                                      'Evaluate_episode')

            if self.mode == tf.contrib.learn.ModeKeys.TRAIN:
                pred, pred_ts, pred_seq, em_seq, sig_seq, c2_seq, entropy_seq \
                        = results
            else:
                pred, pred_ts, pred_seq, em_seq, sig_seq, c2_seq, \
                        sig_single_seq, glimpse_seq, entropy_seq = results

            b = self.Baseline.mapTrajectory(c2_seq)

            # Get the numbers
            N_ts = tf.shape(pred_seq)[0]
            N_pred = tf.shape(pred)[0]
            N_targ = tf.shape(target)[0]

            predictions = tf.squeeze(pred, axis=-2)
            pred_ts.set_shape([None, 1])

            pred_sum = tf.reduce_sum(predictions, axis=0)  # [N_cl]
            targ_sum = tf.reduce_sum(target, axis=0)  # [N_cl]

            accuracy = self.unordered_accuracy(predictions, target)

            rewards = self.rewards(N_pred, pred_ts, N_targ,
                                   N_ts, entropy_seq, accuracy)

            returns = self.returns(rewards)

            delta_td = self.td_loss(returns, b)
            delta_td = tf.expand_dims(delta_td, -1)
            delta_td.set_shape([None, 1])

            logP_e = (logP_em[idx], logP_sig[idx])
            L_re = self.reinforce_loss(logP_e, delta_td, b)
            L_cl = self.classification_loss(pred_sum, targ_sum)

            L_cum = L_re + L_cl
            L_bl += self.Baseline.getLoss(returns, b)

            idx += 1

            l_seq, p_seq = em_seq[..., :2], em_seq[..., 2]
            o_seq, e_seq = sig_seq[..., 0], sig_seq[..., 1]

            summaries[0] = summaries[0] + tf.reduce_mean(l_seq)
            summaries[1] = summaries[1] + tf.reduce_mean(p_seq)
            summaries[2] = summaries[2] + tf.reduce_mean(o_seq)
            summaries[3] = summaries[3] + tf.reduce_mean(e_seq)
            summaries[4] = summaries[4] + tf.reduce_mean(returns)
            summaries[5] = summaries[5] + tf.reduce_mean(b)
            summaries[6] = summaries[6] + tf.to_float(N_ts)
            summaries[7] = summaries[7] + accuracy

            return idx, L_cum, L_bl, summaries

        loop_vars = [0, 0., 0., [0.]*8]

        _, L_cum, L_bl, summaries = tf.while_loop(
                cond,
                body,
                loop_vars,
                name='while_over_episodes')

        L_cum /= tf.to_float(N_examples)
        L_bl /= tf.to_float(N_examples)

        for i, s in enumerate(summaries):
            summaries[i] = s / tf.to_float(N_examples)

        # TODO add baselines and trajectory histogram summary somehow

        return L_cum, L_bl, summaries

    @addVariableScope
    def unordered_accuracy(self, prediction, target):
        '''Calculates the accuracy of the prediction given the target, taking
        into account the number of predctions made with respect to how many
        should be made and does not care for order of predictions.

        Arguments:
            prediction: [N_pred, N_cl]
            target: [N_targ, N_cl]

        Returns:
            accuracy: scalar value
        '''

        labels_p = tf.argmax(prediction, axis=-1)
        labels_t = tf.argmax(target, axis=-1)

        N_p = tf.shape(labels_p)[0]
        N_t = tf.shape(labels_t)[0]
        N_cl = tf.shape(prediction)[-1]

        # Number of possible comparable predictions and reference
        # min(N_p, N_t), max(N_p, N_t)
        max_pred, ref_pred = tf.cond(
                N_p < N_t,
                lambda: (tf.to_float(N_p), tf.to_float(N_t)),
                lambda: (tf.to_float(N_t), tf.to_float(N_p)))

        # Convert to one hot encoding and sum up
        pred_onehot = tf.one_hot(tf.cast(labels_p, tf.int32), N_cl)
        pred_onehot = tf.reduce_sum(pred_onehot, axis=0)
        targ_onehot = tf.one_hot(tf.cast(labels_t, tf.int32), N_cl)
        targ_onehot = tf.reduce_sum(targ_onehot, axis=0)

        # Number of correct predictions -> See unordere accuracy notebook
        diff = pred_onehot - targ_onehot
        zeros = tf.zeros_like(diff)
        N_wrong_predictions = tf.reduce_sum(tf.abs(tf.where(pred_onehot > 0,
                                                            diff,
                                                            zeros)))
        N_correct_predictions = tf.to_float(N_p) - N_wrong_predictions

        # Prediction accuracy: Fraction of correct over possible predictions
        acc_pred = N_correct_predictions / max_pred

        # Correct number of predictions accuracy: fraction of min and max
        # of (N_t, N_p)
        acc_N = max_pred / ref_pred

        # The overall accuracy is the product of both acc_pred and acc_N
        accuracy = acc_pred * acc_N

        # Should no predictions have been made the value of accuracy is nan
        # It should in this case be 0. though
        accuracy = tf.cond(tf.is_finite(accuracy),
                           lambda: accuracy,
                           lambda: 0.)

        return accuracy

    @addVariableScope
    def classification_loss(self, pred_sum, targ_sum):
        '''Calculates the softmax cross entropy for the sum of all predictions
        given the sum of all targets.

        Arguments:
            pred_sum: All signaled predictions summed up. Shape [N_cl]
            targets: Correct labels in correct order. Shape [N_cl]

        Returns:
            loss: scalar softmax cross entropy
        '''
        loss = -tf.reduce_mean(tf.log(pred_sum) * targ_sum)

        # If no predictions are made the loss is nan, but should be 0.
        # (No gradient is propagated in the latter case)
        loss = tf.cond(tf.is_finite(loss), lambda: loss, lambda: 0.)

        return loss

    @addVariableScope
    def rewards(self, N_pred, p_ts, N_targ, N_ts, entropy_seq, accuracy):
        '''Calculates the rewards sequence for one episode.
        Arguments:
            N_pred: Number of predictions
            p_ts: Associated timesteps of predictions. Shape [N_pred, 1]
            N_targ: Number of targets
            N_ts: Number of timesteps for this episode. Scalar
            entropy_seq: Sequence of entropies of the viewd glimpses. Large for
                complexe glimpses
            accuracy: scalar, used as reward at each prediction time step

        Returns:
            rewards: Tensor of length of time sequence with reward for each
            time step. Shape [N_ts]
        '''

        # Make value array of episode length -> see corresponding notebook
        # Rewards are given, whenever a prediction was made for the
        # first N_targ predictions, with the value of the accuracy of all
        # predictions given the targets, as the time at which the prediction
        # was made is not known
        max_rewards = tf.cond(N_pred < N_targ,
                              lambda: N_pred,
                              lambda: N_targ)
        indeces = tf.cast(p_ts[:max_rewards], tf.int32)
        shape = [N_ts]
        vals = tf.to_float(tf.tile([accuracy], [max_rewards]))
        rewards = tf.sparse_to_dense(sparse_indices=indeces,
                                     output_shape=shape,
                                     sparse_values=vals,
                                     default_value=0.)

        # Using the entropy each timestep gets a reward for the complexity of
        # viewed glimpse, directly proportional to its entropy
        rewards += tf.squeeze(entropy_seq)

        # rewards scale with 1./T to penalize duration
        time_penalty = self.Config.getParameter('HyperParameters/timePenalty')
        time_reward = time_neg_reward(N_ts)
        rewards = time_penalty * time_reward + rewards
        rewards.set_shape([None])

        return rewards

    def td_loss(self, returns, value_estimate):
        '''Given the rewards and the expected values calculates the TD error
        used to calcualte the Value function update and the policy update'''

        V_ts = tf.squeeze(value_estimate, 1)
        V_tsplus1 = V_ts[1:]
        V_tsplus1 = tf.concat([V_tsplus1, [V_ts[-1]]], axis=-1)

        discount = self.Config.getParameter('HyperParameters/discountFactor')

        delta_td = returns + discount * V_tsplus1 - V_ts

        return tf.stop_gradient(delta_td)

    @addVariableScope
    def returns(self, rewards):
        '''Given the accuracy of the predictions and when these happend, as
        well as how many happened, calculate the value of each timestep.

        Arguments:
            rewards: ...
            discount: ...

        Returns:
            returns: Tensor of length of time sequence with value for each time
                step. Shape [N_ts, 1]'''
        N_ts = tf.shape(rewards)[0]

        # The value at each timestep is a weighted sum of all future rewards
        gamma = self.Config.getParameter('HyperParameters/discountFactor')

        def cond(i, *args):
            return i < N_ts

        def body(i, values):
            start = 0.
            limit = tf.to_float(N_ts - i)
            discounts = gamma ** tf.range(start, limit, dtype=tf.float32)
            discounted_rewards = rewards[i:] * discounts
            value = tf.reduce_sum(discounted_rewards)
            values = values.write(i, value)
            i += 1

            return i, values

        returns = tf.TensorArray(tf.float32, size=1, dynamic_size=True)
        _, returns = tf.while_loop(cond, body, [0, returns])
        returns = returns.stack()

        # returns = tf.Print(returns, [predictions], 'predictions: ',
        #                              first_n=1)
        # returns = tf.Print(returns, [preds], 'preds: ', first_n=1)
        # returns = tf.Print(returns, [targs], 'targs: ', first_n=1)
        # returns = tf.Print(returns, [correct], 'correct: ', first_n=1)
        # returns = tf.Print(returns, [cnop], 'cnop: ', first_n=1)
        # returns = tf.Print(returns, [rewards], 'rewards: ', first_n=1)
        # returns = tf.Print(returns, [returns], 'returns: ', first_n=1)

        return returns

    @addVariableScope
    def reinforce_loss(self, log_probs, values, baselines):
        '''Loss calcualte to resemble reinforce gradient update when derived
        wrt. model parameters.'''

        logP_em, logP_sig = log_probs
        l = self.Config.getParameter('HyperParameters/l')

        scale = - tf.stop_gradient(l * (values - baselines))

        with tf.variable_scope('Emission_Loss'):
            L_em = tf.reduce_mean(scale * logP_em)

        with tf.variable_scope('Signal_Loss'):
            L_sig = tf.reduce_mean(scale * logP_sig)

        loss = L_em + L_sig
        # loss = tf.Print(loss, [L_em], 'L_em: ')
        # loss = tf.Print(loss, [L_sig], 'L_sig: ')
        # loss = tf.Print(loss, [values], 'values: ')
        # loss = tf.Print(loss, [baselines], 'baselines: ')
        # loss = tf.Print(loss, [scale], 'scale: ')
        return loss

    @addVariableScope
    def evaluate_batch(self, features, targets):
        '''Given a batch of images run an evaluation on these.

        Arguments:
            features: batch of images in high and low resolution.
                Shape [bs, dim_x, dim_y, channels]
            targets: labels for the images, shape [N_obj, bs, N_cl]

        Returns:
            L_cum: Cumulative loss over all images and episode to be minimized
            L_b: Baseline regression loss
            pred: predictions signaled when recording the first episode
            pred_seq: predictions at each time step when recording the first ep
        '''

        images = features['input_original']
        downsampled = features['input_coarse']
        N_img = tf.shape(features['input_original'])[0]

        def cond(idx, *args):
            cond = idx < N_img
            return cond

        def body(idx, L_cum, L_b, summaries, eval_tas):
            image = images[idx]
            low_res = downsampled[idx]
            target = targets[:, idx, :]  # [N_obj, N_cl]

            # Add artifical batch dimension of 1
            image = tf.expand_dims(image, 0)
            low_res = tf.expand_dims(low_res, 0)

            # Epsiodes
            if self.mode == tf.contrib.learn.ModeKeys.TRAIN:
                e, pred, pred_ts, p_seq = self.record_epsiode(image, low_res)
                E, logP_E = self.sample_episodes(e)

                # Accumulate Losses over batch
                l_cum, l_b, sum_vals = self.evaluate_episodes(image,
                                                              low_res,
                                                              E,
                                                              logP_E,
                                                              target)

                for i, val in enumerate(sum_vals):
                    summaries[i] = summaries[i] + val

            else:
                # Store a lot of information to make plots
                (em_seq, sig_seq), pred, pred_ts, p_seq, c2_seq, \
                    sig_single_seq, glimpse_seq, entropy_seq \
                    = self.record_epsiode(image, low_res)

                # Get the numbers
                N_ts = tf.shape(p_seq)[0]
                N_pred = tf.shape(pred)[0]
                N_targ = tf.shape(target)[0]

                predictions = tf.squeeze(pred, axis=-2)
                pred_ts.set_shape([None, 1])

                acc = accuracy = self.unordered_accuracy(predictions, target)

                rewards = self.rewards(N_pred, pred_ts, N_targ,
                                       N_ts, entropy_seq, accuracy)

                returns = self.returns(rewards)

                with tf.variable_scope('evaluate_episodes'):
                    # Specify this variable scope, st weights are found
                    b = self.Baseline.mapTrajectory(c2_seq)

                delta_td = self.td_loss(returns, b)
                delta_td = tf.expand_dims(delta_td, -1)
                delta_td.set_shape([None, 1])

                # delta_td = tf.Print(delta_td, [tf.argmax(pred, axis=-1)],
                #                     'pred: ', first_n=1)
                # delta_td = tf.Print(delta_td, [pred_ts], 'pred_ts: ',
                #                     first_n=1)
                # delta_td = tf.Print(delta_td, [tf.argmax(target, axis=-1)],
                #                     'target: ', first_n=1)
                # delta_td = tf.Print(delta_td, [N_ts], 'N_ts: ', first_n=1)
                # delta_td = tf.Print(delta_td, [delta_td], 'delta_td: ',
                #                     first_n=1)

                eval_tas[0] = eval_tas[0].write(idx, em_seq)
                eval_tas[1] = eval_tas[1].write(idx, sig_seq)
                eval_tas[2] = eval_tas[2].write(idx, pred)
                eval_tas[3] = eval_tas[3].write(idx,
                                                tf.cast(pred_ts, tf.int32))
                eval_tas[4] = eval_tas[4].write(idx, p_seq)
                eval_tas[5] = eval_tas[5].write(idx, c2_seq)
                eval_tas[6] = eval_tas[6].write(idx, sig_single_seq)
                eval_tas[7] = eval_tas[7].write(idx, glimpse_seq)
                eval_tas[8] = eval_tas[8].write(idx, returns)
                eval_tas[9] = eval_tas[9].write(idx, b)
                eval_tas[10] = eval_tas[10].write(idx, image)
                eval_tas[11] = eval_tas[11].write(idx, low_res)
                eval_tas[12] = eval_tas[12].write(idx, target)

                l_seq, p_seq = em_seq[..., :2], em_seq[..., 2]
                o_seq, e_seq = sig_seq[..., 0], sig_seq[..., 1]

                summaries[0] = summaries[0] + tf.reduce_mean(l_seq)
                summaries[1] = summaries[1] + tf.reduce_mean(p_seq)
                summaries[2] = summaries[2] + tf.reduce_mean(o_seq)
                summaries[3] = summaries[3] + tf.reduce_mean(e_seq)
                summaries[4] = summaries[4] + tf.reduce_mean(returns)
                summaries[5] = summaries[5] + tf.reduce_mean(b)
                summaries[6] = summaries[6] + tf.to_float(N_ts)
                summaries[7] = summaries[7] + acc

                pred_sum = tf.reduce_sum(predictions, axis=0)  # [N_cl]
                targ_sum = tf.reduce_sum(target, axis=0)  # [N_cl]

                l_cum = self.classification_loss(pred_sum, targ_sum)
                l_b = self.Baseline.getLoss(delta_td, b)

            L_cum += l_cum
            L_b += l_b

            # image index
            idx += 1

            return idx, L_cum, L_b, summaries, eval_tas

        loop_vars = [0, 0., 0., [0.]*8]

        if self.mode == tf.contrib.learn.ModeKeys.EVAL:
            names = ['em_arr', 'sig_arr', 'pred_arr', ['p_ts_arr', tf.int32],
                     'p_seq_arr',
                     'c2_arr', 'sig_single_arr', 'glimpse_arr', 'V_arr',
                     'b_arr', 'image_arr', 'low_res_arr', 'target_arr']
            loop_vars += [get_tensor_arrays(names)]
        else:
            loop_vars += [[]]

        _, L_cum, L_b, summaries, eval_tas = tf.while_loop(cond,
                                                           body,
                                                           loop_vars,
                                                           name='while_batch')
        L_cum /= tf.to_float(N_img)
        L_b /= tf.to_float(N_img)

        for i, s in enumerate(summaries):
            summaries[i] = s / tf.to_float(N_img)

        tf.summary.scalar('trajectory_mean', summaries[0])
        tf.summary.scalar('patch_mean', summaries[1])
        tf.summary.scalar('o_mean', summaries[2])
        tf.summary.scalar('e_mean', summaries[3])
        tf.summary.scalar('V_mean', summaries[4])
        tf.summary.scalar('b_mean', summaries[5])
        tf.summary.scalar('N_ts_mean', summaries[6])
        tf.summary.scalar('accuracy', summaries[7])

        accuracy = tf.identity(summaries[7], name='accuracy')
        print accuracy
        if self.mode == tf.contrib.learn.ModeKeys.EVAL:
            self.name_eval_tensors(eval_tas)

        return L_cum, L_b, accuracy, eval_tas

    def name_eval_tensors(self, eval_tas):
        bs = self.Config.getParameter('batchSize')

        print '========================'
        print 'tensor indeces:'
        for i in range(bs):
            print i
            tf.identity(eval_tas[0].read(i), 'emission_{}'.format(i))
            tf.identity(eval_tas[1].read(i), 'signal_{}'.format(i))
            tf.identity(eval_tas[2].read(i), 'prediction_{}'.format(i))
            tf.identity(eval_tas[3].read(i), 'prediction_ts_{}'.format(i))
            tf.identity(eval_tas[4].read(i), 'prediction_seq_{}'.format(i))
            tf.identity(eval_tas[5].read(i), 'c2_seq_{}'.format(i))
            tf.identity(eval_tas[6].read(i), 'sig_single_{}'.format(i))
            tf.identity(eval_tas[7].read(i), 'glimpses_{}'.format(i))
            tf.identity(eval_tas[8].read(i), 'value_{}'.format(i))
            tf.identity(eval_tas[9].read(i), 'baseline_{}'.format(i))
            tf.identity(eval_tas[10].read(i), 'image_{}'.format(i))
            tf.identity(eval_tas[11].read(i), 'low_res_{}'.format(i))
            tf.identity(eval_tas[12].read(i), 'target_{}'.format(i))

        print '========================'

    def collect_eval_tas(self, idx, collector, samples):
        '''Concatenates tensors stored in collectTA to tensors stored in
        writeTA.'''

        N_samples = self.Config.getParameter('HyperParameters/M')
        N_collections = 10
        start_index = idx * N_samples
        stop_index = (idx + 1) * N_samples

        def cond(write_idx, *args):
            write_idx < stop_index

        def body(write_idx, collect_idx, collector, samples):
            for i in range(N_collections):
                collector[i] = collector[i].write(write_idx,
                                                  samples[i].read(collect_idx))
            write_idx += 1
            collect_idx += 1

            return write_idx, collect_idx, collector, samples

        loop_vars = [start_index, 0, collector, samples]

        _, _, collector, _ = tf.while_loop(cond, body, loop_vars)

        return collector
