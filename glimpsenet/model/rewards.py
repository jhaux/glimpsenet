import tensorflow as tf
import logging

from util.decorators import addVariableScope

logger = logging.getLogger(__name__)


def rewards(predictions, targets):
    '''Calculate reward based on targets and predictions

    Arguments:
        predictions: Tensor containing predicted labels.
            Shape: [M, timesteps, batch, numClasses]
        targets: Tensor containing ground truth.
            Shape: same as predictions

    Returns:
        rewards: Tensor with entries either 1 or 0.
                 Shape: [M, timesteps, batch]
    '''

    logger.debug('predictions:'.format(predictions))
    logger.debug('targets:'.format(targets))

    with tf.variable_scope('Reward'):
        asserts = [
                tf.assert_rank_at_least(predictions, 3),
                tf.assert_rank_at_least(targets, 3)
                ]
        with tf.control_dependencies(asserts):
            argmax_t = tf.to_int32(tf.argmax(targets, axis=-1))
            argmax_p = tf.to_int32(tf.argmax(predictions, axis=-1))

            # I do not understand why this happens
            if argmax_p.get_shape().as_list()[0] == 1:
                argmax_p = tf.squeeze(argmax_p, axis=0)

            logger.debug('argmax_t'.format(argmax_t))
            logger.debug('argmax_p'.format(argmax_p))

            rewards = tf.equal(argmax_p, argmax_t)
            rewards = tf.to_float(rewards)

            logger.debug('rewards:'.format(rewards))

        return rewards  # shape [M, timesteps, batch]


@addVariableScope
def combined_rewards(rewards):
    '''Turn the rewards into cumulative rewards modulus pred_steps.

    Given all rewards one is interested to grant later rewards also the
    previous rewards, as they are harder to get.
    In the paper http://arxiv.org/pdf/1412.7755v2.pdf the authors use the
    following formula:
        $$R_s = \sum_{j \leq s} R_j$$
    As there are multiple rewards per object (N = pred_steps many) I
    approximate the formula by summing over all previous mean rewards per
    object. This yields:
    \begin{align}
        R^{\prime}_{sl}
        &= R_{sl} + \sum_{j < s} \frac{1}{N} \sum_{i=1}^N R_{ji} \\
        &= R_{sl} + \frac{1}{N} \sum_{j=1}^{(s-1)N} R_j
    \end{align}
    with $l \in [1,N]$, $s \in [0, maxObjs]$.

    Ok, so this is nice, but a simple cumsum is easier and makes as much sense.
    Predicting the correct label predSteps times is also cool and rewarding
    this behaviour is also favorable.
    '''

    # ax 0 is samples, 1 is time, 2 is batch
    combined_rewards = tf.cumsum(rewards, axis=-2)
    print 'combined_rewards', combined_rewards
    logger.debug('rewards vs combined rewards: {} vs {}'
                 .format(rewards, combined_rewards))

    combined_rewards = tf.Print(combined_rewards, [rewards[..., 0]],
                                'rewards vs combined_rewards: ', 20, 20)
    combined_rewards = tf.Print(combined_rewards, [combined_rewards[..., 0]],
                                'rewards vs combined_rewards: ', 20, 20)

    return combined_rewards


@addVariableScope
def maybeStepWise(tensors, Config, mode=tf.contrib.learn.ModeKeys.EVAL):
    '''Determine if loss is only evaluated at every predStep'th timestep
    and comb tensors accordingly

    Arguments:
        tensors: list of tensors to manipulate
        config: GlimpseNetConfig instance
        predSteps: prediction interval
        mode: either train or eval

    Returns:
        tensors: list of manipulated tensors
    '''

    print tensors

    predSteps = Config.getParameter('HyperParameters/predSteps')
    maxObjs = Config.getParameter('HyperParameters/maxObjs')

    with tf.variable_scope('Step_preparation'):
        print 'stepwise:', Config.getParameter("stepWisePrediction")
        if not Config.getParameter("stepWisePrediction"):
            new_tensors = []
            for tensor in tensors:
                tensor_at_steps = []
                if mode == tf.contrib.learn.ModeKeys.TRAIN:
                    for i in range(maxObjs):
                        idx = (predSteps-1) * (i + 1)
                        tensor_at_steps.append(tensor[:, idx, ...])

                    tensor_at_steps.append(tensor[:, -1, ...])  # EOS

                    tensor = tf.stack(tensor_at_steps, axis=1)
                else:
                    for i in range(maxObjs):
                        idx = (predSteps-1) * (i + 1)
                        tensor_at_steps.append(tensor[idx, ...])

                    tensor_at_steps.append(tensor[-1, ...])  # EOS
                    tensor = tf.stack(tensor_at_steps, axis=0)
                new_tensors.append(tensor)
                print 'after stepwise:', tensor
            tensors = new_tensors
        return tensors


@addVariableScope
def expand_rewards(rewards, Config):
    '''Given the rewards for all prediction steps expand the rewards over
    all timesteps.

    Arguments:
        rewards: Tensor of shape [(M), maxObjs, batch]
        config: DramaConfig instance

    Returns:
        expanded_rewards: Tensor of shape [(M), N, batch]
    '''

    predSteps = Config.getParameter('HyperParameters/predSteps')
    stepWisePredictions = Config.getParameter('stepWisePrediction')

    num_expand = predSteps if stepWisePredictions else 1

    transform = [1, 0, 2] if len(rewards.shape.as_list()) == 3 else [1, 0]

    expanded_rewards = tf.transpose(rewards, transform)

    expanded_rewards = tf.unstack(expanded_rewards, axis=0)  # 0 is batch size
    expanded_rewards_expand = []
    for target in expanded_rewards:
        single_expand = [target] * num_expand
        expanded_rewards_expand += single_expand
    # stack -> [maxObjs*predSteps or maxObjs, bs, C]
    expanded_rewards = tf.parallel_stack(expanded_rewards_expand)

    expanded_rewards = tf.transpose(expanded_rewards, transform)

    return expanded_rewards
